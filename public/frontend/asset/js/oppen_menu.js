function openMenu() {
    $('body').css('overflow', 'hidden');
    $('.overlay-menu').fadeIn(300);
    $('.menu-mobile').addClass('show');
}
function closeMenu() {
    $('body').css('overflow', '');
    $('.overlay-menu').fadeOut(300);
    $('.menu-mobile').removeClass('show');
}
addLoadEvent(function () {
        
            var itemTop = $('.header .main-header').offset().top + $('.header .main-header').height() + $('.header .extra-header').height();
            if ($(window).scrollTop() > itemTop) {
                $('.header .extra-header').addClass('sticky');
            }
            $(window).on('scroll', function () {
                if ($(this).scrollTop() > itemTop) {
                    $('.header .extra-header').addClass('sticky');
                } else {
                    $('.header .extra-header').removeClass('sticky');
                }
            });
        
});