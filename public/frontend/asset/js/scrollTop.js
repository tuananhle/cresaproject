var images = document.querySelectorAll('source, img');
if ('IntersectionObserver' in window) {
    var onChange = function onChange(changes, observer) {
        changes.forEach(function (change) {
            if (change.intersectionRatio > 0) {
                loadImage(change.target);
                observer.unobserve(change.target);
            }
        });
    };
    var config = { root: null, threshold: 0.1 }; var observer = new IntersectionObserver(onChange, config); images.forEach(function (img) { return observer.observe(img); });
} else {
    images.forEach(function (image) { return loadImage(image); });
}

function loadImage(image) {
    image.classList.add('fade', 'show');
    if (image.dataset && image.dataset.src) {
        image.src = image.dataset.src;
    }

    if (image.dataset && image.dataset.srcset) {
        image.srcset = image.dataset.srcset;
    }
}
addLoadEvent(function () {
    $(window).scroll(function () {
        if ($(window).scrollTop() > 700) {
            $('.scroll-top').show();
        }
        else {
            $('.scroll-top').hide();
        }
    });

    $('.scroll-top').click(function () {
        $("html, body").animate({ scrollTop: 0 }, "slow");
    });
    if ($(window).width() < 768) {
        $('.scroll-top').css({ 'bottom': '90px', 'right': '8px' });
    }
});
addLoadEvent(function () {
    setTimeout(function () {
        (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-WM8XKV');
    }, 3000);
});
var __lc = {};
__lc.license = 4036971;