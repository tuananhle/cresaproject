
var flagTop = false;
var flagBot = false;

addLoadEvent(function () {
    var ulExperience = $('.experience ul');
    if ($(window).width() < 767) {
        ulExperience.find('li:nth-child(n+4):not(.view-more)').addClass('hidden-item');
    }
    $('.view-more a').on('click', function () {
        var $this = $(this),
            ul = $this.parents('ul');
        if ($(window).width() > 767) {

            if (ul.find('li.hidden-item:nth-child(n+5):not(.view-more)').length > 0) {
                ul.find('li.hidden-item:nth-child(n+5):not(.view-more)').removeClass('hidden-item');
                $this.html('<i class="fa fa-angle-double-right"></i> Thu gọn');
            } else {
                ul.find('li:nth-child(n+5):not(.view-more)').addClass('hidden-item');
                $this.html('<i class="fa fa-angle-double-right"></i> Xem thêm');
            }
        } else {
            if (ul.find('li.hidden-item:nth-child(n+4):not(.view-more)').length > 0) {
                ul.find('li.hidden-item:nth-child(n+4):not(.view-more)').removeClass('hidden-item');
                $this.html('<i class="fa fa-angle-double-right"></i> Thu gọn');
            } else {
                ul.find('li:nth-child(n+4):not(.view-more)').addClass('hidden-item');
                $this.html('<i class="fa fa-angle-double-right"></i> Xem thêm');
            }
        }
    });

    $('.block-mobile .block-info .title').on('click', function () {
        var $this = $(this),
            item = $this.parent('.block-info').find('.content');
        if (item.is(':visible')) {
            item.slideUp(300);
            $this.find(".fa-plus").removeClass('hidden');
            $this.find(".fa-minus").addClass('hidden');
        } else {
            $('.block-mobile .block-info .title .fa-plus').removeClass('hidden');
            $('.block-mobile .block-info .title .fa-minus').addClass('hidden');
            $('.block-mobile .block-info .content').slideUp(300);
            item.slideDown(300);
            $this.find(".fa-plus").addClass('hidden');
            $this.find(".fa-minus").removeClass('hidden');
        }
    });

    $('.scroll-down').on('click', function () {
        var dataHref = $(this).data('href'),
            extraHeader = 0;
        if ($(window).width() >= 1200) {
            extraHeader = 64
        }
        $('html, body').animate({
            scrollTop: $(dataHref).offset().top - extraHeader
        }, 500);
    });
    var benefitSwiper = new Swiper('.benefit .swiper-container', {
        slidesPerView: 2,
        spaceBetween: 30,
        pagination: {
            el: ".benefit .swiper-pagination",
            clickable: true,
        },
        navigation: {
            nextEl: '.benefit .swiper-button-next',
            prevEl: '.benefit .swiper-button-prev',
        },
        loop: true,
        autoplay: '20000',
        preventClicks: false,
        preventClicksPropagation: false,
        simulateTouch: false,
        breakpoints: {
            991: {
                slidesPerView: 1
            }
        }
    });
    var functionSwiper = new Swiper('.function .swiper-container', {
        slidesPerView: 1,
        pagination: {
            el: ".function .swiper-pagination",
            clickable: true,
        },
        navigation: {
            nextEl: '.function .swiper-button-next',
            prevEl: '.function .swiper-button-prev',
        },
        loop: true,
        autoplay: '20000',
        preventClicks: false,
        preventClicksPropagation: false,
        simulateTouch: false
    });
    if ($(window).width() < 1200) {
        functionSwiper.destroy(true, true);
    }
    $(window).on('resize', function () {
        if ($(window).width() < 1200) {
            functionSwiper.destroy(true, true);
        } else {
            functionSwiper = new Swiper('.function .swiper-container', {
                slidesPerView: 1,
                pagination: {
                    el: ".function .swiper-pagination",
                    clickable: true,
                },
                navigation: {
                    nextEl: '.function .swiper-button-next',
                    prevEl: '.function .swiper-button-prev',
                },
                loop: true,
                autoplay: '20000',
                preventClicks: false,
                preventClicksPropagation: false,
                simulateTouch: false
            });
        }
    });
    if ($(window).width() < 768) {
        var blockSwiper5 = new Swiper('.technology .swiper-container', {
            pagination: {
                el: ".technology .swiper-pagination",
                clickable: true,
            },
            slidesPerView: 1,
            autoplay: '5000'
        });
    }
    $('.function h3').on('click', function () {
        var $this = $(this),
            content = $(this).parent('div').find('p');
        if (content.is(":visible")) {
            content.slideUp();
            $this.find('i').removeClass('fa-minus').addClass('fa-plus');
        } else {
            $('.function .block-content p').slideUp();
            $('.function h3 i').removeClass('fa-minus').addClass('fa-plus');
            content.slideDown();
            $this.find('i').removeClass('fa-plus').addClass('fa-minus');
        }
    });
});
