import Vue from 'vue';
import VueRouter from 'vue-router';

const _import = require('./_import_sync');
import store from '../store/index';
import CONSTANTS from '../core/utils/constants';
import ENUM from "../../config/enum";

Vue.use(VueRouter); 
let _routers = [
            {
                name:'login',
                path:'/login',
                component: _import('auth/login'),
                meta:{
                    requiresVisitor: true,
                }
            },
            {
                name: 'register',
                path: '/register',
                component: _import('auth/register'),
                meta: {
                    requiresVisitor: true,
                }
            },
            {
                name: 'home',
                path: '/',
                component: _import('home'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'list_category',
                path: '/product/category',
                component: _import('cate/list'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'edit_category',
                path: '/product/category/edit/:quiz_cate/:language',
                component: _import('cate/edit'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'listProduct',
                path: '/product',
                component: _import('products/list'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'edit_product',
                path: '/product/edit/:pro_code/:language',
                component: _import('products/edit'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'createProduct',
                path: '/product/create',
                component: _import('products/add'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'listBlogs',
                path: '/blogs',
                component: _import('blogs/list'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'addBlogs',
                path: '/blog/add',
                component: _import('blogs/add'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'editBlog',
                path: '/blog/edit/:quiz_id/:language',
                component: _import('blogs/edit'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'listCateBlog',
                path: '/blog/category',
                component: _import('blogs/category/list'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'editCateBlog',
                path: '/blog/category/edit/:quiz_cate/:language',
                component: _import('blogs/category/edit'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'language',
                path: '/language',
                component: _import('language/language'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'languageKeyword',
                path: '/language/keyword',
                component: _import('language/keyword'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'pageContent',
                path: '/pagecontent',
                component: _import('pagecontent/list'),
                meta: {
                    requiresAuth: true,
                }
            },
             {
                name: 'pageContentAdd',
                path: '/pagecontent/add',
                component: _import('pagecontent/add'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'pageContentEdit',
                path: '/pagecontent/edit/:quiz_id/:language',
                component: _import('pagecontent/edit'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'banner',
                path: '/banner',
                component: _import('website/banner'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'partner',
                path: '/partner',
                component: _import('website/partner'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'prize',
                path: '/prize',
                component: _import('website/prize'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'setting',
                path: '/setting',
                component: _import('website/setting'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'customer',
                path: '/customer',
                component: _import('customer/list'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'customerAdd',
                path: '/customer/add',
                component: _import('customer/add'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'customerEdit',
                path: '/customer/edit/:id_customer',
                component: _import('customer/edit'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'billAdd',
                path: '/bill/add',
                component: _import('bill/add'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'billDetail',
                path: '/bill/detail/:code_bill',
                component: _import('bill/detail'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'billDraft',
                path: '/bill/draft',
                component: _import('bill/draft'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'billPaymented',
                path: '/bill/paymented',
                component: _import('bill/paymented'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                name: 'billUnPayment',
                path: '/bill/Unpayment',
                component: _import('bill/unpayment'),
                meta: {
                    requiresAuth: true,
                }
            }
];
const router = new VueRouter({
    errorHandler(to, from, next, error) {
    },
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else {
            return {x: 0, y: 0}
        }
    },
    routes: _routers
});
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!store.getters.isLoggedIn) {
            next({
                name: 'login'
            })
        } else {
            next()
        }
    } else if (to.matched.some(record => record.meta.requiresVisitor)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (store.getters.isLoggedIn) {
            next({
                name: 'home'
            })
        } else {
            next()
        }
    } else {
        next() // make sure to always call next()!
    }
})

export default router;
