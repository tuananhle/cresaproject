<div class="extra-header d-none d-xl-block">
    <div class="container d-xl-flex align-items-center justify-content-between">
       <div class="logo d-flex align-items-center">
       <a href="{{url('/')}}">
       <img src="{{url('/').$setting->logo}}" alt="Cresa" />
          <span>CresaWeb</span>
          </a>
       </div>
         <div class="menu justify-content-between">
          <ul class="d-xl-flex align-items-center">
            <li class="@if(Request::route()->getName() == 'tinhnang') active @endif"  ><a href="{{route('tinhnang')}}">Tính năng</a></li>
            <li class="@if(Request::route()->getName() == 'priceTable') active @endif"><a href="{{route('priceTable')}}">Bảng giá theo giao diện mẫu</a></li>
            <li class="@if(Request::route()->getName() == 'priceRequest') active @endif"><a href="{{route('priceRequest')}}">Bảng giá theo yêu cầu</a></li>
            <li class="@if(Request::route()->getName() == 'khogiaodien') active @endif"><a href="{{url('kho-giao-dien/'.'all'.'.html')}}">Kho giao diện</a></li>
            <li class=""><a href="{{route('khachHang')}}">Khách hàng</a></li>
          </ul>
          <a href="javascript:;" class="btn-registration" data-toggle="modal" data-target="#ModalSetupTuvan">Bạn cần tư vấn?</a>
         </div>
    </div>
 </div>
