<div class="main-header d-flex align-items-center flex-wrap">
    <div class="container">
       <div class="row">
          <div class="col-xl-2 col-8 d-flex align-items-center">
             <a id="logo" class="main-logo d-xl-block d-none" href="/">
             <img src="{{url('/').$setting->logo}}" alt="Cresa logo" />
             </a>
             <a class="main-logo d-block d-xl-none" href="/">
             <img src="{{url('/').$setting->logo}}" alt="Cresa logo" />
             </a>
          </div>
          <div class="col-xl-10 col-4 d-flex justify-content-end">
             <ul class="d-none d-xl-flex justify-content-end">
               <li>
               <a href="{{route('aboutUs')}}">Về chúng tôi</a>
               </li>
            <li><a href="{{route('lienHe')}}">Liên hệ</a></li>
             </ul>
             <a href="javascript:;" class="d-inline-block d-xl-none btn-menu" onclick="openMenu();" aria-hidden="true">
               <i class="fas fa-bars"></i>
             </a>
          </div>
       </div>
    </div>
 </div>