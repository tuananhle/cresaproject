<div class="menu-mobile">
    <div class="logo-mobile">
       <a href="/thiet-ke-website-ban-hang.html">
       <img src="{{url('/').$setting->logo}}" alt="Sapo logo" />
       </a>
       <a href="javascript:;" class="btn-close-menu" onclick="closeMenu();" aria-hidden="true">
         <i class="fas fa-times-circle"></i>
       </a>
    </div>
    <div class="box-scroll">
       <ul class="nav">
          <li class="@if(Request::route()->getName() == 'tinhnang') active @endif"><a href="{{route('tinhnang')}}">Tính năng</a></li>
          <li class="@if(Request::route()->getName() == 'priceTable') active @endif"><a href="{{route('priceTable')}}">Bảng giá</a></li>
          <li class="@if(Request::route()->getName() == 'khogiaodien') active @endif"><a href="{{url('kho-giao-dien/'.'all'.'.html')}}">Kho giao diện</a></li>
          <li class=""><a href="{{route('khachHang')}}">Khách hàng</a></li>
          <li class="trial">
             <a href="javascript:;" class="btn-registration" data-toggle="modal" data-target="#ModalSetupTuvan"><span>Bạn cần tư vấn?</span></a>
          </li>
       </ul>
       <ul class="nav nav-bottom">
          <li>
             <a href="{{route('aboutUs')}}">Về chúng tôi</a>
          </li>
          <li >
             <a href="{{route('lienHe')}}">Liên hệ</a>
          </li>
          <li class="home">
             <a href="/">Cresa.vn</a>
          </li>
       </ul>
    </div>
 </div>