<div class="modal modal-setup fade" id="ModalSetupTuvan" tabindex="-1" role="dialog" aria-labelledby="ModalSetupTuvan" aria-hidden="true">
    <div class="modal-dialog">
       <div class="modal-content modal-themes">
          <div class="modal-header" style="padding-right:0;">
             <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body text-center">
             <div class="form-horizontal">
                <div class="form-group">
                   <h3>Hãy để lại thông tin để chúng tôi hỗ trợ bạn nhé</h3>
                   <p style="font-size: 15px; color: #b332d3cf;">Điền đầy đủ thông tin</p>
                </div>
             <form method="post" action="{{route('userRequesta')}}" id="formLoginRedirect" novalidate>
                   @csrf
                   <div class="form-group row quick-registration">
                      <div class="col-md-12">
                         <input name="name" type="text" placeholder="Họ và tên" class="input-site-name">
                      </div>
                   </div>
                   <div class="form-group row quick-registration">
                     <div class="col-md-12">
                         <input name="phone" type="text" placeholder="Số điện thoại" required class="input-site-name">
                      </div>
                   </div>
                   <div class="form-group row quick-registration">
                     <div class="col-md-12">
                         <input name="email" type="text" placeholder="Email" required class="input-site-name">
                      </div>
                   </div>
                   <div class="form-group row quick-registration">
                    <div class="col-md-12">
                        <textarea name="note" type="text"  id="" cols="50" rows="5" placeholder="Ghi chú" class="input-site-name"></textarea>
                     </div>
                  </div>
                   <hr style="margin: 20px 0; border-bottom: 1px solid #e0e0e0;">
                    <div class="form-group quick-registration">
                   <button id="btnSearch" type="submit" class="btn-registration event-Sapo-Free-Trial-form-open">
                   Hoàn tất
                   </button>
                </div>
                </form>
                <script>
                    $("#btnSearch").click(function(event) {

                     // Fetch form to apply custom Bootstrap validation
                     var form = $("#formLoginRedirect")

                     if (form[0].checkValidity() === false) {
                     event.preventDefault()
                     event.stopPropagation()
                     }

                     form.addClass('was-validated');
                     // Perform ajax submit here...

                     });
                </script>
             </div>
          </div>
       </div>
    </div>
 </div>