<!DOCTYPE html>
<html lang="vi">
   <head>
      <title>@yield('title')</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
      <meta name="description" content='@yield('title')'/>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="robots" content="index, follow"/>
      <meta name="Googlebot" content="index, follow"/>
      <!-- Open Graph data -->
      <meta property="og:site_name" content="@yield('title')"/>
      <meta property="og:type" content="article"/>
      <meta property="og:locale" content="vi_VN"/>
      <meta property="og:title" content='@yield("title")'/>
      <meta property="og:description" content='@yield("description")'/>
      <meta property="og:url" content='@yield("url")'/>
      <meta name="keywords" content='@yield("keyword")'/>
      <meta property="og:image" itemprop="thumbnailUrl" content='@yield("images")'/>
      <meta property="og:image:width" content="600"/>
      <meta property="og:image:height" content="348"/>
      
      <link rel="shortcut icon" href="{{$setting->favicon}}">
      <meta name="csrf-token" content="{{ csrf_token() }}" />

      <script src="{{url('frontend/asset/js/defaultV2.min.js')}}" async></script>
        @yield('css')
      <link href="{{url('frontend/asset/css/detail.min.css')}}" rel="stylesheet" />
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i&display=swap&subset=vietnamese" rel="stylesheet">
      <script type="text/javascript">
         function addLoadEvent(e) { if (document.readyState === "complete") { e() } else { var t = window.onload; if (typeof window.onload != "function") { window.onload = e } else { window.onload = function () { if (t) { t() } e() } } } }
      </script>
      <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.2/css/all.min.css" rel="stylesheet" type="text/css" />
      <script async src="{{url('frontend/asset/js/ins.js')}}"></script>
      <!-- Start of HubSpot Embed Code -->
      <script type="text/javascript" id="hs-script-loader" async defer src="{{url('frontend/asset/js/6624727.js')}}"></script>
      @yield('js')
      <!-- Load Facebook SDK for JavaScript -->
      <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v5.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your customer chat code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="100300274835770"
         theme_color="#7646ff"
         logged_in_greeting="Xin chào bạn! Chúng tôi có thể giúp gì cho bạn?"
         logged_out_greeting="Xin chào bạn! Chúng tôi có thể giúp gì cho bạn?">
               </div>
      <!-- End of HubSpot Embed Code -->
   </head>
   <body>
      <div style="position: absolute; width: 900px; height: 500px; float: left; vertical-align: top; top: -1950px;">
         <h1>@yield('title')</h1>
         <h2>@yield("description")</h2>
      </div>
      <div id="container">
         <div id="header" class="index WEB active-child header">
            @include('layouts.header.main-header')
            @include('layouts.header.extra-header')
         </div>
         @include('layouts.header.mobile')
         <div class="overlay-menu" onclick="closeMenu()"></div>
        <script type="text/javascript" src="{{asset('frontend/asset/js/oppen_menu.js')}}"></script>
         @yield('content')
         @include('layouts.footer')
         <!-- Banner marketing -->
         <style type="text/css">
            .Cresa-advertiser {
            position: fixed;
            bottom: 0;
            left: 0;
            z-index: 999;
            }
            .Cresa-advertiser a {
            outline: none;
            }
            .Cresa-advertiser .popup-adv {
            position: relative;
            }
            .Cresa-advertiser .popup-adv i {
            z-index: 99;
            color: #333;
            font-size: 24px;
            cursor: pointer;
            height: 15px;
            width: 15px;
            line-height: 15px;
            }
            .Cresa-advertiser .popup-adv .hide-popup {
            position: absolute;
            right: -15px;
            top: 0;
            }
            .Cresa-advertiser .popup-adv .hide-popup.active {
            top: 0;
            right: 0;
            }
         </style>
         <script type="text/javascript">
            addLoadEvent(function () {
                var checkHide = sessionStorage.getItem("hide_banner");
                if (checkHide == null) {
                    $(".Cresa-advertiser").show();
                }
            
                $(".Cresa-advertiser .popup-adv .hide-popup").click(function () {
                    $(".Cresa-advertiser").hide();
                    sessionStorage.setItem("hide_banner", "true");
                });
            });
         </script>
      </div>
      <img class="scroll-top" data-src="{{url('frontend/images/totop.png')}}" style="display:none;cursor :pointer; position : fixed;bottom : 90px;right : 33px;z-index : 99;" />
    <script src="{{asset('frontend/asset/js/scrollTop.js')}}"></script>
   @include('layouts.modal.modal_user_request')
   </body>
</html>