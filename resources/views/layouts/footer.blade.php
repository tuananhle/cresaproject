<div id="footer" class="footer">
    <div class="footer-menu">
       <div class="container">
       </div>
    </div>
    <div class="footer-address">
       <div class="container">
          <div class="row">
             <div class="col-w-20 col-md-4 d-lg-block d-none">
                <div class="title">Cresa</div>
                <ul>
                   <li>
                      <a target="_blank" href="{{route('aboutUs')}}" rel="nofollow">Về chúng tôi</a>
                   </li>
                   <li>
                      <a href="{{route('khachHang')}}" rel="nofollow">Khách hàng</a>
                   </li>
                   <li>
                      <a href="{{route('lienHe')}}">Liên hệ</a>
                   </li>
                </ul>
             </div>
             <div class="col-w-20 col-md-4">
                <div class="title">Dịch vụ</div>
                <ul>
                   <li>
                      <a href="{{url('kho-giao-dien/'.'all'.'.html')}}">Kho giao diện</a>
                   </li>
                   <li>
                      <a href="{{route('priceTable')}}">Bảng giá</a>
                   </li>
                   <li>
                      <a href="{{route('khachHang')}}" target="_blank" rel="nofollow">Khách hàng</a>
                   </li>
                </ul>
                </ul>
             </div>
             <div class="col-w-60 col-md-8">
                <div class="title">Liên hệ</div>
                <ul class="social">
                   <li>
                      <a href="#" target="_blank" rel="nofollow" aria-hidden="true"><i class="fab fa-facebook-square"></i></a>
                   </li>
                   <li>
                      <a href="#" target="_blank" rel="nofollow" aria-hidden="true"><i class="fab fa-youtube"></i></a>
                   </li>
                   <li>
                      <a href="#" target="_blank" aria-hidden="true"><i class="fab fa-weixin"></i></a>
                   </li>
                </ul>
                <div class="contact-info">
                   <p class="text-uppercase">{{$setting->company}}</p>
                   <p><span>Trụ sở <i class="fa fa-map-marker"></i> </span>{{$setting->address1}}</p>
                   <p><strong>Tổng đài tư vấn và hỗ trợ khách hàng: </strong><b>{{$setting->phone1}}</b></p>
                   <p><span>Email: </span><b>{{$setting->email}}</b></p>
                   <p>Từ 8h00 – 22h00 các ngày từ thứ 2 đến Chủ nhật</p>
                </div>
                <a href="http://online.gov.vn/CustomWebsiteDisplay.aspx?DocId=44198" target="_blank" rel="nofollow" class="footer-bct">
                <img src="{{url('frontend/images/bocongthuong.png')}}" data-src="{{url('frontend/images/bocongthuong.png')}}" alt="Chứng nhận Bộ Công Thương" />
                </a>
             </div>
          </div>
       </div>
    </div>
    <div class="footer-copyright">
       <div class="container">
          <div class="row">
             <div class="col-lg-7 col-md-6 order-md-1 order-3">
                <p class="copyright"><span class="d-none d-lg-inline-block">Copyright © 2019&nbsp;</span><a href="https://www.Cresa.vn" target="_blank">Cresa.vn</a> - Nền tảng quản lý và bán hàng đa kênh được sử dụng nhiều nhất Việt Nam</p>
             </div>
             <hr style="border-top-color: #2b2a32; width: 100%;margin:0;" class="d-block d-sm-none order-2" />
             <div class="col-lg-5 col-md-6 order-md-2 order-1">
             </div>
          </div>
       </div>
       <p class="copyright-mobile d-lg-none d-md-block d-none">© Copyright 2008 - 2019</p>
    </div>
 </div>