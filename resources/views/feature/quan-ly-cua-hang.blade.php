@extends('layouts.master')
@section('title') Quản lý cửa hàng online thông minh @endsection
@section('description') Giúp bạn tiết kiệm tới 30% thời gian quản lý những vẫn đạt được hiệu quả cao nhất @endsection 
@section('url') {{ route('quanLySanPham') }} @endsection
@section('keyword') Quản lý cửa hàng , quản lý sản phẩm , mạnh mẽ, bảo mật @endsection
@section('image') {{ url('/').$setting->logo }} @endsection
@section('css')
    <link href="{{url('frontend/asset/css/function.min.css')}}" rel="preload" as="style" type="text/css" />
    <link href="{{url('frontend/asset/css/function.min.css')}}" rel="stylesheet" />
@endsection
@section('content')
<div id="wrapper" class="clearfix">
    <div class="function-page smartmanager">
       <div class="function-menu">
          <div class="container">
             <h1>Quản lý cửa hàng online thông minh</h1>
             <p>Giúp bạn tiết kiệm tới 30% thời gian quản lý những vẫn đạt được hiệu quả cao nhất</p>
             <div class="function-menu-slide">
                <div class="swiper-container swiper-container-horizontal">
                   <div class="swiper-wrapper">
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('tinhnang')}}">Tính năng<br>nổi bật</a></div>
                     <div class="item swiper-slide swiper-slide-next" style="width: 162.857px;"><a href="{{route('banHangUuViet')}}">Web bán hàng<br>ưu việt</a></div>
                     <div class="item swiper-slide current swiper-slide-active" style="width: 162.857px;"><a href="{{route('quanLyCuaHang')}}">Quản lý cửa<br> hàng online</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('tuyChinhGiaoDien')}}">Tùy chỉnh<br>giao diện</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('marketingSeo')}}">Marketing<br>&amp; SEO</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('quanLySanPham')}}">Quản lý<br>sản phẩm</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('manhMeBaoMat')}}">Mạnh mẽ<br>&amp; Bảo mật</a></div>
                   </div>
                   <div class="swiper-pagination d-lg-none swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span></div>
                   <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                </div>
                <div class="swiper-button-prev d-lg-none swiper-button-disabled" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="true"><i class="fa fa-angle-left"></i></div>
                <div class="swiper-button-next d-lg-none swiper-button-disabled" tabindex="0" role="button" aria-label="Next slide" aria-disabled="true"><i class="fa fa-angle-right"></i></div>
             </div>
          </div>
       </div>
       <script type="text/javascript">
          addLoadEvent(function () {
              var blockSwipermain = new Swiper('.function-menu .swiper-container', {
                  slidesPerView: 7,
                  loop: false,
                  preventClicks: false,
                  preventClicksPropagation: false,
                  simulateTouch: false,
                  pagination: {
                      el: '.function-menu .swiper-pagination',
                      clickable: true,
                  },
                  navigation: {
                      nextEl: '.function-menu .swiper-button-next',
                      prevEl: '.function-menu .swiper-button-prev',
                  },
                  breakpoints: {
                      400: {
                          slidesPerView: 2,
                          simulateTouch: true,
                          slidesPerGroup: 2
                      },
                      767: {
                          slidesPerView: 3,
                          simulateTouch: true,
                          slidesPerGroup: 3
                      },
                      991: {
                          slidesPerView: 5,
                          simulateTouch: true,
                          slidesPerGroup: 5
                      }
                  }
              });
              blockSwipermain.slideTo(3, 300, false);
          });
       </script>
       <div class="function-content">
          <div class="container">
             <div class="row item align-items-center">
                <div class="col-lg-6 order-lg-2 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab4-1.jpg" alt="Quản lý cửa hàng trên nền tảng website bán hàng Cresa Web" class="fade show">
                </div>
                <div class="col-lg-6 order-lg-1 block-content">
                   <h2>Quản lý <br class="d-none d-lg-block">thông tin khách hàng</h2>
                   <p>Thấu hiểu tâm lý khách hàng thông qua lịch sử mua hàng sẽ giúp bạn đưa ra các chương trình quảng cáo phù hợp giúp tăng nhanh doanh số. Ngoài ra việc khuyến khích khách hàng tạo tài khoản trên website cũng sẽ giúp bạn thu thập thông tin khách hàng phục vụ cho các chương trình khuyến mại của mình.</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab4-2.jpg" alt="Quản lý cửa hàng trên nền tảng website bán hàng cresa Web">
                </div>
                <div class="col-lg-6 block-content">
                   <h2>Tùy chỉnh mẫu email <br class="d-none d-lg-block">gửi đến khách hàng</h2>
                   <p>Bạn có thể dễ dàng tùy chỉnh mẫu email gửi đến khách hàng theo từng thời điểm khác nhau để chăm sóc khách hàng hiệu quả và tối ưu nhất. Qua đó, khách hàng sẽ thấy yên tâm hơn và hoàn toàn hài lòng với dịch vụ của bạn.</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 order-lg-2 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab4-3.jpg" alt="Quản lý cửa hàng trên nền tảng website bán hàng Cresa Web">
                </div>
                <div class="col-lg-6 order-lg-1 block-content">
                   <h2>Xử lý đơn hàng <br class="d-none d-lg-block">nhanh chóng</h2>
                   <p>Tốc độ xử lý đơn hàng là 1 trong những điểm quan trong giúp bạn giữ được khách hàng ở lại với mình. <a href="/thiet-ke-website-ban-hang.html#công_nghệ_tính_năng">Website bán hàng Cresa Web</a> được trang bị đầy đủ công cụ để xử lý đơn hàng với chỉ vài click chuột. Việc hoàn thành đơn hàng, liên hệ đơn vị vận chuyển, thống kê quản lý đơn hàng... sẽ không còn tốn nhiều công sức của bạn.</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab4-4.jpg" alt="Quản lý cửa hàng trên nền tảng website bán hàng Cresa Web">
                </div>
                <div class="col-lg-6 block-content">
                   <h2>Quản lý cửa hàng <br class="d-none d-lg-block">mọi lúc, mọi nơi</h2>
                   <p>Với xu thế sử dụng máy tính bảng và điện thoại thông minh phổ biến như hiện nay thì nhu cầu theo dõi và quản lý cửa hàng trên thiết bị di động là cấp thiết hơn bao giờ hết. Với Cresa Web bạn có thể truy cập quản trị website trên máy tính bảng, điện thoại ở khắp mọi nơi để cập nhật thông tin, quản lý sản phẩm, đơn hàng, liên hệ với khách hàng...</p>
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>
 </div>
@endsection