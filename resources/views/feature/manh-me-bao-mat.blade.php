@extends('layouts.master')
@section('title') Nền tảng hệ thống website mạnh mẽ @endsection
@section('description') Website hoạt động ổn định tốc độ truy cập vượt trội và đảm bảo tính bảo mật an toàn @endsection 
@section('url') {{ route('manhMeBaoMat') }} @endsection
@section('keyword') Quản lý cửa hàng , quản lý sản phẩm , mạnh mẽ, bảo mật @endsection
@section('image') {{ url('/').$setting->logo }} @endsection
@section('css')
    <link href="{{url('frontend/asset/css/function.min.css')}}" rel="preload" as="style" type="text/css" />
    <link href="{{url('frontend/asset/css/function.min.css')}}" rel="stylesheet" />
@endsection
@section('content')
<div id="wrapper" class="clearfix">
    <div class="function-page powerfulplatform">
       <div class="function-menu">
          <div class="container">
             <h1>Nền tảng hệ thống website mạnh mẽ</h1>
             <p>Website hoạt động ổn định tốc độ truy cập vượt trội và đảm bảo tính bảo mật an toàn</p>
             <div class="function-menu-slide">
                <div class="swiper-container swiper-container-horizontal">
                   <div class="swiper-wrapper">
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('tinhnang')}}">Tính năng<br>nổi bật</a></div>
                     <div class="item swiper-slide swiper-slide-next" style="width: 162.857px;"><a href="{{route('banHangUuViet')}}">Web bán hàng<br>ưu việt</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('quanLyCuaHang')}}">Quản lý cửa<br> hàng online</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('tuyChinhGiaoDien')}}">Tùy chỉnh<br>giao diện</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('marketingSeo')}}">Marketing<br>&amp; SEO</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('quanLySanPham')}}">Quản lý<br>sản phẩm</a></div>
                     <div class="item swiper-slide current swiper-slide-active" style="width: 162.857px;"><a href="{{route('manhMeBaoMat')}}">Mạnh mẽ<br>&amp; Bảo mật</a></div>
                   </div>
                   <div class="swiper-pagination d-lg-none swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span></div>
                   <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                </div>
                <div class="swiper-button-prev d-lg-none swiper-button-disabled" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="true"><i class="fa fa-angle-left"></i></div>
                <div class="swiper-button-next d-lg-none swiper-button-disabled" tabindex="0" role="button" aria-label="Next slide" aria-disabled="true"><i class="fa fa-angle-right"></i></div>
             </div>
          </div>
       </div>
       <script type="text/javascript">
          addLoadEvent(function () {
              var blockSwipermain = new Swiper('.function-menu .swiper-container', {
                  slidesPerView: 7,
                  loop: false,
                  preventClicks: false,
                  preventClicksPropagation: false,
                  simulateTouch: false,
                  pagination: {
                      el: '.function-menu .swiper-pagination',
                      clickable: true,
                  },
                  navigation: {
                      nextEl: '.function-menu .swiper-button-next',
                      prevEl: '.function-menu .swiper-button-prev',
                  },
                  breakpoints: {
                      400: {
                          slidesPerView: 2,
                          simulateTouch: true,
                          slidesPerGroup: 2
                      },
                      767: {
                          slidesPerView: 3,
                          simulateTouch: true,
                          slidesPerGroup: 3
                      },
                      991: {
                          slidesPerView: 5,
                          simulateTouch: true,
                          slidesPerGroup: 5
                      }
                  }
              });
              blockSwipermain.slideTo(7, 300, false);
          });
       </script>
       <div class="function-content">
          <div class="container">
             <div class="row item align-items-center">
                <div class="col-lg-6 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab2-1.jpg" alt="Nền tảng website bán hàng được bảo mật mạnh mẽ của Cresa Web" class="fade show">
                </div>
                <div class="col-lg-6 block-content">
                   <h2>Không giới hạn băng thông</h2>
                   <p>Cresa Web cung cấp cho bạn website bán hàng với tốc độ truy cập vượt trội, băng thông không giới hạn. Bạn không bao giờ phải lo lắng về việc trả thêm chi phí để mở rộng băng thông nhằm phục vụ khách hàng truy cập vào website của mình.</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 order-lg-2 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab2-2.jpg" alt="Nền tảng website bán hàng được bảo mật mạnh mẽ của Cresa Web">
                </div>
                <div class="col-lg-6 order-lg-1 block-content">
                   <h2>Hệ thống máy chủ mạnh mẽ</h2>
                   <p>Cresa Web sử dụng hệ thống máy chủ mạnh mẽ, đảm bảo website của bạn hoạt động ổn định, điều hướng trang mượt mà và mọi dữ liệu được bảo mật ở mức cao nhất</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab2-3.jpg" alt="Nền tảng website bán hàng được bảo mật mạnh mẽ của Cresa Web">
                </div>
                <div class="col-lg-6 block-content">
                   <h2>Website được bảo mật cao <br class="d-none d-lg-block">với chứng chỉ SSL</h2>
                   <p>Website của bạn được trang bị sẵn chuẩn SSL giúp xác thực website và bảo mật thông tin trước sự tấn công của virus, hacker. Đồng thời, nâng cao mức độ uy tín của doanh nghiệp và đưa website nhanh chóng leo top Google.</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 order-lg-2 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab2-4.jpg" alt="Nền tảng website bán hàng được bảo mật mạnh mẽ của Cresa Web">
                </div>
                <div class="col-lg-6 order-lg-1 block-content">
                   <h2>Sao lưu dữ liệu liên tục</h2>
                   <p>Hệ thống của chúng tôi mỗi ngày đều thực hiện backup dữ liệu của bạn để đảm bảo không có sự cố nào về dữ liệu có thể xảy ra với website của bạn.</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab2-5.jpg" alt="Nền tảng website bán hàng được bảo mật mạnh mẽ của Cresa Web">
                </div>
                <div class="col-lg-6 block-content">
                   <h2>Nâng cấp tính năng tự động</h2>
                   <p>Website của bạn sẽ liên tục được nâng cấp các tính năng mới tự động để đảm bảo bạn và khách hàng có những trải nghiệm tốt nhất khi sử dụng Cresa Web.</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 order-lg-2 block-img">
                   <div class="info-img"><img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab2-6.jpg" alt="Nền tảng website bán hàng được bảo mật mạnh mẽ của Cresa Web" class="img-responsive"></div>
                </div>
                <div class="col-lg-6 order-lg-1 block-content">
                   <h2>Hệ thống hoạt động <br class="d-none d-lg-block">ổn định, hỗ trợ 24/7</h2>
                   <p>Với Crea Web, bạn có thể hoàn toàn yên tâm vì luôn có 1 đội ngũ kỹ thuật hùng hậu theo dõi website của bạn 24/7.  Đảm bảo website của bạn sẽ luôn hoạt động ổn định và Cresa Web sẵn sàng trợ giúp bạn bất cứ lúc nào</p>
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>
@endsection