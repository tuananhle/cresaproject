@extends('layouts.master')
@section('title') Công cụ Marketing hiệu quả @endsection
@section('description') Công cụ marketing mạnh mẽ giúp việc quảng bá sản phẩm trở nên dễ dàng và mang lại hiệu quả cao @endsection 
@section('url') {{ route('marketingSeo') }} @endsection
@section('keyword') Quản lý cửa hàng , quản lý sản phẩm , mạnh mẽ, bảo mật @endsection
@section('image') {{ url('/').$setting->logo }} @endsection
@section('css')
    <link href="{{url('frontend/asset/css/function.min.css')}}" rel="preload" as="style" type="text/css" />
    <link href="{{url('frontend/asset/css/function.min.css')}}" rel="stylesheet" />
@endsection
@section('content')
<div id="wrapper" class="clearfix">
    <div class="function-page seomarketing">
       <div class="function-menu">
          <div class="container">
             <h1>Công cụ Marketing hiệu quả</h1>
             <p>Công cụ marketing mạnh mẽ giúp việc quảng bá sản phẩm trở nên dễ dàng và mang lại hiệu quả cao</p>
             <div class="function-menu-slide">
                <div class="swiper-container swiper-container-horizontal">
                   <div class="swiper-wrapper">
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('tinhnang')}}">Tính năng<br>nổi bật</a></div>
                     <div class="item swiper-slide swiper-slide-next" style="width: 162.857px;"><a href="{{route('banHangUuViet')}}">Web bán hàng<br>ưu việt</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('quanLyCuaHang')}}">Quản lý cửa<br> hàng online</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('tuyChinhGiaoDien')}}">Tùy chỉnh<br>giao diện</a></div>
                     <div class="item swiper-slide current swiper-slide-active" style="width: 162.857px;"><a href="{{route('marketingSeo')}}">Marketing<br>&amp; SEO</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('quanLySanPham')}}">Quản lý<br>sản phẩm</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('manhMeBaoMat')}}">Mạnh mẽ<br>&amp; Bảo mật</a></div>
                   </div>
                   <div class="swiper-pagination d-lg-none swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span></div>
                   <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                </div>
                <div class="swiper-button-prev d-lg-none swiper-button-disabled" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="true"><i class="fa fa-angle-left"></i></div>
                <div class="swiper-button-next d-lg-none swiper-button-disabled" tabindex="0" role="button" aria-label="Next slide" aria-disabled="true"><i class="fa fa-angle-right"></i></div>
             </div>
          </div>
       </div>
       <script type="text/javascript">
          addLoadEvent(function () {
              var blockSwipermain = new Swiper('.function-menu .swiper-container', {
                  slidesPerView: 7,
                  loop: false,
                  preventClicks: false,
                  preventClicksPropagation: false,
                  simulateTouch: false,
                  pagination: {
                      el: '.function-menu .swiper-pagination',
                      clickable: true,
                  },
                  navigation: {
                      nextEl: '.function-menu .swiper-button-next',
                      prevEl: '.function-menu .swiper-button-prev',
                  },
                  breakpoints: {
                      400: {
                          slidesPerView: 2,
                          simulateTouch: true,
                          slidesPerGroup: 2
                      },
                      767: {
                          slidesPerView: 3,
                          simulateTouch: true,
                          slidesPerGroup: 3
                      },
                      991: {
                          slidesPerView: 5,
                          simulateTouch: true,
                          slidesPerGroup: 5
                      }
                  }
              });
              blockSwipermain.slideTo(5, 300, false);
          });
       </script>
       <div class="function-content">
          <div class="container">
             <div class="row item align-items-center">
                <div class="col-lg-6 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab1-1.png" alt="Công cụ Marketing được Cresa Web tích hợp sẵn trên website" class="fade show">
                </div>
                <div class="col-lg-6 block-content">
                   <h2>Nền tảng vững chắc để đưa website lên top</h2>
                   <p>Cresa Web giúp bạn xây dựng nền tảng để đưa website lên top trên các trang công cụ tìm kiếm như Google, Yahoo, Bing… Tối ưu hóa trang web thật dễ dàng. Tùy chỉnh các thẻ meta title, tag, mô tả, URL của trang Bổ sung thẻ alt cho hình ảnh Tối ưu hóa theo chủ đề SEO Xây dựng liên kết thân thiện với các bộ máy tìm kiếm</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 order-lg-2 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab1-2.png" alt="Công cụ Marketing được Cresa Web tích hợp sẵn trên website">
                </div>
                <div class="col-lg-6 order-lg-1 block-content">
                   <h2>Đầy đủ công cụ hỗ trợ</h2>
                   <p>Để đưa từ khóa lên top 1 không phải là 1 điều đơn giản nhưng cũng không có nghĩa bạn phải là 1 chuyên gia SEO mới có thể làm được. <a href="/thiet-ke-website-ban-hang.html#công_nghệ_tính_năng">Website thiết kế bởi Cresa Web</a> được cung cấp đầy đủ công cụ để bạn có thể tự đưa website của mình lên top một cách dễ dàng</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab1-3.png" alt="Công cụ Marketing được Cresa Web tích hợp sẵn trên website">
                </div>
                <div class="col-lg-6 block-content">
                   <h2>Tự động tạo sitemap.xml</h2>
                   <p>Tự động tạo sẵn file sitemap.xml bao gồm: sản phẩm, nhóm sản phẩm, blog, trang nội dung đơn giản và nhanh chóng chỉ với vài cú click chuột</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 order-lg-2 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab1-4.png" alt="Công cụ Marketing được Cresa Web tích hợp sẵn trên website">
                </div>
                <div class="col-lg-6 order-lg-1 block-content">
                   <h2>Đánh giá sản phẩm</h2>
                   <p>Đối với khách hàng, còn gì đáng tin hơn chính những nhận xét của người mua trước về sản phẩm họ định mua. Với tính năng đánh giá sản phẩm, bạn có thể khuyến khích khách hàng để lại đánh giá cho sản phẩm của mình và nó cũng hỗ trợ SEO tốt hơn.</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab1-7.png" alt="Công cụ Marketing được Cresa Web tích hợp sẵn trên website">
                </div>
                <div class="col-lg-6 block-content">
                   <h2>Chương trình khuyến mại và coupon</h2>
                   <p>Đối với hầu hết khách hàng thì việc nhận được những món quà sau khi mua hàng đều khiến họ thích thú và nhớ đến cửa hàng của bạn. Bởi vậy, các chương trình khuyến mại, tặng mã coupon dành cho khách hàng luôn được các công ty, cửa hàng áp dụng triệt để nhằm làm hài lòng cũng như tạo dựng được mạng lưới khách hàng thân thiết. Cresa Web cung cấp cho bạn công cụ hỗ trợ thiết lập và quản lý các chương trình khuyến mại được nhanh chóng và thuận tiện nhất.</p>
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>
@endsection