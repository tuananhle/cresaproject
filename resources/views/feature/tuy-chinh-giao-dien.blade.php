@extends('layouts.master')
@section('title') Tùy chỉnh website theo phong cách của bạn @endsection
@section('description') Tự tùy chỉnh giao diện website linh hoạt không giới hạn @endsection 
@section('url') {{ route('tuyChinhGiaoDien') }} @endsection
@section('keyword') Quản lý cửa hàng , quản lý sản phẩm , mạnh mẽ, bảo mật @endsection
@section('image') {{ url('/').$setting->logo }} @endsection
@section('css')
    <link href="{{url('frontend/asset/css/function.min.css')}}" rel="preload" as="style" type="text/css" />
    <link href="{{url('frontend/asset/css/function.min.css')}}" rel="stylesheet" />
@endsection
@section('content')
<div id="wrapper" class="clearfix">
    <div class="function-page customizewebsite">
       <div class="function-menu">
          <div class="container">
             <h1>Tùy chỉnh website theo phong cách của bạn</h1>
             <p>Tự tùy chỉnh giao diện website linh hoạt không giới hạn</p>
             <div class="function-menu-slide">
                <div class="swiper-container swiper-container-horizontal">
                   <div class="swiper-wrapper">
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('tinhnang')}}">Tính năng<br>nổi bật</a></div>
                     <div class="item swiper-slide swiper-slide-next" style="width: 162.857px;"><a href="{{route('banHangUuViet')}}">Web bán hàng<br>ưu việt</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('quanLyCuaHang')}}">Quản lý cửa<br> hàng online</a></div>
                     <div class="item swiper-slide current swiper-slide-active" style="width: 162.857px;"><a href="{{route('tuyChinhGiaoDien')}}">Tùy chỉnh<br>giao diện</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('marketingSeo')}}">Marketing<br>&amp; SEO</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('quanLySanPham')}}">Quản lý<br>sản phẩm</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('manhMeBaoMat')}}">Mạnh mẽ<br>&amp; Bảo mật</a></div>
                   </div>
                   <div class="swiper-pagination d-lg-none swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span></div>
                   <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                </div>
                <div class="swiper-button-prev d-lg-none swiper-button-disabled" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="true"><i class="fa fa-angle-left"></i></div>
                <div class="swiper-button-next d-lg-none swiper-button-disabled" tabindex="0" role="button" aria-label="Next slide" aria-disabled="true"><i class="fa fa-angle-right"></i></div>
             </div>
          </div>
       </div>
       <script type="text/javascript">
          addLoadEvent(function () {
              var blockSwipermain = new Swiper('.function-menu .swiper-container', {
                  slidesPerView: 7,
                  loop: false,
                  preventClicks: false,
                  preventClicksPropagation: false,
                  simulateTouch: false,
                  pagination: {
                      el: '.function-menu .swiper-pagination',
                      clickable: true,
                  },
                  navigation: {
                      nextEl: '.function-menu .swiper-button-next',
                      prevEl: '.function-menu .swiper-button-prev',
                  },
                  breakpoints: {
                      400: {
                          slidesPerView: 2,
                          simulateTouch: true,
                          slidesPerGroup: 2
                      },
                      767: {
                          slidesPerView: 3,
                          simulateTouch: true,
                          slidesPerGroup: 3
                      },
                      991: {
                          slidesPerView: 5,
                          simulateTouch: true,
                          slidesPerGroup: 5
                      }
                  }
              });
              blockSwipermain.slideTo(4, 300, false);
          });
       </script>
       <div class="function-content">
          <div class="container">
             <div class="row item align-items-center">
                <div class="col-lg-6 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab6-1.jpg" alt="Tùy chỉnh giao diện website bán hàng không giới hạn" class="fade show">
                </div>
                <div class="col-lg-6 block-content">
                   <h2>Kho giao diện phong phú</h2>
                   <p>Kho giao diện của Cresa Web đa dạng phong cách và phong phú phù hợp với mọi ngành nghề, được sáng tạo bởi những chuyên gia hàng đầu trong lĩnh vực thương mại điện tử. Bạn không phải lo lắng quá nhiều về việc thiết kế website như thế nào để thu hút khách hàng, Cresa Web sẽ làm giúp bạn!</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 order-lg-2 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab6-2.jpg" alt="Tùy chỉnh giao diện website bán hàng không giới hạn" v="">
                </div>
                <div class="col-lg-6 order-lg-1 block-content">
                   <h2>Tùy biến giao diện không giới hạn</h2>
                   <p>Đối với chủ cửa hàng, giao diện website là nơi thể hiện phong cách cũng như nét thu hút riêng của sản phẩm. Sapo Web cho phép bạn tùy biến giao diện website không giới hạn. Bạn có thể thoải mái cấu hình website theo ý muốn của mình</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab6-3.jpg" alt="Tùy chỉnh giao diện website bán hàng không giới hạn">
                </div>
                <div class="col-lg-6 block-content">
                   <h2>Tự thiết kế chuyên nghiệp</h2>
                   <p>Chúng tôi cho phép bạn truy cập và chỉnh sửa giao diện thông qua file HTML/ CSS hay download các nội dung file về để lưu giữ thành các phiên bản của website. Thông qua việc sửa đổi nội dung file HTML/ CSS bạn cũng có thể tối ưu hóa nội dung và hỗ trợ SEO</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 order-lg-2 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tu-chinh-sua-giao-dien.png" alt="Tùy chỉnh giao diện website bán hàng không giới hạn">
                </div>
                <div class="col-lg-6 order-lg-1 block-content">
                   <h2>Dịch vụ thiết kế đồ họa</h2>
                   <p>Nếu bạn vẫn chưa hoàn toàn ưng ý với giao diện website hiện tại thì chúng tôi có đội ngũ các chuyên gia thiết kế website giàu kinh nghiệm và nhạy bén trong lĩnh vực thương mại điện tử sẽ giúp website của bạn trở nên thu hút và chuyên nghiệp.</p>
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>
@endsection