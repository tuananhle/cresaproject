@extends('layouts.master')
@section('title') Thiết kế website bán hàng với nhiều tính năng ưu việt @endsection
@section('description') Giải pháp gia tăng doanh thu nhanh chóng với website bán hàng chuyên nghiệp @endsection 
@section('url') {{ route('banHangUuViet') }} @endsection
@section('keyword') Quản lý cửa hàng , quản lý sản phẩm , mạnh mẽ, bảo mật @endsection
@section('image') {{ url('/').$setting->logo }} @endsection
@section('css')
    <link href="{{url('frontend/asset/css/function.min.css')}}" rel="preload" as="style" type="text/css" />
    <link href="{{url('frontend/asset/css/function.min.css')}}" rel="stylesheet" />
@endsection
@section('content')
<div id="wrapper" class="clearfix">
    <div class="function-page buildweb">
       <div class="function-menu">
          <div class="container">
             <h1>Thiết kế website bán hàng với nhiều tính năng ưu việt</h1>
             <p>Giải pháp gia tăng doanh thu nhanh chóng với website bán hàng chuyên nghiệp</p>
             <div class="function-menu-slide">
                <div class="swiper-container swiper-container-horizontal">
                   <div class="swiper-wrapper">
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('tinhnang')}}">Tính năng<br>nổi bật</a></div>
                     <div class="item swiper-slide swiper-slide-next  current swiper-slide-active" style="width: 162.857px;"><a href="{{route('banHangUuViet')}}">Web bán hàng<br>ưu việt</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('quanLyCuaHang')}}">Quản lý cửa<br> hàng online</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('tuyChinhGiaoDien')}}">Tùy chỉnh<br>giao diện</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('marketingSeo')}}">Marketing<br>&amp; SEO</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('quanLySanPham')}}">Quản lý<br>sản phẩm</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('manhMeBaoMat')}}">Mạnh mẽ<br>&amp; Bảo mật</a></div>
                   </div>
                   <div class="swiper-pagination d-lg-none swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span></div>
                   <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                </div>
                <div class="swiper-button-prev d-lg-none swiper-button-disabled" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="true"><i class="fa fa-angle-left"></i></div>
                <div class="swiper-button-next d-lg-none swiper-button-disabled" tabindex="0" role="button" aria-label="Next slide" aria-disabled="true"><i class="fa fa-angle-right"></i></div>
             </div>
          </div>
       </div>
       <script type="text/javascript">
          addLoadEvent(function () {
              var blockSwipermain = new Swiper('.function-menu .swiper-container', {
                  slidesPerView: 7,
                  loop: false,
                  preventClicks: false,
                  preventClicksPropagation: false,
                  simulateTouch: false,
                  pagination: {
                      el: '.function-menu .swiper-pagination',
                      clickable: true,
                  },
                  navigation: {
                      nextEl: '.function-menu .swiper-button-next',
                      prevEl: '.function-menu .swiper-button-prev',
                  },
                  breakpoints: {
                      400: {
                          slidesPerView: 2,
                          simulateTouch: true,
                          slidesPerGroup: 2
                      },
                      767: {
                          slidesPerView: 3,
                          simulateTouch: true,
                          slidesPerGroup: 3
                      },
                      991: {
                          slidesPerView: 5,
                          simulateTouch: true,
                          slidesPerGroup: 5
                      }
                  }
              });
              blockSwipermain.slideTo(2, 300, false);
          });
       </script>
       <div class="function-content">
          <div class="container">
             <div class="row item align-items-center">
                <div class="col-lg-6 order-lg-2 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab7-2.jpg" alt="Web bán hàng ưu việt Cresa Web được +67,000 shop sử dụng" class="fade show">
                </div>
                <div class="col-lg-6 order-lg-1 block-content">
                   <h2>Giao diện website <br class="d-none d-lg-block">chuyên nghiệp, cực đẹp</h2>
                   <p>Với kho giao diện đa dạng được thiết kế bới các chuyên gia nhiều kinh nghiệm trong lĩnh vực thương mại điện tử của Cresa Web, bạn không cần mất nhiều thời gian cho việc lên ý tưởng thiết kế hay dựng giao diện. Chỉ cần thao tác chọn nhanh chóng là website của bạn đã có một giao diện đẹp, chuyên nghiệp, hoàn thiện.</p>
                   <a href="https://themes.sapo.vn/" target="_blank">Xem thêm kho giao diện <i class="fa fa-angle-double-right"></i></a>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab7-3.jpg" alt="Web bán hàng ưu việt Cresa Web được +1,000 shop sử dụng">
                </div>
                <div class="col-lg-6 block-content">
                   <h2>Hệ thống quản lý <br class="d-none d-lg-block">sản phẩm và  đơn hàng tối ưu</h2>
                   <p>Sử dụng Cresa Web bạn không chỉ có 1 website với giao diện đẹp mà bạn còn có cả 1 hệ thống quản lý sản phẩm, đơn hàng, khách hàng, doanh thu lãi lỗ, quản lý kho hàng và mọi thông tin liên quan đến cửa hàng của bạn 1 cách thông minh và thân thiện nhất. Bạn không cần là chuyên gia về công nghệ nhưng cũng sẽ tự quản lý tốt website của mình.</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 order-lg-2 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab7-4.jpg" alt="Web bán hàng ưu việt Cresa Web được +67,000 shop sử dụng">
                </div>
                <div class="col-lg-6 order-lg-1 block-content">
                   <h2>Thu hút khách hàng <br class="d-none d-lg-block">trên mọi thiết bị</h2>
                   <p>Tỷ lệ người dùng smartphone ngày một tăng cao đồng nghĩa với nó là bạn không thể bỏ qua một lượng khách hàng khổng lồ tiếp cập webisite của bạn qua di động. Vì vậy, Cresa Web sẽ giúp bạn cung cấp đầy đủ các tính năng để website của bạn luôn đẹp và thân thiện trên mọi thiết bị.</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab7-5.jpg" alt="Web bán hàng ưu việt Cresa Web được +1,000 shop sử dụng">
                </div>
                <div class="col-lg-6 block-content">
                   <h2>Nâng tầm <br class="d-none d-lg-block">thương hiệu của bạn</h2>
                   <p>Một website bán hàng chuyên nghiệp được thiết kế với bố cục sinh động cùng những tính năng nổi bật sẽ tạo được ấn tượng mạnh mẽ đối với doanh nghiệp. Thiết kế website bán hàng cùng Cresa Web bạn có thể đặt tên miền riêng hoặc mua tên miền riêng của Cresa Web để lưu lại dấu ấn với khách hàng.</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 order-lg-2 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab7-6.jpg" alt="Web bán hàng ưu việt Cresa Web được +1,000 shop sử dụng">
                </div>
                <div class="col-lg-6 order-lg-1 block-content">
                   <h2>Hoàn thiện website <br class="d-none d-lg-block">với đội ngũ chuyên gia</h2>
                   <p>Bạn lo lắng vì không am hiểu công nghệ?<br>Bạn sợ mình ko thể tự thiết kế website đẹp<br> Đội ngũ chuyên gia của chúng tôi sẵn sàng giúp đỡ bạn trên mọi lĩnh vực.</p>
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>
@endsection