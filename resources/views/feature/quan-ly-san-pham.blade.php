@extends('layouts.master')
@section('title') Quản lý sản phẩm thông minh và linh hoạt @endsection
@section('description') Hơn cả 1 website thông thường Cresa Web còn là 1 hệ thống quản lý sản phẩm mà bạn mong muốn @endsection 
@section('url') {{ route('quanLySanPham') }} @endsection
@section('keyword') Quản lý cửa hàng , quản lý sản phẩm , mạnh mẽ, bảo mật @endsection
@section('image') {{ url('/').$setting->logo }} @endsection
@section('css')
    <link href="{{url('frontend/asset/css/function.min.css')}}" rel="preload" as="style" type="text/css" />
    <link href="{{url('frontend/asset/css/function.min.css')}}" rel="stylesheet" />
@endsection
@section('content')
<div id="wrapper" class="clearfix">
    <div class="function-page managerproduct">
       <div class="function-menu">
          <div class="container">
             <h1>Quản lý sản phẩm thông minh và linh hoạt</h1>
             <p>Hơn cả 1 website thông thường Cresa Web còn là 1 hệ thống quản lý sản phẩm mà bạn mong muốn</p>
             <div class="function-menu-slide">
                <div class="swiper-container swiper-container-horizontal">
                   <div class="swiper-wrapper">
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('tinhnang')}}">Tính năng<br>nổi bật</a></div>
                     <div class="item swiper-slide swiper-slide-next" style="width: 162.857px;"><a href="{{route('banHangUuViet')}}">Web bán hàng<br>ưu việt</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('quanLyCuaHang')}}">Quản lý cửa<br> hàng online</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('tuyChinhGiaoDien')}}">Tùy chỉnh<br>giao diện</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('marketingSeo')}}">Marketing<br>&amp; SEO</a></div>
                     <div class="item swiper-slide current swiper-slide-active" style="width: 162.857px;"><a href="{{route('quanLySanPham')}}">Quản lý<br>sản phẩm</a></div>
                     <div class="item swiper-slide" style="width: 162.857px;"><a href="{{route('manhMeBaoMat')}}">Mạnh mẽ<br>&amp; Bảo mật</a></div>
                   </div>
                   <div class="swiper-pagination d-lg-none swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span></div>
                   <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                </div>
                <div class="swiper-button-prev d-lg-none swiper-button-disabled" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="true"><i class="fa fa-angle-left"></i></div>
                <div class="swiper-button-next d-lg-none swiper-button-disabled" tabindex="0" role="button" aria-label="Next slide" aria-disabled="true"><i class="fa fa-angle-right"></i></div>
             </div>
          </div>
       </div>
       <script type="text/javascript">
          addLoadEvent(function () {
              var blockSwipermain = new Swiper('.function-menu .swiper-container', {
                  slidesPerView: 7,
                  loop: false,
                  preventClicks: false,
                  preventClicksPropagation: false,
                  simulateTouch: false,
                  pagination: {
                      el: '.function-menu .swiper-pagination',
                      clickable: true,
                  },
                  navigation: {
                      nextEl: '.function-menu .swiper-button-next',
                      prevEl: '.function-menu .swiper-button-prev',
                  },
                  breakpoints: {
                      400: {
                          slidesPerView: 2,
                          simulateTouch: true,
                          slidesPerGroup: 2
                      },
                      767: {
                          slidesPerView: 3,
                          simulateTouch: true,
                          slidesPerGroup: 3
                      },
                      991: {
                          slidesPerView: 5,
                          simulateTouch: true,
                          slidesPerGroup: 5
                      }
                  }
              });
              blockSwipermain.slideTo(6, 300, false);
          });
       </script>
       <div class="function-content">
          <div class="container">
             <div class="row item align-items-center">
                <div class="col-lg-6 order-lg-2 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab5-1.jpg" alt="Quản lý sản phẩm thông minh trên web bán hàng của Cresa Web" class="fade show">
                </div>
                <div class="col-lg-6 order-lg-1 block-content">
                   <h2>Thông tin sản phẩm <br class="d-none d-lg-block">đầy đủ, rõ ràng</h2>
                   <p>Bạn có thể đăng sản phẩm thật nhanh chóng với những thông tin cơ bản như: Tên, giá, mô tả sản phẩm hay tạo những cấu hình cao cấp hơn như: Thiết lập tùy chọn, quản lý kho hàng, SEO… Tất cả yêu cầu đều được đáp ứng giúp việc quản lý sản phẩm của bạn trở nên dễ dàng nhất</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab5-2.jpg" alt="Quản lý sản phẩm thông minh trên web bán hàng của Cresa Web">
                </div>
                <div class="col-lg-6 block-content">
                   <h2>Trình bày sản phẩm <br class="d-none d-lg-block">thu hút</h2>
                   <p>Sản phẩm được trình bày bắt mắt, thông tin về sản phẩm được bố trí rõ ràng, thông tin các chương trình khuyến mãi cũng được đặt ở vị trí dễ nhìn giúp khách hàng cảm thấy tin tưởng và kích thích đặt mua hàng.</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 order-lg-2 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab5-3.jpg" alt="Quản lý sản phẩm thông minh trên web bán hàng của Cresa Web">
                </div>
                <div class="col-lg-6 order-lg-1 block-content">
                   <h2>Nhập xuất <br class="d-none d-lg-block">danh sách sản phẩm</h2>
                   <p>Bạn có thể nhập/ xuất danh sách sản phẩm nhanh chóng với >website bán hàng của Cresa Web, Chỉ cần vài thao tác là bạn đã có thể đăng lên cùng lúc hàng nghìn sản phẩm hay xuất danh sách các sản phẩm trên hệ thống để lưu trữ quản lý 1 cách nhanh chóng.</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab5-4.jpg" alt="Quản lý sản phẩm thông minh trên web bán hàng của Cresa Web">
                </div>
                <div class="col-lg-6 block-content">
                   <h2>Quản lý <br class="d-none d-lg-block">danh mục sản phẩm</h2>
                   <p>Bạn có thể sắp xếp và quản lý sản phẩm theo danh mục, tag, loại sản phẩm…. Danh mục sản phẩm tự động sẽ giúp bạn quản lý sản phẩm theo những tiêu chí bạn đặt ra.</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 order-lg-2 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab5-3.jpg" alt="Quản lý sản phẩm thông minh trên web bán hàng của Cresa Web">
                </div>
                <div class="col-lg-6 order-lg-1 block-content">
                   <h2>Tìm kiếm sản phẩm <br class="d-none d-lg-block">bằng bộ lọc thông minh</h2>
                   <p>Thông qua việc cài đặt và sử dụng ứng dụng Bộ lọc sản phẩm, bạn có thể sử dụng bộ lọc tự động với các trường thông tin từ sản phẩm như: màu sắc, kích thước, giá bán, nhà cung cấp...Bạn chỉ cần điền tên bộ lọc tự động muốn dùng và chọn Cập nhập, website của bạn sẽ ngay lập tức hiển thị thông tin các bộ lọc này.</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tab5-5.jpg" alt="Quản lý sản phẩm thông minh trên web bán hàng của Cresa Web">
                </div>
                <div class="col-lg-6 block-content">
                   <h2>Tùy chỉnh, <br class="d-none d-lg-block">tối ưu SEO hiệu quả</h2>
                   <p>Website bán hàng Cresa Web được tối ưu để thân thiện với các công cụ tìm kiếm, đặc biệt là Google. Tất cả các danh mục đều được tối ưu tiêu đề, mô tả SEO cho từng trang nội dung và cơ chế gợi ý tiêu đề, mô tả tự động. Ngoài hỗ trợ SEO hình ảnh với thẻ ALT  website bán hàng của Cresa Web còn được hỗ trợ tối ưu chuẩn SEO tại link URL</p>
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>
@endsection