@extends('layouts.master') @section('title') Báo giá thiết kế website tại cresa @endsection 
@section('description') {{$setting->fax}} @endsection 
@section('keyword') @endsection 
@section('url') {{ $setting->webname }} @endsection 
@section('image') {{ url('/').$setting->logo }} @endsection 
@section('css')
<link href="{{url('frontend/asset/css/about-us.min.css')}}" rel="preload" as="style" type="text/css" />
<link href="{{url('frontend/asset/css/about-us.min.css')}}" rel="stylesheet" />
<link href="{{url('frontend/asset/css/price.min.css')}}" rel="preload" as="style" type="text/css" />
<link href="{{url('frontend/asset/css/price.min.css')}}" rel="stylesheet" />
<link href="{{url('frontend/asset/css/bang_gia_theo_mau.css')}}" rel="stylesheet" /> @endsection @section('js') 
@endsection 
@section('content')
<div id="wrapper" class="clearfix">
    <div class="about-us">
        <div class="banner">
            <div class="container">
                <h1>Bảng giá các gói dịch vụ Cresa</h1>
                <p>Nếu bạn chưa chuẩn bị được ý tưởng cho website của mình, thì chúng tôi đã chuẩn bị hết tất cả cho bạn. Chỉ cần lựa chọn gói dịch vụ phù hợp với bạn</p>
            </div>
        </div>
    </div>
    <br>
    <div class="home-price">
        <div class="packages text-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-12">
                        <div class="row">
                            <div class="trai col-md-3">
                                <div class="chucnang">Chi tiết</div>
                                <div class="iconchucnang"><img src="https://cdn.pixabay.com/photo/2015/06/25/07/16/gears-820974_960_720.png" alt="chức năng website cresa"></div>
                                <div>
                                    <div class="tieudetron">
                                        <img src="{{url('frontend/images/icon_price/iconongheo.png')}}">
                                        <div>
                                            <div>
                                                <span class="spanchui textchucnang1_1">Combo tiết kiệm</span> <q></q>
                                            </div>
                                            <div class="divkhiiconihover textchucnang1_note">Thiết kế Website trọn gói đã bao gồm chi phí Thiết kế Website và chi phí Hosting với chi phí tiết kiệm hơn các hình thức thiết kế khác!</div>
                                            <span class="textchucnang1_2">(Web+Hosting)</span>
                                        </div>
                                    </div>
                                    <div class="tieudetron">
                                        <img src="{{url('frontend/images/icon_price/iconmenu.png')}}">
                                        <div>
                                            <div>
                                                <span class="spanchui textchucnang2_1">Chú thích phân bổ</span> <q></q>
                                            </div>
                                            <div class="divkhiiconihover textchucnang2_note">Bạn chỉ tốn 6 ngàn đồng 1 ngày hoặc 195 ngàn / 1 tháng để kinh doanh online với đầy đủ chức năng web thương mại điện tử. Chúng tôi cam kết giá cạnh tranh nhất thị trường, chất lượng dịch vụ đảm bảo tốt. Hoàn tiền nếu không đúng cam kết. </div>
                                            <span class="textchucnang2_2">giá theo tháng </span>
                                        </div>
                                    </div>
                                    <div class="tieudenho">
                                        <div class="backtrang_img"><img src="{{url('frontend/images/icon_price/icon1.png')}}"></div>
                                        <span class="spanchui textchucnang3">Dự kiến hoàn thành</span> <q></q>
                                        <div class="divkhiiconihover textchucnang3_note">Khoảng thời gian tối thiểu để kích hoạt dịch vụ và sử dụng được Website tính từ thời điểm Quý khách quyết định chọn các gói Dịch vụ theo yêu cầu.</div>
                                    </div>
                                    <div class="tieudenho">
                                        <div class="backtrang_img"><img src="{{url('frontend/images/icon_price/icon19.png')}}"></div>
                                        <span class="spanchui textchucnang21">Bàn giao Source Code</span> <q></q>
                                        <div class="divkhiiconihover textchucnang21_note">Bàn giao mã nguồn của Website. Tất cả các Website Quý khách hàng khi thiết kế tại Cresa đều có bàn giao mã nguồn khi Quý Khách hàng có yêu cầu.</div>
                                    </div>
                                    <div class="tieudenho">
                                        <div class="backtrang_img"><img src="{{url('frontend/images/icon_price/icon20.png')}}"></div>
                                        <span class="spanchui textchucnang22">Bảo hành trọn đời</span> <q></q>
                                        <div class="divkhiiconihover textchucnang22_note">Hỗ trợ bảo hành vĩnh viễn đối với các Website được thiết kế tại Cresa (Kèm theo điều kiện tương ứng).</div>
                                    </div>
                                    <div class="tieudenho">
                                        <div class="backtrang_img"><img src="{{url('frontend/images/icon_price/icon10.png')}}"></div>
                                        <span class="spanchui textchucnang12">Responsive Design</span> <q></q>
                                        <div class="divkhiiconihover textchucnang12_note">Tính tương thích với các thiết bị di động như: Smartphone, Tablet,...</div>
                                    </div>
                                    <div class="tieudenho" style="height: 75px">
                                        <div class="backtrang_img"><img src="{{url('frontend/images/icon_price/icon3.png')}}"></div>
                                        <span class="spanchui textchucnang5">Giao diện website</span> <q></q>
                                        <div class="divkhiiconihover textchucnang5_note">Giao diện Website được thiết kế tại Cresa đều đạt chuẩn SEO, Ứng dụng các công nghệ mới như: HTML5, CSS3, Responsive,... tuỳ theo gói chức năng.</div>
                                    </div>
                                    <div class="tieudenho tieudequatang">
                                        <div class="backtrang_img"><img src="{{url('frontend/images/icon_price/icon24.png')}}"></div>
                                        <span class="spanchui textchucnang23">Quà tặng đặc biệt</span> <q></q>
                                        <div class="divkhiiconihover textchucnang23_note">Quà tặng đặc biệt</div>
                                    </div>
                                    <div class="tieudenho">
                                        <div class="backtrang_img"><img src="{{url('frontend/images/icon_price/icon12.png')}}"></div>
                                        <span class="spanchui textchucnang14">Chuẩn SEO</span> <q></q>
                                        <div class="divkhiiconihover textchucnang14_note">Search Engine Optimization - Hỗ trợ giải pháp tối ưu thứ hạng Website trên các cỗ máy tìm kiếm</div>
                                    </div>
                                    <div class="tieudenho">
                                        <div class="backtrang_img"><img src="{{url('frontend/images/icon_price/icon14.png')}}"></div>
                                        <span class="spanchui textchucnang16">Chức năng Website</span> <q></q>
                                        <div class="divkhiiconihover textchucnang16_note">Các chức năng Website tương ứng với từng gói.</div>
                                    </div>
                                    <div class="tieudenho">
                                        <div class="backtrang_img"><img src="{{url('frontend/images/icon_price/icon4.png')}}"></div>
                                        <span class="spanchui textchucnang6">Tên miền</span> <q></q>
                                        <div class="divkhiiconihover textchucnang6_note">Tên miền hay còn gọi Domain là địa chỉ Website của cửa hàng hoặc Doanh nghiệp</div>
                                    </div>
                                    <div class="tieudenho">
                                        <div class="backtrang_img"><img src="{{url('frontend/images/icon_price/icon5.png')}}"></div>
                                        <span class="spanchui textchucnang7">Dung lượng lưu trữ</span> <q></q>
                                        <div class="divkhiiconihover textchucnang7_note">Dung lượng để lưu trữ mã nguồn Website (Source Code), hình ảnh, văn bản, và các dự liệu khác của Website.</div>
                                    </div>
                                    <div class="tieudenho">
                                        <div class="backtrang_img"><img src="{{url('frontend/images/icon_price/icon6.png')}}"></div>
                                        <span class="spanchui textchucnang8">Băng thông</span> <q></q>
                                        <div class="divkhiiconihover textchucnang8_note">Băng thông thể hiện lượt truy cập (Traffic) của người dùng cuối đến với Website trong 1 tháng.</div>
                                    </div>
                                    <div class="tieudenho">
                                        <div class="backtrang_img"><img src="{{url('frontend/images/icon_price/icon7.png')}}"></div>
                                        <span class="spanchui textchucnang9">Email Doanh Nghiệp</span> <q></q>
                                        <div class="divkhiiconihover textchucnang9_note">Email theo Tên miền Doanh nghiệp. VD: info@Cresa.com</div>
                                    </div>
                                    <div class="tieudenho">
                                        <div class="backtrang_img"><img src="{{url('frontend/images/icon_price/icon8.png')}}"></div>
                                        <span class="spanchui textchucnang10">Ngôn Ngữ</span> <q></q>
                                        <div class="divkhiiconihover textchucnang10_note">Ngôn ngữ hiển thị trên 1 Website.</div>
                                    </div>
                                    <div class="tieudenho">
                                        <div class="backtrang_img"><img src="{{url('frontend/images/icon_price/icon13.png')}}"></div>
                                        <span class="spanchui textchucnang15">SSL/HTTPS</span> <q></q>
                                        <div class="divkhiiconihover textchucnang15_note">Đây là một tiêu chuẩn công nghệ an ninh, bảo mật Website toàn cầu, giúp tạo ra một liên kết thông tin giữa máy chủ Web và trình duyệt Web được mã hoá và bảo mật an toàn. Hỗ trợ tốt cho SEO.</div>
                                    </div>
                                    <div class="tieudenho">
                                        <div class="backtrang_img"><img src="{{url('frontend/images/icon_price/icon17.png')}}"></div>
                                        <span class="spanchui textchucnang19">Thanh toán trực tuyến</span> <q></q>
                                        <div class="divkhiiconihover textchucnang19_note">Hỗ trợ tích hợp các cổng thanh toán trực tuyến phổ biến hiện nay.</div>
                                    </div>
                                    <div class="tieudenho">
                                        <div class="backtrang_img"><img src="{{url('frontend/images/icon_price/icon18.png')}}"></div>
                                        <span class="spanchui textchucnang20">Công cụ Chat Online</span> <q></q>
                                        <div class="divkhiiconihover textchucnang20_note">Công cụ cho phép Khách hàng có thể giao tiếp, kết nối hay gửi yêu cầu hỗ trợ, tư vấn đến Shop, hay Doanh nghiệp ngay trên Website.</div>
                                    </div>
                                    <div class="tieudenho">
                                        <div class="backtrang_img"><img src="{{url('frontend/images/icon_price/icon23.png')}}"></div>
                                        <span class="spanchui textchucnang25">Chi tiết gói dịch vụ</span> <q></q>
                                        <div class="divkhiiconihover textchucnang25_note">Các điều kiện và điều khoản sử dụng Dịch vụ tại Cresa.</div>
                                    </div>
                                    <div class="dangkyngay">
                                        <div class="backtrang_img"><img src="{{url('frontend/images/icon_price/icondangkyngay.png')}}"></div>
                                        <span class="spanchui"><div class="dangkyngay2" href="Đăng ký ngay">Đăng ký ngay</div></span>
                                    </div>
                                </div>
                            </div>
                            <div class="phai col-md-9">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="nendo">
                                            <div class="tieudedo">GÓI STARTUP</div>
                                            <div class="giagoiweb">
                                                <div class="giacu startupgiacu">2.990K</div>
                                                <div class="giamoi startupgiamoi">1.990K</div>
                                            </div>
                                            <div class="icontieudedo"></div>
                                        </div>
                                        <div class="bodynendo_respon">
                                            <div class="bodynendo col-xs-6 col-sm-12">
                                                <div class="tieudenho bolddamchu">
                                                    <span class="spanchui startup1">83K/tháng</span>
                                                </div>
                                                <div class="tieudenho">
                                                    <span class="spanchui startup2">Từ 1 - 3 ngày</span>
                                                </div>

                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui startup20"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                    </div>
                                                    <div class="divkhiiconihover startup20_note">Sau khi hoàn thành dự án, Cresa hỗ trợ bàn giao toàn bộ Source Code Website cho Quý Khách hàng khi nhận được yêu cầu (Có xác nhận bằng văn bản giữa 2 bên)</div>
                                                </div>

                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui startup21">Vĩnh Viễn</span>
                                                        
                                                    </div>
                                                    <div class="divkhiiconihover startup21_note">Cresa bảo hành vĩnh viễn trong suốt quá trình sử dụng đối với các Source Code do Công Ty thiết kế và chạy trên Hosting của Công Ty. Cresa sẽ không áp dụng chính sách bảo hành vĩnh viễn đối với các trường hợp Công Ty đã bàn giao toàn bộ Source Code cho Quý khách.</div>
                                                </div>

                                                <div class="tieudenho">
                                                    <span class="spanchui startup11"><img src="{{url('frontend/images/icon_price/daux.png')}}"></span>
                                                </div>

                                                <div class="tieudenho" style="height: 75px;">
                                                    <div>
                                                        <span class="spanchui startup4">Chọn mẫu có sẵn trong <br><a href="javascript:;" data-toggle="modal" data-target="#ModalSetupTuvan">Liện hệ</a></span>
                                                        
                                                    </div>
                                                    <div class="divkhiiconihover startup4_note">Chọn mẫu giao diện Website có sẵn trong kho Mẫu web 500K</div>
                                                </div>

                                                <div class="tieudenho tieudequatang">
                                                    <span class="spanchui startup22"><img src="{{url('frontend/images/icon_price/daux.png')}}"></span>
                                                </div>

                                                <div class="tieudenho">
                                                    <span class="spanchui startup13"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                </div>

                                                <div class="tieudenho">
                                                    <span class="spanchui startup15">Theo mẫu Demo <br><a href="{{url('kho-giao-dien/'.'all'.'.html')}}" data-toggle="modal" data-target="#ModalSetupTuvan">Liên hệ</a></span>
                                                </div>

                                                <div class="tieudenho">
                                                    <span class="spanchui startup5"><a href="https://thietke.Cresa.com/ten-mien-gia-goc">Tên miền giá gốc</a></span>
                                                </div>

                                                <div class="tieudenho">
                                                    <span class="spanchui startup6">1.5 GB</span>
                                                </div>
                                                <div class="tieudenho">
                                                    <span class="spanchui startup7">30 GB</span>
                                                </div>
                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui startup8"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                    </div>
                                                    <div class="divkhiiconihover startup8_note">10 Email theo Hosting</div>
                                                </div>
                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui startup9">Tiếng Việt</span>
                                                    </div>
                                                </div>
                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui startup14"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                    </div>
                                                    <div class="divkhiiconihover startup14_note">Miễn phí (SSL Let's Encrypt)</div>
                                                </div>

                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui startup18"><img src="{{url('frontend/images/icon_price/daux.png')}}"></span>
                                                    </div>
                                                    <div class="divkhiiconihover startup18_note">Chưa tích hợp cổng thanh toán</div>
                                                </div>
                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui startup19"><img src="{{url('frontend/images/icon_price/daux.png')}}"></span>
                                                    </div>
                                                    <div class="divkhiiconihover startup19_note">Chưa tích hợp công cụ Chat Online</div>
                                                </div>

                                                <div class="tieudenho">
                                                    <span class="spanchui"><a href="{{route('packageStartup')}}" class="startup24">Chi tiết >>></a></span>
                                                </div>
                                                <div class="dangkyngay">
                                                    <span class="spanchui"><a href="javascript:;" class="startupbuttontext" data-toggle="modal" data-target="#ModalSetupTuvan">Đăng ký</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="nendo">
                                            <div class="chuhot">HOT</div>
                                            <div class="tieudedo protengoi">GÓI PRO</div>
                                            <div class="giagoiweb">
                                                <div class="giacu progiacu">5.990K</div>
                                                <div class="giamoi progiamoi">4.890K</div>
                                            </div>
                                            <div class="icontieudedo"></div>
                                        </div>
                                        <div class="bodynendo_respon">
                                            <div class="bodynendo col-xs-6 col-md-12">
                                                <div class="tieudenho bolddamchu">
                                                    <span class="spanchui pro1">150K/tháng</span>
                                                </div>
                                                <div class="tieudenho">
                                                    <span class="spanchui pro2">Từ 3 - 5 ngày</span>
                                                </div>
                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui pro20"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                        
                                                    </div>
                                                    <div class="divkhiiconihover pro20_note">Sau khi hoàn thành dự án, Cresa hỗ trợ bàn giao toàn bộ Source Code Website cho Quý Khách hàng khi nhận được yêu cầu (Có xác nhận bằng văn bản giữa 2 bên)</div>
                                                </div>
                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui pro21">Vĩnh Viễn</span>
                                                        
                                                    </div>
                                                    <div class="divkhiiconihover pro21_note">Cresa bảo hành vĩnh viễn trong suốt quá trình sử dụng đối với các Source Code do Công Ty thiết kế và chạy trên Hosting của Công Ty. Cresa sẽ không áp dụng chính sách bảo hành vĩnh viễn đối với các trường hợp Công Ty đã bàn giao toàn bộ Source Code cho Quý khách.</div>
                                                </div>
                                                <div class="tieudenho">
                                                    <span class="spanchui pro11"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                </div>
                                                <div class="tieudenho" style="height: 75px;">
                                                    <div>
                                                        <span class="spanchui pro4">Chọn mẫu có sẵn trong <br>kho <a href="{{url('kho-giao-dien/'.'all'.'.html')}}">giao diện</a></span>
                                                        
                                                    </div>
                                                    <div class="divkhiiconihover pro4_note">Chọn mẫu giao diện Website có sẵn trong kho giao diện.</div>
                                                </div>

                                                <div class="tieudenho tieudequatang">
                                                    <span class="spanchui pro22"><img src="{{url('frontend/images/icon_price/daux.png')}}"></span>
                                                </div>
                                                <div class="tieudenho">
                                                    <span class="spanchui pro13"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                </div>

                                                <div class="tieudenho">
                                                    <span class="spanchui pro15">Theo mẫu Demo <br><a href="{{url('kho-giao-dien/'.'all'.'.html')}}">kho giao diện</a></span>
                                                </div>

                                                <div class="tieudenho">
                                                    <span class="spanchui pro5"><a href="https://thietke.Cresa.com/ten-mien-gia-goc">Tên miền giá gốc</a></span>
                                                </div>

                                                <div class="tieudenho">
                                                    <span class="spanchui pro6">3 GB</span>
                                                </div>
                                                <div class="tieudenho">
                                                    <span class="spanchui pro7">50 GB</span>
                                                </div>
                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui pro8"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                    </div>
                                                    <div class="divkhiiconihover pro8_note">Không giới hạn Email theo Hosting</div>
                                                </div>
                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui pro9">Tiếng Việt</span>
                                                    </div>
                                                </div>
                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui pro14"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                        
                                                    </div>
                                                    <div class="divkhiiconihover pro14_note">Miễn phí (SSL Let's Encrypt)</div>
                                                </div>

                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui pro18"><img src="{{url('frontend/images/icon_price/daux.png')}}"></span>
                                                        
                                                    </div>
                                                </div>
                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui pro19"><img src="{{url('frontend/images/icon_price/daux.png')}}"></span>
                                                        
                                                    </div>
                                                    <div class="divkhiiconihover pro19_note">Chưa tích hợp công cụ Chat Online</div>
                                                </div>

                                                <div class="tieudenho">
                                                    <span class="spanchui"><a href="{{route('packageCombopro')}}" class="pro24">Chi tiết >>></a></span>
                                                </div>
                                                

                                                <div class="dangkyngay">
                                                    <span class="spanchui"><a href="{{url('kho-giao-dien/'.'all'.'.html')}}" class="probuttontext">Đăng ký</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="nendo">
                                            <div class="chunew">NEW</div>
                                            <div class="tieudedo ecommercetengoi">GÓI ECOMMERCE</div>
                                            <div class="giagoiweb">
                                                <div class="giacu ecommercegiacu">9.990K</div>
                                                <div class="giamoi ecommercegiamoi">5.990K</div>
                                            </div>
                                            <div class="icontieudedo"></div>
                                        </div>
                                        <div class="bodynendo_respon">
                                            <div class="bodynendo col-xs-6 col-md-12">
                                                <div class="tieudenho bolddamchu">
                                                    <span class="spanchui ecommerce1">150K/tháng</span>
                                                </div>
                                                <div class="tieudenho">
                                                    <span class="spanchui ecommerce2">5 - 7 ngày</span>
                                                </div>
                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui ecommerce20"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                    </div>
                                                    <div class="divkhiiconihover ecommerce20_note">Sau khi hoàn thành dự án, Cresa hỗ trợ bàn giao toàn bộ Source Code Website cho Quý Khách hàng khi nhận được yêu cầu (Có xác nhận bằng văn bản giữa 2 bên)</div>
                                                </div>
                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui ecommerce21">Vĩnh Viễn</span>
                                                    </div>
                                                    <div class="divkhiiconihover ecommerce21_note">Cresa bảo hành vĩnh viễn trong suốt quá trình sử dụng đối với các Source Code do Công Ty thiết kế và chạy trên Hosting của Công Ty. Cresa sẽ không áp dụng chính sách bảo hành vĩnh viễn đối với các trường hợp Công Ty đã bàn giao toàn bộ Source Code cho Quý khách.</div>
                                                </div>
                                                <div class="tieudenho">
                                                    <span class="spanchui ecommerce11"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                </div>
                                                <div class="tieudenho" style="height: 75px;">
                                                    <div>
                                                        <span class="spanchui ecommerce4">Chọn mẫu có sẵn trong <br>kho <a href="{{url('kho-giao-dien/'.'all'.'.html')}}">giao diện</a></span>
                                                        
                                                    </div>
                                                    <div class="divkhiiconihover ecommerce4_note">Chọn mẫu giao diện Website có sẵn trong kho giao diện.</div>
                                                </div>

                                                <div class="tieudenho tieudequatang">
                                                    <span class="spanchui business13"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                </div>

                                                <div class="tieudenho">
                                                    <span class="spanchui ecommerce13"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                </div>

                                                <div class="tieudenho">
                                                    <span class="spanchui ecommerce15">Theo mẫu Demo <br><a href="{{url('kho-giao-dien/'.'all'.'.html')}}">kho giao diện</a></span>
                                                </div>

                                                <div class="tieudenho">
                                                    <span class="spanchui ecommerce5"><a href="https://thietke.Cresa.com/ten-mien-gia-goc">Tên miền giá gốc</a></span>
                                                </div>

                                                <div class="tieudenho">
                                                    <span class="spanchui ecommerce6">3 GB</span>
                                                </div>
                                                <div class="tieudenho">
                                                    <span class="spanchui ecommerce7">80 GB</span>
                                                </div>
                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui ecommerce8"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                        
                                                    </div>
                                                    <div class="divkhiiconihover ecommerce8_note">Không giới hạn Email theo Hosting</div>
                                                </div>
                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui ecommerce9">Tiếng Anh & Tiếng Việt</span>
                                                        
                                                    </div>
                                                </div>


                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui ecommerce14"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                        
                                                    </div>
                                                    <div class="divkhiiconihover ecommerce14_note">Miễn phí (SSL Let's Encrypt)</div>
                                                </div>

                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui ecommerce18"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                        
                                                    </div>
                                                    <div class="divkhiiconihover ecommerce18_note">Tích hợp 1 cổng thanh toán trực tuyến bằng PayPal</div>
                                                </div>
                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui ecommerce19"><img src="{{url('frontend/images/icon_price/daux.png')}}"></span>
                                                        
                                                    </div>
                                                    <div class="divkhiiconihover ecommerce19_note">Chưa tích hợp công cụ Chat Online</div>
                                                </div>

                                                <div class="tieudenho">
                                                    <span class="spanchui"><a href="{{route('packageEcommerce')}}" class="ecommerce24">Chi tiết >>></a></span>
                                                </div>

                                                <div class="dangkyngay">
                                                    <span class="spanchui"><a href="{{url('kho-giao-dien/'.'all'.'.html')}}" class="ecommercebuttontext">Đăng ký</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="nendo">
                                            <div class="tieudedo businesstengoi">GÓI BUSINESS</div>
                                            <div class="giagoiweb">
                                                <div class="giacu businessgiacu">11.100K</div>
                                                <div class="giamoi businessgiamoi">8.990K</div>
                                            </div>
                                            <div class="icontieudedo"></div>
                                        </div>
                                        <div class="bodynendo_respon">
                                            <div class="bodynendo col-xs-6 col-md-12">
                                                <div class="tieudenho bolddamchu">
                                                    <span class="spanchui business1">325K/tháng</span>
                                                </div>
                                                <div class="tieudenho">
                                                    <span class="spanchui business2">7 ngày</span>
                                                </div>
                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui business20"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                        
                                                    </div>
                                                    <div class="divkhiiconihover business20_note">Sau khi hoàn thành dự án, Cresa hỗ trợ bàn giao toàn bộ Source Code Website cho Quý Khách hàng khi nhận được yêu cầu (Có xác nhận bằng văn bản giữa 2 bên)</div>
                                                </div>
                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui business21">Vĩnh Viễn</span>
                                                        
                                                    </div>
                                                    <div class="divkhiiconihover business21_note">Cresa bảo hành vĩnh viễn trong suốt quá trình sử dụng đối với các Source Code do Công Ty thiết kế và chạy trên Hosting của Công Ty. Cresa sẽ không áp dụng chính sách bảo hành vĩnh viễn đối với các trường hợp Công Ty đã bàn giao toàn bộ Source Code cho Quý khách.</div>
                                                </div>
                                                <div class="tieudenho">
                                                    <span class="spanchui business11"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                </div>
                                                <div class="tieudenho" style="height: 75px;">
                                                    <div>
                                                        <span class="spanchui business4">Chọn mẫu có sẵn trong <br>kho <a href="{{url('kho-giao-dien/'.'all'.'.html')}}">giao diện</a></span>
                                                        
                                                    </div>
                                                    <div class="divkhiiconihover business4_note">Chọn mẫu giao diện Website có sẵn trong kho giao diện. Hỗ trợ chỉnh sửa giao diện Website 1 lần dựa trên mẫu ban đầu.</div>
                                                </div>
                                                <div class="tieudenho tieudequatang">
                                                    <span class="spanchui business13"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                </div>

                                                <div class="tieudenho">
                                                    <span class="spanchui business13"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                </div>
                                                <div class="tieudenho">
                                                    <span class="spanchui business15">Theo mẫu Demo <br><a href="{{url('kho-giao-dien/'.'all'.'.html')}}">kho giao diện</a></span>
                                                </div>

                                                <div class="tieudenho">
                                                    <span class="spanchui business5"><a href="https://thietke.Cresa.com/ten-mien-gia-goc">Tên miền giá gốc</a></span>
                                                </div>

                                                <div class="tieudenho">
                                                    <span class="spanchui business6">6 GB</span>
                                                </div>
                                                <div class="tieudenho">
                                                    <span class="spanchui business7">300 GB</span>
                                                </div>
                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui business8"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                        
                                                    </div>
                                                    <div class="divkhiiconihover business8_note">Dịch vụ Email Server riêng, tạo được 5 Email</div>
                                                </div>
                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui business9">Không giới hạn</span>
                                                    </div>
                                                </div>

                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui business14"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                        
                                                    </div>
                                                    <div class="divkhiiconihover business14_note">Miễn phí (SSL Let's Encrypt)</div>
                                                </div>

                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui business18"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                        
                                                    </div>
                                                    <div class="divkhiiconihover business18_note">Tích hợp 3 cổng thanh toán trực tuyến bằng PayPal, Ngân Lượng, Bảo Kim.</div>
                                                </div>
                                                <div class="tieudenho">
                                                    <div>
                                                        <span class="spanchui business19"><img src="{{url('frontend/images/icon_price/check.png')}}"></span>
                                                        
                                                    </div>
                                                    <div class="divkhiiconihover business19_note">Tích hợp công cụ Chat Online miễn phí 1 năm (Khi nhận được yêu cầu tích hợp từ Quý Khách hàng)</div>
                                                </div>

                                                <div class="tieudenho">
                                                    <span class="spanchui"><a href="{{route('packageBusiness')}}" class="business24">Chi tiết >>></a></span>
                                                </div>

                                                <div class="dangkyngay">
                                                    <span class="spanchui"><a href="{{url('kho-giao-dien/'.'all'.'.html')}}" class="businessbuttontext">Đăng ký</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        addLoadEvent(function() {
            var mySwiper = new Swiper('.swiper-container', {
                loop: true,
                slidesPerView: 1,
                effect: 'fade',
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });
        });
    </script>
</div>
@endsection