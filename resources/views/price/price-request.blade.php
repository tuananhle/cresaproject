@extends('layouts.master')
@section('title') Cresa báo giá theo yêu cầu khách hàng @endsection
@section('description') Với những website đòi hỏi những yêu cầu đặc biệt từ khách hàng, cresa sắn sàng đáp ứng @endsection 
@section('url') {{route('aboutUs')}} @endsection 
@section('keyword') Giao diện website, giao dien , website, giao dien websie chuan seo, giao dien web dep, thiết kết website theo yêu cầu @endsection
@section('image') {{ url('/').$setting->logo }} @endsection
@section('css')
    <link href="{{url('frontend/asset/css/about-us.min.css')}}" rel="preload" as="style" type="text/css" />
    <link href="{{url('frontend/asset/css/about-us.min.css')}}" rel="stylesheet" />
@endsection
@section('js')
@endsection
@section('content')
<div id="wrapper" class="clearfix">
    <div class="about-us">
       <div class="banner">
          <div class="container">
             <h1>Báo giá thiết kế website theo yêu cầu</h1>
             <p>Với những website đòi hỏi những yêu cầu đặc biệt từ khách hàng, cresa sắn sàng đáp ứng</p>
          </div>
       </div>
       <div class="lookback">
          <div class="container">
             <p>Quy trình thiết kế</p>
             <ul>
                <li class="item-1">
                   <span class="icon"></span>
                   <p><strong>1. PHÂN TÍCH YÊU CẦU</strong>Sau khi nhận đây đủ các yêu cầu thiết kế website đầy đủ, chúng tôi<br class="d-none d-xl-block">tiến hành phân tích yêu cầu kỹ thuật, phân tích hệ thống, lên kế <br class="d-none d-xl-block">hoạch thực hiện dự án.</p>
                </li>
                <li class="item-2">
                  <span class="icon"></span>
                  <p><strong>2. BÁO GIÁ & KÝ HỢP ĐỒNG</strong>Tổng hợp bản yêu cầu xây dựng webiste, chi tiết kế hoạch thực hiện
                  dự án và gửi báo giá cho khách hàng. Sau đó ký hợp đồng thiết kế
                  webiste.</p>
               </li>
                <li class="item-3">
                   <span class="icon"></span>
                   <p><strong>3. THIẾT KẾ DEMO</strong>Dựa vào yêu cầu về logo, ý nghĩa website cần thể hiện, lĩnh vực,
                    các tông màu khách hàng yêu cầu. Chúng tôi sẽ lên thiết kế demo
                    đảm bảo giao diện đáp ứng đươc các yêu cầu đề ra.</p>
                </li>
                <li class="item-5">
                   <span class="icon"></span>
                   <p><strong>4. LẬP TRÌNH WEBSITE</strong>Sau khi bản thiết kế được duyệt. Chúng tôi tiến hành lập
                    trình cơ sở dữ liệu dựa trên thiết kế. Soạn thảo nội dung tài liệu, chỉnh
                    sửa hoàn thiện nội dung rồi đưa lên trang web.</p>
                </li>
                <li class="item-7">
                   <span class="icon"></span>
                   <p><strong>5. CHẠY THỬ BẢN BETA</strong>Kiểm tra và sửa lỗi đảm bảo không sai lệch với thiết kế và phần mềm
                    hoạt động tốt. Chạy thử hệ thống. Khách hàng kiểm thử các tính
                    năng đã xây dựng và yêu cầu chỉnh sửa nếu cần.</p>
                </li>
                <li class="item-10">
                   <span class="icon"></span>
                   <p><strong>6. BÀN GIAO NGHIỆM THU</strong>Một khi bạn hoàn toàn hài lòng với websiste của mình, chúng tôi sẽ tiến
                    hành chuyển giao sản phẩm cùng các tài liệu liên quan.</p>
                </li>
                <li class="item-11">
                   <span class="icon"></span>
                   <p><strong>7. BẢO TRÌ WEBSITE</strong>Chúng tôi sẽ bảo trì website trong thời hạn bảo hành và hỗ trợ khách
                    hàng trong quá trình sử dụng và quản tri website.</p>
                </li>
             </ul>
          </div>
          <div class="block-img d-none d-xl-block">
          <img src="{{url('frontend/images/lookback-1200.jpg')}}" alt="Cùng nhìn lại chặng đường 10 năm phát triển của Sapo" class="fade show">
          </div>
          <div class="block-img d-none d-lg-block d-xl-none">
          <img src="{{url('frontend/images/lookback-960.jpg')}}" alt="Cùng nhìn lại chặng đường 10 năm phát triển của Sapo">
          </div>
          <div class="block-img d-none d-md-block d-lg-none">
          <img src="{{url('frontend/images/lookback-768.png')}}" alt="Cùng nhìn lại chặng đường 10 năm phát triển của Sapo">
          </div>
       </div>
       <div class="reason">
          <a href="javascript:;" class="btn-registration" data-toggle="modal" data-target="#ModalSetupTuvan">Yêu cầu tư vấn</a><br>
       </div>
    </div>
    <script>
       addLoadEvent(function () {
           var mySwiper = new Swiper('.swiper-container', {
               loop: true,
               slidesPerView: 1,
               effect: 'fade',
               pagination: {
                   el: '.swiper-pagination',
                   clickable: true,
               },
               navigation: {
                   nextEl: '.swiper-button-next',
                   prevEl: '.swiper-button-prev',
               },
           });
       });
    </script>
 </div>
@endsection