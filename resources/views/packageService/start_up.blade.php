@extends('layouts.master')
@section('title') Startup | Cresa gói dịch vụ Startup, giải pháp tốt nhất cho doanh nghiệp mới mở @endsection
@section('description') Cresa gói dịch vụ Startup, giải pháp tốt nhất cho doanh nghiệp mới mở @endsection 
@section('url') {{route('packageStartup')}} @endsection
@section('keyword') Thiết kế website giá rẻ chỉ từ 1.000.000đ , thiết kế website chuyên nghiệp, nhanh chóng @endsection
@section('image') {{ url('/').$setting->logo }} @endsection
@section('css')
    {{-- <link href="{{url('frontend/asset/css/index.min.css')}}" rel="preload" as="style" type="text/css" />
    <link href="{{url('frontend/asset/css/popup-register-default.min.css')}}" rel="stylesheet" /> --}}
@endsection
@section('js')
    <link href="{{url('frontend/asset/css/popup-register-default.min.css')}}" rel="stylesheet">
    <script src="{{url('frontend/asset/js/script.js')}}"></script>
    <script src="{{url('frontend/asset/js/popup-trial.min.js')}}"></script>
@endsection
@section('content')
<div id="wrapper" class="default-wrapper">
    <div class="detail-container">
       <div class="theme-intro responsive">
       </div>
       <div class="theme-detail">
          <div class="container">
             <div class="row">
                <div class="col-lg-12">
                   <div class="detail-feature">
                    <div id="content" class="col-sm-12 information_id_is_q_19">      
      <p align="center"><h1 style="font-size: 21px; text-align: center;">Điều kiện và điều khoản sử dụng gói thiết kế Web Combo Startup</h1></p>

<p>&nbsp;</p>

<ol>
  <li><strong>Thời gian kích hoạt dịch vụ</strong></li>
</ol>

<p style="margin-left:.25in;">Dịch vụ sẽ được kích hoạt trong vòng 1 đến 3 ngày kể từ khi Cresa tiếp nhận yêu cầu chọn mẫu từ Quý khách hàng và Thanh toán đủ 100% chi phí cho gói Startup theo hình thức chuyển khoản ngân hàng.</p>

<ol>
  <li value="2"><strong>Giao diện Website cho gói Combo Startup</strong></li>
</ol>

<p style="margin-left:.25in;">Chọn các mẫu giao diện có sẵn trong kho <a href="">Template</a>. Những mẫu giao diện ngoài kho <a href="">Template</a> dù có chức năng tương đương cũng không được áp dụng cho gói Combo Startup. Gói Web không hỗ trợ điều chỉnh giao diện Web mẫu miễn phí theo yêu cầu. Tất cả các hạng mục khi phát sinh nhu cầu điều chỉnh lại sẽ phát sinh chi phí cho từng hạng mục cụ thể.</p>

<ol>
  <li value="3"><strong>Hỗ trợ hoàn thiện Website</strong></li>
</ol>

<p style="margin-left:.25in;">Cresa hỗ trợ Quý khách hàng miễn phí hoàn thiện Website cho đến khi Quý khách hàng có thể tự quản trị được web không&nbsp;giới hạn thời gian&nbsp;(không tính ngày nghỉ Thứ 7 và Chủ Nhật).</p>

<p style="margin-left:.25in;"><strong>Các hạng mục được hỗ trợ hoàn thiện Website miễn phí bao gồm:</strong></p>

<ul style="list-style-type:circle;">
  <li>Hướng dẫn Quý Khách hàng thay đổi&nbsp;Logo, các Banner Quảng Cáo, Slideshow, Header, Footer Website, Menu chính.</li>
  <li>Hướng dẫn Quý Khách hàng cập nhật các Trang thông tin hoặc Tin tức theo nội dung Quý khách hàng&nbsp;muốn trình bày lên Website.</li>
  <li>Hướng dẫn Quý Khách hàng cập nhật&nbsp;các Sản phẩm mẫu (bao gồm thông tin sản phẩm, hình ảnh sản phẩm, thuộc tính sản phẩm...)</li>
  <li>Hướng dẫn Quý Khách&nbsp;hàng cập nhật&nbsp;các Danh mục sản phẩm, Danh mục tin tức theo nhu&nbsp;cầu Quý khách.</li>
</ul>

<p style="margin-left:.25in;"><span style="font-size:16px;"><span style="color:#FF0000;"><strong>&gt;&gt; Lưu ý:</strong></span></span>&nbsp;Cresa không làm thay công việc của Quý khách hàng, mà cùng đồng hành, hướng dẫn quý Khách hàng thực hiện các công việc liên quan đến việc sử dụng và quản trị Web. Chúng tôi tin rằng, thông qua cách làm này, kĩ năng sử dụng cũng như quản trị Website của Quý khách hàng sẽ được nâng cao, hỗ trợ đắc lực cho công việc vận hành và phát triển Website trong tương lai của Quý khách rất nhiều.</p>

<p style="margin-left:.25in;"><strong>Các hạng mục không được hỗ trợ hoàn thiện Website miễn phí bao gồm:</strong></p>

<ul style="list-style-type:circle;">
  <li>Không hỗ trợ tìm kiếm hình ảnh, thông tin Demo theo yêu cầu.</li>
  <li>Không hỗ trợ thiết kế, chỉnh sửa hình ảnh như (Resize hình ảnh, chỉnh độ sắc nét, …)</li>
</ul>

<p style="margin-left:.25in;">Sau thời gian quy định 3 ngày được hỗ trợ miễn phí hoàn thiện Website theo điều khoản sử&nbsp;dụng của gói Combo Startup, mọi yêu cầu hỗ trợ điều chỉnh hay hoàn thiện Website sẽ phát sinh chi phí hỗ trợ từ 300K đến 500K tuỳ hạng mục công việc.</p>

<ol>
  <li value="4"><strong>Gói dịch vụ Combo Startup bao gồm</strong></li>
</ol>

<ul style="list-style-type:circle;">
  <li>Tặng miễn phí gói Hosting năm đầu tiên: Dung lượng lưu trữ 1.5GB. Băng thông 30GB.</li>
  <li>Miễn phí 10 Email theo Hosting.</li>
  <li>Website có mặc định chỉ 1 ngôn ngữ Tiếng Việt.</li>
  <li>Chưa hỗ trợ tính năng tương thích di động.</li>
  <li>Website được lập trình chuẩn SEO, hỗ trợ Quý khách hàng làm SEO cho Website.</li>
  <li>Hỗ trợ tích hợp SSL miễn phí từ Let’s Encrypt khi có yêu cầu từ Quý khách hàng.</li>
  <li>Các chức năng Website theo mẫu Quý khách hàng đã chọn. Mọi trường hợp thêm tính năng hoặc bỏ bớt tính năng mà mẫu Website có sẵn sẽ phát sinh chi phí hỗ trợ.</li>
  <li>Tài liệu hướng dẫn Quý khách hàng sử dụng và Quản trị Website.</li>
  <li>Chưa tích hợp cổng thanh toán trực tuyến. Nếu Quý khách hàng có yêu cầu tích hợp sẽ phát sinh chi phí thực hiện.</li>
  <li>Chưa tích hợp công cụ Live chat trực tuyến trên Website. Nếu Quý khách hàng có yêu cầu tích hợp sẽ phát sinh chi phí thực hiện.</li>
</ul>

<ol>
  <li value="5"><strong>Liên hệ hỗ trợ kĩ thuật</strong></li>
</ol>

<p style="margin-left:.25in;">Với gói Startup Quý khách hàng có thể liên hệ Cresa để hoàn thiện Website trong vòng 3 ngày qua các hình thức Email hoặc Ticket. Sau thời gian hoàn thiện Website mọi yêu cầu hỗ trợ về kĩ thuật hoặc quản trị Website Quý khách hàng đều liên hệ qua kênh Email hoặc Ticket.</p>

<ol>
  <li value="6"><strong>Bàn giao Source Code (Mã nguồn Website)</strong></li>
</ol>

<p style="margin-left:.25in;">Sau khi hoàn thành dự án thiết kế Web, Cresa hỗ trợ bàn giao toàn bộ Source Code Website cho Khách hàng khi nhận được yêu cầu.</p>

<p style="margin-left:.25in;"><strong>&gt;&gt;</strong> <strong>Điều kiện:</strong> Có xác nhận bằng văn bản giữa 2 bên trước khi Cresa thực hiện bàn giao Source Code.</p>

<ol>
  <li value="7"><strong>Bảo hành Website.</strong></li>
</ol>

<ul style="list-style-type:circle;">
  <li>Cresa sẽ hỗ trợ khôi phục dữ liệu và giải quyết những vấn đề tự phát sinh từ Source Code miễn phí khi Quý khách hàng gặp trục trặc trong quá trình sử dụng Website.</li>
  <li>Bảo hành Website miễn phí và vĩnh viễn trong điều kiện khách hàng sử dụng dịch vụ Hosting của Cresa và không lấy Source Code cũng như Không can thiệp vào bộ Source gốc ban đầu. Đồng thời trên Footer Website có hiển thị dòng “Thiết kế website bởi Cao Tốc”.</li>
  <li>Quý khách hàng sử dụng Hosting bên ngoài Cresa sẽ hỗ trợ bảo hành Source Code 12 tháng, nếu lỗi phát sinh từ phía Cresa. Xác định nguyên nhân lỗi theo hình thức đối chiếu với Source gốc ban đầu.</li>
  <li>Nếu Quý khách hàng xóa dòng “Thiết kế website bởi Cao Tốc”, Cresa sẽ từ chối hỗ trợ bảo hành bảo trì website. Nếu muốn được hỗ trợ Quý khách hàng phải đóng phí bảo hành duy trì 500K/1 năm.</li>
</ul>

<ol>
  <li value="8"><strong>Gia hạn.</strong></li>
</ol>

<ul style="list-style-type:circle;">
  <li>Chi phí gia hạn mỗi năm: 1.089.000 đồng Hosting (990K + 10% VAT).</li>
  <li>Tên miền (Domain) và Hosting là dịch vụ gia hạn bắt buộc. Những dịch vụ còn lại khách hàng có thể gia hạn hoặc không tùy theo nhu cầu.</li>
</ul>

<ol>
  <li value="9"><strong>Phí Upload Source Code lên Hosting đơn vị khác.</strong></li>
</ol>

<p style="margin-left:.25in;">Cresa hỗ trợ Quý khách hàng có nhu cầu Upload Source do CTY thiết kế lên Hosting các đơn vị khác, không phân biệt Hosting trong nước hay nước ngoài với chi phí hỗ trợ 1 lần Upload Source Code là 500k.</p>

<ol>
  <li value="10"><strong>&nbsp;Thanh toán dài hạn</strong></li>
</ol>

<p style="margin-left:.25in;">Khi Quý khách hàng có nhu cầu thanh toán dài hạn từ 2 năm trở lên. Cresa sẽ hỗ trợ phần trăm giảm giá theo từng năm cụ thể như sau:</p>

<ul style="list-style-type:circle;">
  <li>Thanh toán <strong>2</strong> năm: Được giảm giá <strong>10%</strong> trên tổng chi phí Gia hạn.</li>
  <li>Thanh toán <strong>3</strong> năm: Được giảm giá <strong>15%</strong> trên tổng chi phí Gia hạn.</li>
  <li>Thanh toán <strong>5</strong> năm: Được giảm giá <strong>30%</strong> trên tổng chi phí Gia hạn.</li>
</ul>

<ol>
  <li value="11"><strong>&nbsp;Quy trình hỗ trợ</strong></li>
</ol>

<ul style="list-style-type:circle;">
  <li>Tất cả thông tin về khách hàng bao gồm thông tin liên hệ, hợp đồng, gói website, các yêu cầu thống nhất ban đầu đều được lưu trữ trong hệ thống quản trị khách hàng của Cresa.com.</li>
  <li>Khi khách có yêu cầu cần hỗ trợ, khách tạo ticket gửi yêu cầu hoặc gửi vào địa chỉ email hotro@Cresa.com (đối với khách hỗ trợ qua hotline, nhân viên Cresa khi tiếp nhận sẽ tạo 1 ticket mới). Ngay khi tiếp nhận ticket, trong vòng 15 phút đến tối đa 4 tiếng làm việc, nhân viên Cresa sẽ phản hồi cho khách hàng về việc tiếp nhận và hướng xử lý hỗ trợ, bao gồm thời gian thực hiện trong vòng 24h đến 72h tuỳ yêu cầu của khách đơn giản hay phức tạp…</li>
</ul>

<p style="margin-left:.25in;"><strong>Những điểm cần lưu ý:</strong></p>

<ul style="list-style-type:circle;">
  <li>Vì lý do bảo mật, Cresa chỉ hỗ trợ chính chủ trang web và không hỗ trợ người lạ nếu chưa được xác thực. Khách hàng cần gửi văn bản uỷ quyền, nếu uỷ quyền cho người khác thay thế chính chủ đề làm việc, kể từ thời điểm đó, Cresa sẽ làm việc với người được uỷ quyền.</li>
  <li>Trường hợp khách hàng có nhóm làm việc nhiều người, phía khách hàng đề cử 1 trưởng nhóm để thống nhất thông tin và các yêu cầu để làm việc với Web500K. Cresa sẽ chỉ làm theo yêu cầu của trưởng nhóm mà không phải các thành viên khác, mọi thông tin sẽ được thông qua trưởng nhóm.</li>
  <li>Thực hiện trong vòng 24h đến 72h tùy yêu cầu của khách đơn giản hay phức tạp… các ticket đều được tiếp nhận và lên kế hoạch thực hiện theo thứ tự xếp hàng ai trước phục vụ trước.</li>
  <li>Nếu Quý khách có nhu cầu hỗ trợ gấp, quý khách có để đăng ký dịch vụ hỗ trợ VIP. Chúng tôi sẽ có kỹ thuật chuyên trách dự phòng thực hiện nhanh, thời gian giải quyết vấn đề từ 1h đến 4h làm việc.</li>
</ul>
</div>
                   </div>
                </div>
             </div>
          </div>
       </div>
       <div class="themes-related">
          <div class="container">
             <h3>Các giao diện mới nhất</h3>
             <div class="row">
                @foreach($newInterface as $item)
                <div class="col-lg-4 col-xl-3 col-md-6 col-sm-6 ">
                   <div class="theme-item responsive">
                     @php
                        $image = json_decode($item->images);
                     @endphp
                      <div class="theme-image">
                      <img src="{{$image[0]}}" alt="{{ $item->name}}">
                         <div class="theme-action">
                            <div class="button">
                               <a href="{{$item->link_demo}}" class="view-demo action-preview-theme" data-url="{{$item->link_demo}}" target="_blank">Xem thử</a>
                               <a href="{{url('chi-tiet/'.$item->id)}}" class="view-detail">Chi tiết</a>
                            </div>
                         </div>
                      </div>
                      <div class="theme-info">
                         <h3><a href="{{url('chi-tiet/'.$item->id)}}" class="title">{{ $item->name}}</a></h3>
                         <span class="price ">
                         <b>1,500,000 VNĐ</b>
                         </span>
                      </div>
                   </div>
                </div>
                @endforeach
             </div>
          </div>
       </div>
    </div>
    <div class="clear"></div>
<img class="scroll-top" src="{{url('frontend/images/totop.png')}}" style="display:none;cursor :pointer; position : fixed;bottom : 90px;right : 33px;z-index : 99999;">
    <script>
       $(document).ready(function () {
           $(window).scroll(function () {
               if ($(window).scrollTop() > 700) {
                   $('.scroll-top').show();
               }
               else {
                   $('.scroll-top').hide();
               }
           });
           $('.scroll-top').click(function () {
               $("html, body").animate({ scrollTop: 0 }, "slow");
           });
           if ($(window).width() < 768) {
               $('.scroll-top').css({ 'bottom': '90px', 'right': '8px' });
           }
       });
    </script>
    <div class="register-bottom clearfix">
       <div class="container">
          <h4>Bắt đầu dùng thử 15 ngày</h4>
          <p>Để trải nghiệm nền tảng quản lý và bán hàng đa kênh được sử dụng nhiều nhất Việt Nam</p>
          <div class="reg-form">
             <input id="site_name_bottom" class="input-site-name d-none d-md-inline-block" type="text" value="" placeholder="Nhập tên cửa hàng/doanh nghiệp của bạn">
             <a class="btn-registration banner-home-registration event-Sapo-Free-Trial-form-open"  href="javascript:;">Dùng thử miễn phí</a>
          </div>
       </div>
    </div>
    <script>
       $(function () {
           $("#free-trial .close").click(function () {
               var modal = document.getElementById('free-trial');
               $('body').removeClass('open');
               modal.style.display = "none";
               $("#free-trial iframe").attr("src", "");
           });
           $('[data-toggle="tooltip"]').tooltip();
       });
    </script>
    <script type="text/javascript">
       var LAST_STORE_COOKIE_NAME = "last_store";
       
       $(function () {
           var lastStore = getCookie(LAST_STORE_COOKIE_NAME);
           if (lastStore !== null && lastStore !== "") {
               $(".subdomain").removeClass("hide");
               $("#login-form input[name=Subdomain]").val(lastStore);
               $("#login-form input[name=Subdomain]").attr("aria-required", "true");
               $("#login-form input[name=Subdomain]").attr("data-val-required", "Nhập vào đường dẫn website");
               $("#login-form").removeData("validator");
               $("#login-form").removeData("unobtrusiveValidation");
               $.validator.unobtrusive.parse($("#login-form"));
           }
       });
       
       $(document).ready(function () {
           //luu thong tin tracking vao cookie
       
           var aff_id_ck = getCookie("aff_id");
           var aff_id = getParameterByName("aff_id");
           var aff_tracking_id = getParameterByName("aff_tracking_id");
           if (aff_id_ck == null || aff_id_ck == "") {
               if (aff_id !== null && aff_id !== "") {
                   setCookie("aff_id", aff_id, 30);
               }
       
               if (aff_tracking_id !== null && aff_tracking_id !== "") {
                   setCookie("aff_tracking_id", aff_tracking_id, 30);
               }
           }
           else {
               if (aff_id == aff_id_ck) {
                   if (aff_tracking_id !== null && aff_tracking_id !== "") {
                       setCookie("aff_tracking_id", aff_tracking_id, 30);
                   }
               }
           }
       
           var kd = getParameterByName("kd");
           if (kd !== null && kd !== "")
               setCookie("kd", kd, 30);
       
           var ref = getParameterByName("ref");
           if (ref !== null && ref !== "")
               setCookie("ref", ref, 30);
       
           var campaign = getParameterByName("campaign");
           if (campaign !== null && campaign !== "")
               setCookie("campaign", campaign, 30);
       
           if (document.referrer && document.referrer != '') {
               if (document.referrer.indexOf("www.sapo.vn") == -1) {
                   setCookie("referral", document.referrer, 30);
               }
           }
       
           var partner = getParameterByName("aff_partner_id");
           if (partner !== null && partner !== "")
               setCookie("partner", partner, 30);
       
           var landingPage = getCookie("landing_page");
           if (landingPage == null || landingPage == "") {
               setCookie("landing_page", document.location.href, 0.0115);
           }
       
           var startTime = getCookie("start_time");
           if (startTime == null || startTime == "") {
               setCookie("start_time", "12/26/2019 10:48:43", 0.0115);
           }
       
           var pageview = getCookie("pageview");
           if (pageview == null || pageview == "") {
               setCookie("pageview", 1, 0.0115);
           }
           else {
               setCookie("pageview", parseInt(pageview) + 1, 0.0115);
           }
       
           var updateCookie = setInterval(renewFirstPageCookie, 15 * 60 * 1000);
       
           //check orient change
           window.addEventListener("orientationchange", function () {
           }, false)
       
       });
       
       function showTrialForm(e, type, chkOmni) {
           var url = "https://app.sapo.vn/services/signup";
           url = setParameter(url, "Type", type);
       
           if (chkOmni) {
               url = setParameter(url, "PreferredService", "OMNI");
           }
       
           var storeName = $(e).parent().parent().find(".input-site-name").val();
           if (storeName != null && storeName != "")
               url = setParameter(url, "StoreName", storeName);
       
           var kd = getParameterByName("kd");
           if (kd !== null && kd !== "")
               setCookie("kd", kd, 30);
       
           kd = getCookie("kd");
           if (kd !== null && kd !== "")
               url = setParameter(url, "SaleName", kd);
       
           var ref = getParameterByName("ref");
           if (ref !== null && ref !== "")
               setCookie("ref", ref, 30);
       
           ref = getCookie("ref");
           if (ref !== null && ref !== "")
               url = setParameter(url, "Reference", ref);
       
           if (window.location.href && window.location.href != '') {
               url = setParameter(url, "Source", encodeURIComponent(window.location.href));
           }
       
           var referral = getCookie("referral");
           if (referral !== null && referral !== "")
               url = setParameter(url, "Referral", encodeURIComponent(referral));
       
           var campaign = getParameterByName("campaign");
           if (campaign !== null && campaign !== "")
               setCookie("campaign", campaign, 30);
       
           campaign = getCookie("campaign");
           if (campaign !== null && campaign !== "")
               url = setParameter(url, "Campaign", campaign);
       
           var landingPage = getCookie("landing_page");
           if (landingPage !== null && landingPage !== "")
               url = setParameter(url, "LandingPage", encodeURIComponent(landingPage));
       
           var startTime = getCookie("start_time");
           if (startTime !== null && startTime !== "")
               url = setParameter(url, "StartTime", encodeURIComponent(startTime));
       
           var endTime = "12/26/2019 10:48:43";
           if (endTime !== null && endTime !== "")
               url = setParameter(url, "EndTime", encodeURIComponent(endTime));
       
           var pageview = getCookie("pageview");
           if (pageview !== null && pageview !== "")
               url = setParameter(url, "Pageview", pageview);
       
           var aff_id = getCookie("aff_id");
           if (aff_id !== null && aff_id !== "") {
               url = setParameter(url, "AffId", aff_id);
           }
       
           var aff_tracking_id = getCookie("aff_tracking_id");
           if (aff_tracking_id !== null && aff_tracking_id !== "") {
               url = setParameter(url, "AffTrackingId", aff_tracking_id);
           }
       
           var partner = getCookie("partner");
           if (partner !== null && partner !== "") {
               url = setParameter(url, "partner", partner);
           }
       
           $('#free-trial iframe').attr("src", url);
           $('body').addClass('open');
           $('#free-trial').fadeIn('fast');
       }
       
       function renewFirstPageCookie() {
           var landingPage = getCookie("landing_page");
           if (landingPage !== null && landingPage !== "") {
               setCookie("landing_page", landingPage, 0.0115);
           }
       
           var startTime = getCookie("start_time");
           if (startTime !== null && startTime !== "") {
               setCookie("start_time", startTime, 0.0115);
           }
       
           var pageview = getCookie("pageview");
           if (pageview !== null || pageview !== "") {
               setCookie("pageview", pageview, 0.0115);
           }
       }
       
       function bodauTiengViet(str) {
           str = str.toLowerCase();
           str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
           str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
           str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
           str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
           str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
           str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
           str = str.replace(/đ/g, "d");
           return str;
       }
       
       var mobile = false;
       $(window).resize(function () {
           var ww = $(window).width();
           if (ww < 768) {
               if (!$('#login-div br').length > 0)
                   $('#login-div .popup-login-text').before('<br/>');
           }
           else {
               $('#login-div br').remove();
           }
       })
       $(window).trigger('resize');
       
       function getParameterByName(name) {
           name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
           var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
               results = regex.exec(location.search);
           return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
       }
       
       function onInputStoreName(e, element) {
           if (e.keyCode == 13) {
       
               return false;
           }
       }
       
       function generateAlias(text) {
           text = text.toLowerCase();
           text = text.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
           text = text.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
           text = text.replace(/ì|í|ị|ỉ|ĩ/g, "i");
           text = text.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
           text = text.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
           text = text.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
           text = text.replace(/đ/g, "d");
           text = text.replace(/'|\"|\(|\)|\[|\]/g, "");
           text = text.replace(/\W+/g, "-");
           if (text.slice(-1) === "-")
               text = text.replace(/-+$/, "");
       
           if (text.slice(0, 1) === "-")
               text = text.replace(/^-+/, "");
       
           return text;
       }
       
       function setCookie(cname, cvalue, exdays) {
           if (!exdays)
               exdays = 30;
       
           var d = new Date();
           d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
           var expires = "expires=" + d.toGMTString();
           document.cookie = cname + "=" + cvalue + "; " + expires + ";domain=.sapo.vn;path=/";
       }
       
       function newSetCookie(cname, cvalue) {
           document.cookie = cname + "=" + cvalue;
       }
       
       function getUrlWithoutDomain(url) {
           return url.replace(/^.*\/\/[^\/]+/, '');
       }
       
       function getCookie(cname) {
           var name = cname + "=";
           var ca = document.cookie.split(';');
           for (var i = 0; i < ca.length; i++) {
               var c = ca[i];
               while (c.charAt(0) == ' ') c = c.substring(1);
               if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
           }
           return null;
       }
       
       function getSessionStorage(sname) {
           return window.sessionStorage.getItem(sname);
       }
       
       function setSessionStorage(sname, svalue) {
           window.sessionStorage.setItem(sname, svalue);
       }
       
       function guid() {
           return 'xxxxxxxx-xxxx-4xxx-yxxx'.replace(/[xy]/g, function (c) {
               var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
               return v.toString(16);
           }).toUpperCase();
       }
       
       function setParameter(url, paramName, paramValue) {
           if (url.indexOf(paramName + "=") >= 0) {
               var prefix = url.substring(0, url.indexOf(paramName));
               var suffix = url.substring(url.indexOf(paramName));
               suffix = suffix.substring(suffix.indexOf("=") + 1);
               suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
               url = prefix + paramName + "=" + paramValue + suffix;
           }
           else {
               if (url.indexOf("?") < 0)
                   url += "?" + paramName + "=" + paramValue;
               else
                   url += "&" + paramName + "=" + paramValue;
           }
       
           return url;
       }
    </script>
 </div>
@endsection