@extends('layouts.master')
@section('title') Thông tin Cresa @endsection
@section('description') Cresa cung cấp dịch vụ thiết kế website chuyện nghiệp, website giá rẻ @endsection 
@section('url') {{route('aboutUs')}} @endsection 
@section('keyword') Giao diện website, giao dien , website, giao dien websie chuan seo, giao dien web dep @endsection
@section('image') {{ url('/').$setting->logo }} @endsection
@section('css')
    <link href="{{url('frontend/asset/css/about-us.min.css')}}" rel="preload" as="style" type="text/css" />
    <link href="{{url('frontend/asset/css/about-us.min.css')}}" rel="stylesheet" />
@endsection
@section('js')
@endsection
@section('content')
<div id="wrapper" class="clearfix">
    <div class="about-us">
       <div class="banner">
          <div class="container">
             <h1>Cresa - Công ty cung cấp website và các nền tảng quản lý được<br class="d-none d-xl-block">sử dụng nhiều nhất Việt Nam</h1>
             <p>Hãy để chúng tôi đồng hành cùng sự phát triển của doanh nghiệp bạn</p>
          </div>
       </div>
       <div class="introduce">
          <div class="container">
             <div class="row">
                <div class="col-lg-6">
                <img src="{{url('frontend/images/career-main.png')}}" alt="Về chúng tôi" class="fade show">
                </div>
                <div class="col-lg-6">
                   <h2>Về chúng tôi</h2>
                   <ul>
                      <li>Creasa là một công ty cung cấp các dịch vụ phát triển phần mềm và CNTT được thành lập vào năm 2019. Với 3 mục tiêu chính : trách nhiệm ( responsibility ) , hài lòng ( satisfied ) , tin tưởng ( credence ) .  Cresa được thành lập bởi 1 nhóm du học sinh Nhật Bản và Mỹ với chung 1 mục tiêu là mang những văn hoá,tinh hoa đã được học tập và tích luỹ tại nước ngoài về để xây dựng và phát triển đất nước. Giúp các doanh nghiệp to,vừa,nhỏ và các startup tiếp cận đến những công nghệ tốt nhất. Ngoài ra,với những kinh nghiệm của mình tại các nước phát triển mạnh như Nhật Bản,Mỹ,.. chúng tôi tự hào với một đội ngũ kỹ sư CNTT tài năng, phát triển mạnh mẽ và giải quyết các vấn đề CNTT của bạn và đáp ứng nhu cầu kinh doanh của bạn. </li>
                      <li>      
                        Luôn luôn lắng nghe,phân tích và thấu hiểu <br>
                        Đồng hành với sự phát triển của Quốc gia và Cộng đồng <br>
                        Khách hàng là trung tâm. Luôn luôn lắng nghe,phân tích và thấu hiểu <br>  
                        Cam kết sản phẩm và dịch vụ chất lượng nhất đến tay khách hàng
                     </li>
                      <li class="d-block d-lg-none d-xl-block">Cresa luôn cố gắng nỗ lực với mục tiêu cao là mang lại sự hài lòng cho các khách hàng thông qua những sản phẩm và giải pháp công nghệ tối ưu nhất vào bán hàng. Đồng thời, chúng tôi không ngừng nghiên cứu và tiên phong trong các giải pháp công nghệ mới góp phần khẳng định vị thế của mình.</li>
                   </ul>
                </div>
             </div>
             <div class="text-tablet d-none d-lg-block d-xl-none">Cresa luôn cố gắng nỗ lực với mục tiêu cao là mang lại sự hài lòng cho các khách hàng thông qua những sản phẩm và giải pháp công nghệ tối ưu nhất vào bán hàng. Đồng thời, chúng tôi không ngừng nghiên cứu và tiên phong trong các giải pháp công nghệ mới góp phần khẳng định vị thế của mình.</div>
          </div>
       </div>
       {{-- <div class="lookback">
          <div class="container">
             <p>Về hội đồng quản trị</p>
             <ul>
                <li class="item-1">
                   <span class="icon"></span>
                   <p><strong>Năm 2008</strong>Chính thức thành lập công ty</p>
                </li>
                <li class="item-2">
                  <span class="icon"></span>
                  <p><strong>Năm 2008</strong>Chính thức thành lập công ty</p>
               </li>
                <li class="item-3">
                   <span class="icon"></span>
                   <p><strong>Năm 2012</strong>Bizweb được trao tặng danh <br class="d-none d-xl-block">hiệu Sao Khuê năm 2012 với <br class="d-none d-xl-block">hơn 2000 khách hàng</p>
                </li>
                <li class="item-4">
                   <span class="icon"></span>
                   <p><strong>Tháng 9/2013</strong>Sapo mở chi nhánh tại <br class="d-none d-xl-block">Hồ Chí Minh</p>
                </li>
                <li class="item-5">
                   <span class="icon"></span>
                   <p><strong>Năm 2013</strong>Bizweb đã ghi danh vào giải <br class="d-none d-xl-block">thưởng Nhân tài đất Việt 2013 với <br class="d-none d-xl-block">hơn 4000 khách hàng và Sao Khuê <br class="d-none d-xl-block">2012 với 2000 khách hàng</p>
                </li>
                <li class="item-6">
                   <span class="icon"></span>
                   <p><strong>Tháng 1/2014</strong>CYBERAGENT VENTURES - quỹ đầu tư thuộc tập đoàn Cyberagent Nhật Bản đầu tư vào Bizweb</p>
                </li>
                <li class="item-7">
                   <span class="icon"></span>
                   <p><strong>Tháng 10/2014</strong>Ra mắt phần mềm quản lý bán hàng thông minh Sapo.vn</p>
                </li>
                <li class="item-8">
                   <span class="icon"></span>
                   <p><strong>Năm 2015</strong>Sapo được trao tặng danh hiệu <br class="d-none d-xl-block">Sao Khuê năm 2015 với hơn 5000 khách hàng</p>
                </li>
                <li class="item-9">
                   <span class="icon"></span>
                   <p><strong>Tháng 4/2018</strong>Bizweb và Sapo chính thức hợp <br class="d-none d-xl-block">nhất trở thành nền tảng quản lý <br class="d-none d-xl-block">và bán hàng đa kênh Sapo với <br class="d-none d-xl-block">hơn 43,000 khách hàng</p>
                </li>
                <li class="item-10">
                   <span class="icon"></span>
                   <p><strong>Tháng 6/2019</strong>Ra mắt phần mềm quản lý nhà <br class="d-none d-xl-block">hàng, cafe Sapo FnB</p>
                </li>
                <li class="item-11">
                   <span class="icon"></span>
                   <p><strong>Tháng 8/2019</strong>Ra mắt giải pháp quản lý bán <br class="d-none d-xl-block">hàng online Sapo GO</p>
                </li>
             </ul>
          </div>
          <div class="block-img d-none d-xl-block">
          <img src="{{url('frontend/images/lookback-1200.jpg')}}" alt="Cùng nhìn lại chặng đường 10 năm phát triển của Sapo" class="fade show">
          </div>
          <div class="block-img d-none d-lg-block d-xl-none">
          <img src="{{url('frontend/images/lookback-960.jpg')}}" alt="Cùng nhìn lại chặng đường 10 năm phát triển của Sapo">
          </div>
          <div class="block-img d-none d-md-block d-lg-none">
          <img src="{{url('frontend/images/lookback-768.png')}}" alt="Cùng nhìn lại chặng đường 10 năm phát triển của Sapo">
          </div>
       </div> --}}
       <div class="reason">
          <div class="container">
             <div class="row item-reason first">
                <div class="col-lg-6 order-lg-2 block-img">
                <img src="{{url('frontend/images/153-1536474_identifying-students-for-summer-intervention-imagen-de-equipos.png')}}" width="100%" alt="Ngay từ khi thành lập, cresa đã xác định cho mình sứ mệnh..." class="fade show">
                </div>
                <div class="col-lg-6 order-lg-1 block-info">
                   <p class="desc">Ngay từ khi thành lập, cresa đã xác định cho mình sứ mệnh...</p>
                   <h3>“Làm cho việc kinh doanh online trở nên dễ dàng hơn"</h3>
                   <p>Chúng tôi giúp các doanh nghiệp, cửa hàng thay đổi cách bán hàng, quản lý hiệu quả hơn bằng cách cung cấp nền tảng công nghệ đột phá, có thể ứng dụng dễ dàng, nhanh chóng với chi phí thấp dựa trên 3 tiêu chí:</p>
                   <ul>
                      <li class="d-flex">
                         <span class="icon-checked"></span>
                         Credence - Tin tưởng, khách hàng chỉ việc tin tưởng Cresa chúng tôi sẽ không để bạn thất vọng. 
                      </li>
                      <li class="d-flex">
                         <span class="icon-checked"></span>
                         Satisfied - Hài lòng, để có được sự hài lòng từ khách hàng thân yêu, chúng tôi đã phải cố gắng và ngày càng cố gắng hơn nữa.
                      </li>
                      <li class="d-flex">
                         <span class="icon-checked"></span>
                         Responsibility - Trách nhiệm, môi sản phẩm khi được đưa đến khách hàng đều phải thông qua quá trình kiểm tra nghiêm ngặt từ các tester Cresa. Và luôn đồng hành cùng khách hàng khi sản phẩm được đưa ra thị trường.
                      </li>
                   </ul>
                </div>
             </div>
             <div class="row item-reason second">
                <div class="col-lg-6 block-img">
                <img src="{{url('frontend/images/business_intelligence_video.png')}}" width="100%" alt="Và Cresa đang từng bước hiện thực hóa mục tiêu đến năm 2020" class="fade show">
                </div>
                <div class="col-lg-6 block-info">
                   <p class="desc">Và Cresa đang từng bước hiện thực hóa mục tiêu đến năm 2023</p>
                   <h3>"5000 khách hàng là con số mà Cresa đặt ra để phấn đấu cho giấc mơ số 1 Việt Nam"</h3>
                   <p>Cresa luôn không ngừng nghiên cứu công nghệ mới để đón đầu xu hướng nhằm mục tiêu năm 2023:</p>
                   <ul>
                      <li class="d-flex">
                         <span class="icon-checked"></span>
                         5000+ khách hàng tin tưởng và sử dụng dịch vụ của Cresa
                      </li>
                      <li class="d-flex">
                         <span class="icon-checked"></span>
                         Cùng với đó, Cresa không ngừng nỗ lực để mang lại một cuộc sống sung túc cho thành viên của mình.
                      </li>
                      <li class="d-flex">
                         <span class="icon-checked"></span>
                         Nhưng trên hết, chúng tôi mong muốn mang lại một lợi nhuận bền vững cho các nhà đầu tư và có đóng góp thiết thực cho xã hội.
                      </li>
                   </ul>
                </div>
             </div>
          </div>
       </div>
       <div class="criteria">
          <div class="container">
             <h2><strong>04</strong> giá trị cốt lõi</h2>
             <p>Cresa luôn hướng đến quyền lợi của khách hàng và nhân viên trong công ty</p>
          </div>
          <div class="criteria-slider">
             <div class="swiper-container swiper-container-fade swiper-container-horizontal">
                <div class="swiper-wrapper" style="transition-duration: 0ms;">
                   <div class="swiper-slide swiper-slide-duplicate swiper-slide-prev" data-swiper-slide-index="5" style="width: 1903px; opacity: 1; transform: translate3d(0px, 0px, 0px); transition-duration: 0ms;">
                      <div class="container">
                         <div class="row">
                            <div class="col-lg-6 col-12 order-lg-2">
                               <div class="block-img">
                                  <img src="https://www.sapo.vn/Themes/Portal/Default/Images/aboutus/criteria-6.png?2" alt="Đổi mới liên tục">
                               </div>
                            </div>
                            <div class="col-lg-6 col-12 order-lg-1">
                               <div class="block-info">
                                  <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide"><i class="fa fa-arrow-right"></i></div>
                                  <div class="swiper-button-prev" tabindex="0" role="button" aria-label="Previous slide"><i class="fa fa-arrow-left"></i></div>
                                  <span class="number">06</span>
                                  <h3>Đổi mới liên tục</h3>
                                  <ul>
                                     <li class="d-flex">
                                        <span class="icon-checked"></span>
                                        Cresa tin tưởng rằng những cải tiến nhỏ và liên tục sẽ tạo ra kết quả vượt trội.
                                     </li>
                                     <li class="d-flex">
                                        <span class="icon-checked"></span>
                                        Sapo quan niệm: Không có thành công nào bền vững, chỉ có đổi mới và sáng tạo mới có thể giúp bạn tiếp tục tiến lên phía trước.
                                     </li>
                                     <li class="d-flex">
                                        <span class="icon-checked"></span>
                                        Người Cresa luôn xác định tinh thần không ngừng học tập, nâng cao tri thức và năng lực bản thân để phát triển.
                                     </li>
                                  </ul>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                   <div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0" style="width: 1903px; opacity: 1; transform: translate3d(-1903px, 0px, 0px); transition-duration: 0ms;">
                      <div class="container">
                         <div class="row">
                            <div class="col-lg-6 col-12 order-lg-2">
                               <div class="block-img">
                               <img src="{{url('frontend/images/khach-hang.png')}}" width="100%" alt="Khách hàng là trước tiên" class="fade show">
                               </div>
                            </div>
                            <div class="col-lg-6 col-12 order-lg-1">
                               <div class="block-info">
                                  <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide"><i class="fa fa-arrow-right"></i></div>
                                  <div class="swiper-button-prev" tabindex="0" role="button" aria-label="Previous slide"><i class="fa fa-arrow-left"></i></div>
                                  <span class="number">01</span>
                                  <h3>Khách hàng là trước tiên</h3>
                                  <ul>
                                     <li class="d-flex">
                                        <span class="icon-checked"></span>
                                        Cresa lấy khách hàng làm trung tâm cho mọi chiến lược.
                                     </li>
                                     <li class="d-flex">
                                        <span class="icon-checked"></span>
                                        Cresa đặt lợi ích và mong muốn của khách hàng lên đầu tiên.
                                     </li>
                                     <li class="d-flex">
                                        <span class="icon-checked"></span>
                                        Người Cresa luôn tự hỏi: Đây đã phải là điều tốt nhất chúng ta có thể làm cho khách hàng chưa.
                                     </li>
                                  </ul>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                   <div class="swiper-slide swiper-slide-next" data-swiper-slide-index="1" style="width: 1903px; opacity: 0; transform: translate3d(-3806px, 0px, 0px); transition-duration: 0ms;">
                      <div class="container">
                         <div class="row">
                            <div class="col-lg-6 col-12 order-lg-2">
                               <div class="block-img">
                               <img src="{{url('frontend/images/chinh-truc.png')}}" width="100%" alt="Hành động chính trực" class="fade show">
                               </div>
                            </div>
                            <div class="col-lg-6 col-12 order-lg-1">
                               <div class="block-info">
                                  <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide"><i class="fa fa-arrow-right"></i></div>
                                  <div class="swiper-button-prev" tabindex="0" role="button" aria-label="Previous slide"><i class="fa fa-arrow-left"></i></div>
                                  <span class="number">02</span>
                                  <h3>Hành động chính trực</h3>
                                  <ul>
                                     <li class="d-flex">
                                        <span class="icon-checked"></span>
                                        Cresa đặt chữ "Tín" lên hàng đầu, coi trọng chữ "Tín" như chính danh dự của mình.
                                     </li>
                                     <li class="d-flex">
                                        <span class="icon-checked"></span>
                                        Người Cresa luôn nỗ lực hết sức để đảm bảo đúng hoặc vượt trên cam kết của mình đối với khách hàng, đối tác và cả nội bộ.
                                     </li>
                                     <li class="d-flex">
                                        <span class="icon-checked"></span>
                                        Người Cresa xem trọng sự trung thực, đề cao đạo đức nghề nghiệp cũng như đạo đức xã hội trong văn hóa kinh doanh của mình.
                                     </li>
                                  </ul>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                   <div class="swiper-slide" data-swiper-slide-index="2" style="width: 1903px; opacity: 0; transform: translate3d(-5709px, 0px, 0px); transition-duration: 0ms;">
                      <div class="container">
                         <div class="row">
                            <div class="col-lg-6 col-12 order-lg-2">
                               <div class="block-img">
                               <img src="{{url('frontend/images/fast-512.png')}}" alt="Tốc độ nhanh" class="fade show">
                               </div>
                            </div>
                            <div class="col-lg-6 col-12 order-lg-1">
                               <div class="block-info">
                                  <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide"><i class="fa fa-arrow-right"></i></div>
                                  <div class="swiper-button-prev" tabindex="0" role="button" aria-label="Previous slide"><i class="fa fa-arrow-left"></i></div>
                                  <span class="number">03</span>
                                  <h3>Tốc độ nhanh</h3>
                                  <ul>
                                     <li class="d-flex">
                                        <span class="icon-checked"></span>
                                        Cresa lấy "Tốc độ" làm tôn chỉ hành động, vì vậy chúng ta thường ra quyết định nhanh, hành động nhanh, thích ứng nhanh với sự thay đổi.
                                     </li>
                                     <li class="d-flex">
                                        <span class="icon-checked"></span>
                                        Cresa coi trọng tốc độ nhưng phải đi cùng với hiệu quả và sự triệt để chứ không phải "Nhanh ẩu đoảng".
                                     </li>
                                     <li class="d-flex">
                                        <span class="icon-checked"></span>
                                        Người Cresa mang khát vọng tiên phong, sẵn sàng bơi ngược chiều, làm khác để tạo ra đột phá.
                                     </li>
                                  </ul>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                   <div class="swiper-slide" data-swiper-slide-index="3" style="width: 1903px; opacity: 0; transform: translate3d(-7612px, 0px, 0px); transition-duration: 0ms;">
                      <div class="container">
                         <div class="row">
                            <div class="col-lg-6 col-12 order-lg-2">
                               <div class="block-img">
                               <img src="{{url('frontend/images/10-2-team-work-thumb.png')}}" width="100%" alt="Tinh thần đồng đội" class="fade show">
                               </div>
                            </div>
                            <div class="col-lg-6 col-12 order-lg-1">
                               <div class="block-info">
                                  <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide"><i class="fa fa-arrow-right"></i></div>
                                  <div class="swiper-button-prev" tabindex="0" role="button" aria-label="Previous slide"><i class="fa fa-arrow-left"></i></div>
                                  <span class="number">04</span>
                                  <h3>Tinh thần đồng đội</h3>
                                  <ul>
                                     <li class="d-flex">
                                        <span class="icon-checked"></span>
                                        Cresa tin tưởng rằng tinh thần đồng đội sẽ giúp những người bình thường làm nên những điều phi thường.
                                     </li>
                                     <li class="d-flex">
                                        <span class="icon-checked"></span>
                                        Thành viên Cresa luôn ghi nhớ và hướng đến mục tiêu chung.
                                     </li>
                                     <li class="d-flex">
                                        <span class="icon-checked"></span>
                                        Người Cresa yêu thương, giúp đỡ, tương trợ lẫn nhau.
                                     </li>
                                  </ul>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                   <div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active" data-swiper-slide-index="0" style="width: 1903px; opacity: 0; transform: translate3d(-13321px, 0px, 0px); transition-duration: 0ms;">
                      <div class="container">
                         <div class="row">
                            <div class="col-lg-6 col-12 order-lg-2">
                               <div class="block-img">
                                  <img src="https://www.sapo.vn/Themes/Portal/Default/Images/aboutus/criteria-1.png" alt="Khách hàng là trước tiên">
                               </div>
                            </div>
                            <div class="col-lg-6 col-12 order-lg-1">
                               <div class="block-info">
                                  <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide"><i class="fa fa-arrow-right"></i></div>
                                  <div class="swiper-button-prev" tabindex="0" role="button" aria-label="Previous slide"><i class="fa fa-arrow-left"></i></div>
                                  <span class="number">01</span>
                                  <h3>Khách hàng là trước tiên</h3>
                                  <ul>
                                     <li class="d-flex">
                                        <span class="icon-checked"></span>
                                        Cresa lấy khách hàng làm trung tâm cho mọi chiến lược.
                                     </li>
                                     <li class="d-flex">
                                        <span class="icon-checked"></span>
                                        Cresa đặt lợi ích và mong muốn của khách hàng lên đầu tiên.
                                     </li>
                                     <li class="d-flex">
                                        <span class="icon-checked"></span>
                                        Người Sapo luôn tự hỏi: Đây đã phải là điều tốt nhất chúng ta có thể làm cho khách hàng chưa.
                                     </li>
                                  </ul>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                </div>
                <div class="container">
                   <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 2"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 3"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 4"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 5"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 6"></span></div>
                </div>
                <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
             </div>
          </div>
       </div>
    </div>
    <script>
       addLoadEvent(function () {
           var mySwiper = new Swiper('.swiper-container', {
               loop: true,
               slidesPerView: 1,
               effect: 'fade',
               pagination: {
                   el: '.swiper-pagination',
                   clickable: true,
               },
               navigation: {
                   nextEl: '.swiper-button-next',
                   prevEl: '.swiper-button-prev',
               },
           });
       });
    </script>
 </div>
@endsection