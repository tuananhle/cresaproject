@extends('layouts.master')
@section('title') Ribeto | Chăm sóc sức khỏe cộng đồng @endsection
@section('description') {{$setting->fax}} @endsection 
@section('url') {{ $setting->webname }} @endsection
@section('image') {{ url('/').$setting->logo }} @endsection
@section('css')
    <link href="{{url('frontend/asset/css/refer-website.min.css')}}" rel="preload" as="style" type="text/css" />
    <link href="{{url('frontend/asset/css/refer-website.min.css')}}" rel="stylesheet" />
@endsection
@section('js')
@endsection
@section('content')
<div id="wrapper" class="clearfix">
    <div class="refer-website">
       <div class="banner text-center">
          <div class="container">
             <h1>Một số website bán hàng tiêu biểu</h1>
             <p>Hàng trăm khách hàng đã sử dụng Cresa Web hiệu quả</p>
          </div>
       </div>
       <div class="skins text-center">
          <div class="container">
             <div class="row">
                <div class="col-md-6 col-lg-4">
                   <div class="website-item">
                      <div class="infomation">
                         <div class="thumbnail">
                            <a href="https://bicicosmetics.vn/" target="_blank" rel="nofollow" title="Sao Thái Dương">
                            <img src="{{url('frontend/images/bici.png')}}" alt="Sao Thái Dương" class="item-img fade show">
                            </a>
                         </div>
                         <div class="hidden-info">
                            <div class="description">
                                Bicicosmetics là doanh nghiệp bán hàng và phân phối mỹ phẩm lớn ở Việt Nam
                            </div>
                            <div class="view-zoom">
                               <a href="https://bicicosmetics.vn/" class="zoom" target="_blank" rel="nofollow">
                               <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="https://bicicosmetics.vn/" class="fade show">
                               </a>
                            </div>
                            <div class="view-detail">
                               <a href="https://bicicosmetics.vn/" target="_blank" rel="nofollow" class="btn-registration">
                               Xem chi tiết
                               </a>
                            </div>
                         </div>
                      </div>
                      <div class="website-name">
                         <a href="https://bicicosmetics.vn/" target="_blank" rel="nofollow" title="Sao Thái Dương">
                            Mỹ phẩm Bicicosmetics
                         </a>
                      </div>
                   </div>
                </div>
                <div class="col-md-6 col-lg-4">
                   <div class="website-item">
                      <div class="infomation">
                         <div class="thumbnail">
                            <a href="https://www.shopnhatchaly.com/" target="_blank" rel="nofollow" title="Dược phẩm Chaly">
                            <img src="{{url('frontend/images/duocpham.png')}}" alt="Dược phẩm Chaly" class="item-img fade show">
                            </a>
                         </div>
                         <div class="hidden-info">
                            <div class="description">
                                Shop Nhật Chaly tự tin mang tới cho khách hàng những sản phẩm Nhật nội địa chuẩn về nguồn gốc xuất xứ, đảm bảo về chất lượng và cung cách phục vụ tận tình, tâm huyết nhất
                            </div>
                            <div class="view-zoom">
                               <a href="https://www.shopnhatchaly.com/" class="zoom" target="_blank" rel="nofollow">
                               <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="https://www.shopnhatchaly.com/">
                               </a>
                            </div>
                            <div class="view-detail">
                               <a href="https://www.shopnhatchaly.com/" target="_blank" rel="nofollow" class="btn-registration">
                               Xem chi tiết
                               </a>
                            </div>
                         </div>
                      </div>
                      <div class="website-name">
                         <a href="https://www.shopnhatchaly.com/" target="_blank" rel="nofollow" title="Dược phẩm Chaly">
                            Dược phẩm Chaly
                         </a>
                      </div>
                   </div>
                </div>
                <div class="col-md-6 col-lg-4">
                   <div class="website-item">
                      <div class="infomation">
                         <div class="thumbnail">
                            <a href="https://ivymoda.com/" target="_blank" rel="nofollow" title="Shop thời trang Ivymoda">
                            <img src="{{url('frontend/images/ivymoda.png')}}" alt="Shop thời trang Ivymoda" class="item-img fade show">
                            </a>
                         </div>
                         <div class="hidden-info">
                            <div class="description">
                               Shop thời trang Ivymoda 
                            </div>
                            <div class="view-zoom">
                               <a href="https://ivymoda.com/" class="zoom" target="_blank" rel="nofollow">
                               <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="https://ivymoda.com/">
                               </a>
                            </div>
                            <div class="view-detail">
                               <a href="https://ivymoda.com/" target="_blank" rel="nofollow" class="btn-registration">
                               Xem chi tiết
                               </a>
                            </div>
                         </div>
                      </div>
                      <div class="website-name">
                         <a href="https://ivymoda.com/" target="_blank" rel="nofollow" title="Shop thời trang Ivymoda">
                         Shop thời trang Ivymoda
                         </a>
                      </div>
                   </div>
                </div>
                <div class="col-md-6 col-lg-4">
                   <div class="website-item">
                      <div class="infomation">
                         <div class="thumbnail">
                            <a href="http://alphabooks.vn/" target="_blank" rel="nofollow" title="Alpha Books">
                            <img src="https://www.sapo.vn/Upload/ReferWebsites/2019/11/4d105e00f293ed7e00de0e9ac6dfcb4e.png" alt="Alpha Books" class="item-img">
                            </a>
                         </div>
                         <div class="hidden-info">
                            <div class="description">
                               Công ty cổ phần Sách Alpha (gọi tắt là Alpha Books) được thành lập với niềm tin: Tri thức là Sức mạnh. Thông qua việc giới thiệu các tác phẩm có giá trị của thế giới, Alpha Books mong muốn trở thành nhịp cầu nối nguồn tri thức nhân loại với dân tộc Việt Nam.
                            </div>
                            <div class="view-zoom">
                               <a href="http://alphabooks.vn/" class="zoom" target="_blank" rel="nofollow">
                               <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="http://alphabooks.vn/">
                               </a>
                            </div>
                            <div class="view-detail">
                               <a href="http://alphabooks.vn/" target="_blank" rel="nofollow" class="btn-registration">
                               Xem chi tiết
                               </a>
                            </div>
                         </div>
                      </div>
                      <div class="website-name">
                         <a href="http://alphabooks.vn/" target="_blank" rel="nofollow" title="Alpha Books">
                         Alpha Books
                         </a>
                      </div>
                   </div>
                </div>
                <div class="col-md-6 col-lg-4">
                   <div class="website-item">
                      <div class="infomation">
                         <div class="thumbnail">
                            <a href="https://ngoinhasonha.com/" target="_blank" rel="nofollow" title="Sơn Hà">
                            <img src="https://www.sapo.vn/Upload/ReferWebsites/2019/11/33ddd98323d693ad27cf3b84f03a52ee.png" alt="Sơn Hà" class="item-img">
                            </a>
                         </div>
                         <div class="hidden-info">
                            <div class="description">
                               Trải qua 18 năm đổi mới và phát triển, là một tập đoàn có thương hiệu mạnh trên thị trường Việt Nam và quốc tế, tập đoàn Sơn Hà đang khẳng định vị thế là nhà sản xuất các sản phẩm Inox dân dụng và công nghiệp hàng đầu châu Á.
                            </div>
                            <div class="view-zoom">
                               <a href="https://ngoinhasonha.com/" class="zoom" target="_blank" rel="nofollow">
                               <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="https://ngoinhasonha.com/">
                               </a>
                            </div>
                            <div class="view-detail">
                               <a href="https://ngoinhasonha.com/" target="_blank" rel="nofollow" class="btn-registration">
                               Xem chi tiết
                               </a>
                            </div>
                         </div>
                      </div>
                      <div class="website-name">
                         <a href="https://ngoinhasonha.com/" target="_blank" rel="nofollow" title="Sơn Hà">
                         Sơn Hà
                         </a>
                      </div>
                   </div>
                </div>
                <div class="col-md-6 col-lg-4">
                   <div class="website-item">
                      <div class="infomation">
                         <div class="thumbnail">
                            <a href="https://vpphonghaonline.com.vn/" target="_blank" rel="nofollow" title="Hồng Hà">
                            <img src="https://www.sapo.vn/Upload/ReferWebsites/2019/11/f16f9dd976c631f869e258515f3be524.png" alt="Hồng Hà" class="item-img">
                            </a>
                         </div>
                         <div class="hidden-info">
                            <div class="description">
                               Văn phòng phẩm Hồng Hà là trang thương mại điện tử chuyên giới thiệu, cung cấp các đồ dùng học sinh, sách truyện, quà tặng, đồ lưu niệm chất lượng và uy tín nhất. Được rất nhiều thế hệ học sinh yêu thích, tin tưởng và lựa chọn
                            </div>
                            <div class="view-zoom">
                               <a href="https://vpphonghaonline.com.vn/" class="zoom" target="_blank" rel="nofollow">
                               <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="https://vpphonghaonline.com.vn/">
                               </a>
                            </div>
                            <div class="view-detail">
                               <a href="https://vpphonghaonline.com.vn/" target="_blank" rel="nofollow" class="btn-registration">
                               Xem chi tiết
                               </a>
                            </div>
                         </div>
                      </div>
                      <div class="website-name">
                         <a href="https://vpphonghaonline.com.vn/" target="_blank" rel="nofollow" title="Hồng Hà">
                         Hồng Hà
                         </a>
                      </div>
                   </div>
                </div>
                <div class="col-md-6 col-lg-4">
                   <div class="website-item">
                      <div class="infomation">
                         <div class="thumbnail">
                            <a href="https://www.ducvietfoods.vn/" target="_blank" rel="nofollow" title="Đức Việt foods">
                            <img src="https://www.sapo.vn/Upload/ReferWebsites/2019/7/a8d8e55b9fda3bab93bfc78209447a80.jpg" alt="Đức Việt foods" class="item-img">
                            </a>
                         </div>
                         <div class="hidden-info">
                            <div class="description">
                               Đức Việt... là thương hiệu tiên phong đưa sản phẩm xúc xích tươi đến với người tiêu dùng và ghi dấu ấn chất lượng trong suốt 20 năm. Qua năm tháng, các sản phẩm của Đức Việt đã trở thành món ăn quen thuộc được lựa chọn trong căn bếp Việt.
                            </div>
                            <div class="view-zoom">
                               <a href="https://www.ducvietfoods.vn/" class="zoom" target="_blank" rel="nofollow">
                               <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="https://www.ducvietfoods.vn/">
                               </a>
                            </div>
                            <div class="view-detail">
                               <a href="https://www.ducvietfoods.vn/" target="_blank" rel="nofollow" class="btn-registration">
                               Xem chi tiết
                               </a>
                            </div>
                         </div>
                      </div>
                      <div class="website-name">
                         <a href="https://www.ducvietfoods.vn/" target="_blank" rel="nofollow" title="Đức Việt foods">
                         Đức Việt foods
                         </a>
                      </div>
                   </div>
                </div>
                <div class="col-md-6 col-lg-4">
                   <div class="website-item">
                      <div class="infomation">
                         <div class="thumbnail">
                            <a href="https://lug.vn/" target="_blank" rel="nofollow" title="Lug.vn">
                            <img src="https://www.sapo.vn/Upload/ReferWebsites/2019/11/59a21e01305232f11d83e9af1c52341a.jpg" alt="Lug.vn" class="item-img">
                            </a>
                         </div>
                         <div class="hidden-info">
                            <div class="description">
                               Lug.vn chuyên cung cấp các sản phẩm Vali - Balo/túi xách - Phụ kiện phù hợp,... Thương hiệu luôn mong muốn là người bạn đồng hành giúp người Việt gói trọn mọi hành lý cả đi lẫn về, mang đến cho mọi người cảm giác tuyệt vời nhất mỗi lần đi đâu đó xa nhà
                            </div>
                            <div class="view-zoom">
                               <a href="https://lug.vn/" class="zoom" target="_blank" rel="nofollow">
                               <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="https://lug.vn/">
                               </a>
                            </div>
                            <div class="view-detail">
                               <a href="https://lug.vn/" target="_blank" rel="nofollow" class="btn-registration">
                               Xem chi tiết
                               </a>
                            </div>
                         </div>
                      </div>
                      <div class="website-name">
                         <a href="https://lug.vn/" target="_blank" rel="nofollow" title="Lug.vn">
                         Lug.vn
                         </a>
                      </div>
                   </div>
                </div>
                <div class="col-md-6 col-lg-4">
                   <div class="website-item">
                      <div class="infomation">
                         <div class="thumbnail">
                            <a href="https://lichbuudien.vn/" target="_blank" rel="nofollow" title="Bưu điện Việt Nam">
                            <img src="https://www.sapo.vn/Upload/ReferWebsites/2018/3/6d094bd8a04383efcb6ab21c39b165df.jpg" alt="Bưu điện Việt Nam" class="item-img">
                            </a>
                         </div>
                         <div class="hidden-info">
                            <div class="description">
                               Công Phát Hành Báo Chí Việt Nam là doanh nghiệp trực thuộc Bộ Thông tin và Truyền thông, có hơn 70 năm truyền thống với đội ngũ nhân viên hơn 41.000 người, mạng lưới rộng khắp cả nước và hàng chục ngàn điểm giao dịch chuyên cung cấp báo, tạp chí, lịch, sách hàng đầu trong nước.
                            </div>
                            <div class="view-zoom">
                               <a href="https://lichbuudien.vn/" class="zoom" target="_blank" rel="nofollow">
                               <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="https://lichbuudien.vn/">
                               </a>
                            </div>
                            <div class="view-detail">
                               <a href="https://lichbuudien.vn/" target="_blank" rel="nofollow" class="btn-registration">
                               Xem chi tiết
                               </a>
                            </div>
                         </div>
                      </div>
                      <div class="website-name">
                         <a href="https://lichbuudien.vn/" target="_blank" rel="nofollow" title="Bưu điện Việt Nam">
                         Bưu điện Việt Nam
                         </a>
                      </div>
                   </div>
                </div>
                <div class="col-md-6 col-lg-4">
                   <div class="website-item">
                      <div class="infomation">
                         <div class="thumbnail">
                            <a href="http://ohana.vn/" target="_blank" rel="nofollow" title="Trường mầm non QT O'Hana">
                            <img src="https://www.sapo.vn/Upload/ReferWebsites/2019/11/ebc8b61893bf0bc3d96bb2cb28abd397.png" alt="Trường mầm non QT O'Hana" class="item-img">
                            </a>
                         </div>
                         <div class="hidden-info">
                            <div class="description">
                               Được thành lập từ năm 2005, O'Hana tự hào là trường mầm non song ngữ cung cấp dịch vụ chăm sóc và giáo dục chất lượng tiêu chuẩn quốc tế hàng đầu tại Hà Nội với một chế độ chăm sóc kỹ lưỡng cùng giáo trình giảng dạy hoàn hảo.
                            </div>
                            <div class="view-zoom">
                               <a href="http://ohana.vn/" class="zoom" target="_blank" rel="nofollow">
                               <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="http://ohana.vn/">
                               </a>
                            </div>
                            <div class="view-detail">
                               <a href="http://ohana.vn/" target="_blank" rel="nofollow" class="btn-registration">
                               Xem chi tiết
                               </a>
                            </div>
                         </div>
                      </div>
                      <div class="website-name">
                         <a href="http://ohana.vn/" target="_blank" rel="nofollow" title="Trường mầm non QT O'Hana">
                         Trường mầm non QT O'Hana
                         </a>
                      </div>
                   </div>
                </div>
                <div class="col-md-6 col-lg-4">
                   <div class="website-item">
                      <div class="infomation">
                         <div class="thumbnail">
                            <a href="http://thuyhoalua.com/" target="_blank" rel="nofollow" title="Thủy Hoa Lụa">
                            <img src="https://www.sapo.vn/Upload/ReferWebsites/2019/11/3ed15afb675a6aed12822fe6f0ca712b.jpg" alt="Thủy Hoa Lụa" class="item-img">
                            </a>
                         </div>
                         <div class="hidden-info">
                            <div class="description">
                               Khởi nghiệp từ năm 1991, với cửa hàng đầu tiên ở 16 Đồng Xuân, đến nay hệ thống showroom của THUỶ HOA LỤA đã có hơn 20 năm kinh nghiệm trong  lĩnh vực hoa lụa, hoa vải, hoa cao su.
                            </div>
                            <div class="view-zoom">
                               <a href="http://thuyhoalua.com/" class="zoom" target="_blank" rel="nofollow">
                               <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="http://thuyhoalua.com/">
                               </a>
                            </div>
                            <div class="view-detail">
                               <a href="http://thuyhoalua.com/" target="_blank" rel="nofollow" class="btn-registration">
                               Xem chi tiết
                               </a>
                            </div>
                         </div>
                      </div>
                      <div class="website-name">
                         <a href="http://thuyhoalua.com/" target="_blank" rel="nofollow" title="Thủy Hoa Lụa">
                         Thủy Hoa Lụa
                         </a>
                      </div>
                   </div>
                </div>
                <div class="col-md-6 col-lg-4">
                   <div class="website-item">
                      <div class="infomation">
                         <div class="thumbnail">
                            <a href="https://dhkj.vn/" target="_blank" rel="nofollow" title="DHK Jewelry">
                            <img src="https://www.sapo.vn/Upload/ReferWebsites/2019/11/fd5a782901264f10268be4627dfe5f94.png" alt="DHK Jewelry" class="item-img">
                            </a>
                         </div>
                         <div class="hidden-info">
                            <div class="description">
                               DHK Jewelry là thương hiệu chuyên chế tác trang sức hàng đầu Việt Nam. DHK Jewelry mong muốn bất cứ ai cũng có thể sở hữu được ít nhất một món trang sức giá trị với chất lượng hoàn hảo và mức giá hợp lý.
                            </div>
                            <div class="view-zoom">
                               <a href="https://dhkj.vn/" class="zoom" target="_blank" rel="nofollow">
                               <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="https://dhkj.vn/">
                               </a>
                            </div>
                            <div class="view-detail">
                               <a href="https://dhkj.vn/" target="_blank" rel="nofollow" class="btn-registration">
                               Xem chi tiết
                               </a>
                            </div>
                         </div>
                      </div>
                      <div class="website-name">
                         <a href="https://dhkj.vn/" target="_blank" rel="nofollow" title="DHK Jewelry">
                         DHK Jewelry
                         </a>
                      </div>
                   </div>
                </div>
                <div class="col-md-6 col-lg-4">
                   <div class="website-item">
                      <div class="infomation">
                         <div class="thumbnail">
                            <a href="https://bo-le-ro.vn/" target="_blank" rel="nofollow" title="Ẩm thực Bò-Lế-Rồ">
                            <img src="https://www.sapo.vn/Upload/ReferWebsites/2019/11/983d65b4ad33c4cd35650b147166ffc6.jpg" alt="Ẩm thực Bò-Lế-Rồ" class="item-img">
                            </a>
                         </div>
                         <div class="hidden-info">
                            <div class="description">
                               Ẩm thực BÒ - LẾ - RỒ  là chuỗi nhà hàng với những món ăn làm từ thịt bò được biến tấu độc đáo, thu hút các tín đồ ẩm thực ghé đến thưởng thức không chỉ một lần
                            </div>
                            <div class="view-zoom">
                               <a href="https://bo-le-ro.vn/" class="zoom" target="_blank" rel="nofollow">
                               <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="https://bo-le-ro.vn/">
                               </a>
                            </div>
                            <div class="view-detail">
                               <a href="https://bo-le-ro.vn/" target="_blank" rel="nofollow" class="btn-registration">
                               Xem chi tiết
                               </a>
                            </div>
                         </div>
                      </div>
                      <div class="website-name">
                         <a href="https://bo-le-ro.vn/" target="_blank" rel="nofollow" title="Ẩm thực Bò-Lế-Rồ">
                         Ẩm thực Bò-Lế-Rồ
                         </a>
                      </div>
                   </div>
                </div>
                <div class="col-md-6 col-lg-4">
                   <div class="website-item">
                      <div class="infomation">
                         <div class="thumbnail">
                            <a href="http://beemart.vn/" target="_blank" rel="nofollow" title="Beemart - Đồ làm bánh">
                            <img src="https://www.sapo.vn/Upload/ReferWebsites/2019/11/4d12d2f10e3b7099dd5165d2b6a6ab6a.png" alt="Beemart - Đồ làm bánh" class="item-img">
                            </a>
                         </div>
                         <div class="hidden-info">
                            <div class="description">
                               Beemart là một trong những siêu thị cung cấp đầy đủ nguyên liệu, dụng cụ cho người yêu thích làm bánh, là một trong những siêu thị có số lượng mặt hàng phong phú nhất, chất lượng luôn đảm bảo khi cung cấp tới tay khách hàng.
                            </div>
                            <div class="view-zoom">
                               <a href="http://beemart.vn/" class="zoom" target="_blank" rel="nofollow">
                               <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="http://beemart.vn/">
                               </a>
                            </div>
                            <div class="view-detail">
                               <a href="http://beemart.vn/" target="_blank" rel="nofollow" class="btn-registration">
                               Xem chi tiết
                               </a>
                            </div>
                         </div>
                      </div>
                      <div class="website-name">
                         <a href="http://beemart.vn/" target="_blank" rel="nofollow" title="Beemart - Đồ làm bánh">
                         Beemart - Đồ làm bánh
                         </a>
                      </div>
                   </div>
                </div>
                <div class="col-md-6 col-lg-4">
                   <div class="website-item">
                      <div class="infomation">
                         <div class="thumbnail">
                            <a href="https://www.chudauceramic.shop/" target="_blank" rel="nofollow" title="Gốm Chu Đậu">
                            <img src="https://www.sapo.vn/Upload/ReferWebsites/2019/12/91d61b0c2884a5ec034fa06928276cbe.jpg" alt="Gốm Chu Đậu" class="item-img">
                            </a>
                         </div>
                         <div class="hidden-info">
                            <div class="description">
                               Gốm Chu Đậu là gốm sứ cổ truyền Việt Nam đã được sản xuất tại vùng mà nay thuộc làng Chu Đậu - Mỹ Xá - Minh Tân - Hải Dương. Chuyên cung cấp các sản phẩm gốm sứ Chu Đậu chính hãng với giá tốt cùng trải nghiệm mua sắm trực tuyến tin cậy và tuyệt vời.
                            </div>
                            <div class="view-zoom">
                               <a href="https://www.chudauceramic.shop/" class="zoom" target="_blank" rel="nofollow">
                               <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="https://www.chudauceramic.shop/">
                               </a>
                            </div>
                            <div class="view-detail">
                               <a href="https://www.chudauceramic.shop/" target="_blank" rel="nofollow" class="btn-registration">
                               Xem chi tiết
                               </a>
                            </div>
                         </div>
                      </div>
                      <div class="website-name">
                         <a href="https://www.chudauceramic.shop/" target="_blank" rel="nofollow" title="Gốm Chu Đậu">
                         Gốm Chu Đậu
                         </a>
                      </div>
                   </div>
                </div>
                <div class="col-md-6 col-lg-4">
                   <div class="website-item">
                      <div class="infomation">
                         <div class="thumbnail">
                            <a href="https://battranggom.vn/" target="_blank" rel="nofollow" title="Bát Tràng Gốm">
                            <img src="https://www.sapo.vn/Upload/ReferWebsites/2017/9/a346be45176a711e1e62a9b577bb61a6.jpg" alt="Bát Tràng Gốm" class="item-img">
                            </a>
                         </div>
                         <div class="hidden-info">
                            <div class="description">
                               Battranggom.vn là thương hiệu uy tín, chất lượng và chuyên nghiệp hàng đầu trong lĩnh vực kinh doanh sản phẩm gốm sứ Bát Tràng. Chất lượng dịch vụ, giá cả và chất lượng sản phẩm là tiêu chí được chú trọng hàng đầu mang đến cho khách hàng sự hài lòng.
                            </div>
                            <div class="view-zoom">
                               <a href="https://battranggom.vn/" class="zoom" target="_blank" rel="nofollow">
                               <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="https://battranggom.vn/">
                               </a>
                            </div>
                            <div class="view-detail">
                               <a href="https://battranggom.vn/" target="_blank" rel="nofollow" class="btn-registration">
                               Xem chi tiết
                               </a>
                            </div>
                         </div>
                      </div>
                      <div class="website-name">
                         <a href="https://battranggom.vn/" target="_blank" rel="nofollow" title="Bát Tràng Gốm">
                         Bát Tràng Gốm
                         </a>
                      </div>
                   </div>
                </div>
                <div class="col-md-6 col-lg-4">
                   <div class="website-item">
                      <div class="infomation">
                         <div class="thumbnail">
                            <a href="http://netsaigon.com.vn/" target="_blank" rel="nofollow" title="Nét Sài Gòn">
                            <img src="https://www.sapo.vn/Upload/ReferWebsites/2017/1/0b8ff97d8c9309a2b07bb674984cc9a2.png" alt="Nét Sài Gòn" class="item-img">
                            </a>
                         </div>
                         <div class="hidden-info">
                            <div class="description">
                               Đến với Nét Sài Gòn, thực khách không chỉ hài lòng khi được thưởng thức những món ăn ngon miệng, mà như sống lại trong không gian Sài Gòn diễm lệ ngày xưa và cảm nhận được trọn vẹn hương vị của thời gian, của những đam mệ được giữ gìn qua nhiều thế hệ.
                            </div>
                            <div class="view-zoom">
                               <a href="http://netsaigon.com.vn/" class="zoom" target="_blank" rel="nofollow">
                               <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="http://netsaigon.com.vn/">
                               </a>
                            </div>
                            <div class="view-detail">
                               <a href="http://netsaigon.com.vn/" target="_blank" rel="nofollow" class="btn-registration">
                               Xem chi tiết
                               </a>
                            </div>
                         </div>
                      </div>
                      <div class="website-name">
                         <a href="http://netsaigon.com.vn/" target="_blank" rel="nofollow" title="Nét Sài Gòn">
                         Nét Sài Gòn
                         </a>
                      </div>
                   </div>
                </div>
                <div class="col-md-6 col-lg-4">
                   <div class="website-item">
                      <div class="infomation">
                         <div class="thumbnail">
                            <a href="https://www.thegioituixach.vn/" target="_blank" rel="nofollow" title="Thế giới túi xách">
                            <img src="https://www.sapo.vn/Upload/ReferWebsites/2019/11/8dea4de009072d9205d888b1c34e77e8.jpg" alt="Thế giới túi xách" class="item-img">
                            </a>
                         </div>
                         <div class="hidden-info">
                            <div class="description">
                               Thế giới túi xách cũng là nhà cung cấp Vali – hành lý du lịch, xuất khẩu chủ lực sang Nhật theo dạng ODM &amp; OEM. Thegioituixach là thương hiệu Việt dẫn đầu phát triển hệ thống phân phối trong các trung tâm thương mại cao cấp với các nhãn MACAT và TOMI.
                            </div>
                            <div class="view-zoom">
                               <a href="https://www.thegioituixach.vn/" class="zoom" target="_blank" rel="nofollow">
                               <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="https://www.thegioituixach.vn/">
                               </a>
                            </div>
                            <div class="view-detail">
                               <a href="https://www.thegioituixach.vn/" target="_blank" rel="nofollow" class="btn-registration">
                               Xem chi tiết
                               </a>
                            </div>
                         </div>
                      </div>
                      <div class="website-name">
                         <a href="https://www.thegioituixach.vn/" target="_blank" rel="nofollow" title="Thế giới túi xách">
                         Thế giới túi xách
                         </a>
                      </div>
                   </div>
                </div>
             </div>
             <p>
             Bạn muốn sở hữu một website được thiết kế chuyên nghiệp, đẹp mặt như khách hàng Cresa Web? <br class="visible-lg">Cùng khám phá những mẫu <a href="{{url('kho-giao-dien/'.'all'.'.html')}}" target="_blank">website</a> đẹp của chúng tôi!
             </p>
          </div>
       </div>
    </div>
    <script>
       addLoadEvent(function () {
           var m = window.location.href;
           if(m.indexOf('website-tham-khao.html') > 0){
               $('.icon-featured').addClass('actived');
           }
           function onResizeWindow() {
               if(18>=3){
                   if ($(window).width() > 991) {
                       if(18%3 != 0){
                           var m = (18%3) * -1;
                           $('.skins .item-web').slice(m).hide();
                       }
                   }
                   else if ($(window).width() > 767) {
                       if(18%2!=0){
                           $('.skins .item-web').slice(-1).hide();
                       }
                   }
               }
       
           }
       });
    </script>
    <script>
       addLoadEvent(function () {
           $("#header .btn-registration").attr("onclick", "showModalTrial(this, 2, false);");
           $(".menu-mobile .btn-registration").attr("onclick", "showModalTrial(this, 2, false);");
           $(".register-bottom .btn-registration").attr("onclick", "showModalTrial(this, 2, false);");
       });
    </script>
 </div>
@endsection