@extends('layouts.master')
@section('title') Liên hệ với Cresa @endsection
@section('description') Cresa cung cấp dịch vụ thiết kế website chuyện nghiệp, website giá rẻ @endsection 
@section('url') {{route('lienHe')}} @endsection 
@section('keyword') Giao diện website, giao dien , website, giao dien websie chuan seo, giao dien web dep @endsection
@section('image') {{ url('/').$setting->logo }} @endsection
@section('css')
    {{-- <link href="{{url('frontend/asset/css/index.min.css')}}" rel="preload" as="style" type="text/css" />
    <link href="{{url('frontend/asset/css/popup-register-default.min.css')}}" rel="stylesheet" /> --}}
@endsection
@section('js')
    <link href="{{url('frontend/asset/css/contact.min.css')}}" rel="preload" as="style" type="text/css" />
    <link href="{{url('frontend/asset/css/contact.min.css')}}" rel="stylesheet" />
@endsection
@section('content')
<div id="wrapper" class="clearfix">
    <div class="contact-us">
       <div class="company-info text-center">
          <div class="container">
          <h1>{{$setting->company}}</h1>
             <h4>Trụ sở:</h4>
          <p><i class="fas fa-map-marker"></i>{{$setting->address1}}</p>
            <p><b>Email : </b> <a href="mailto:{{$setting->email}}">{{$setting->email}}</a></p>
            <p><b>Tổng đài tư vấn và hỗ trợ khách hàng </b>: {{$setting->phone1}}</p>
             <p>Từ 8h00 – 22h00 các ngày từ thứ 2 đến Chủ nhật</p>
             <div class="contact-map">
                {!!$setting->iframe_map!!}
             </div>
          </div>
       </div>
    </div>
 </div>
@endsection