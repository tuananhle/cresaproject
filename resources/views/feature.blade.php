@extends('layouts.master')
@section('title') Ribeto | Chăm sóc sức khỏe cộng đồng @endsection
@section('description') {{$setting->fax}} @endsection 
@section('url') {{ $setting->webname }} @endsection
@section('image') {{ $setting->logo }} @endsection
@section('css')
    <link href="{{url('frontend/asset/css/function.min.css')}}" rel="preload" as="style" type="text/css" />
    <link href="{{url('frontend/asset/css/function.min.css')}}" rel="stylesheet" />
@endsection
@section('content')
<div id="wrapper" class="clearfix">
    <div class="function-page featured">
       <div class="function-menu">
          <div class="container">
             <h1>Những tính năng nổi bật mà website <br class="d-none d-md-block">bán hàng mang đến cho bạn</h1>
             <p>Chúng tôi giúp cho việc bán hàng của bạn trở nên dễ dàng hơn với một website đầy đủ các tính năng vượt trội.</p>
             <div class="function-menu-slide">
                <div class="swiper-container swiper-container-horizontal">
                   <div class="swiper-wrapper">
                      <div class="item swiper-slide current swiper-slide-active" style="width: 162.857px;"><a href="/tinh-nang-noi-bat-website.html">Tính năng<br>nổi bật</a></div>
                      <div class="item swiper-slide swiper-slide-next" style="width: 162.857px;"><a href="/website-ban-hang-uu-viet.html">Web bán hàng<br>ưu việt</a></div>
                      <div class="item swiper-slide" style="width: 162.857px;"><a href="/quan-ly-thong-minh.html">Quản lý cửa<br> hàng online</a></div>
                      <div class="item swiper-slide" style="width: 162.857px;"><a href="/tu-chinh-sua-giao-dien.html">Tùy chỉnh<br>giao diện</a></div>
                      <div class="item swiper-slide" style="width: 162.857px;"><a href="/marketing-hieu-qua.html">Marketing<br>&amp; SEO</a></div>
                      <div class="item swiper-slide" style="width: 162.857px;"><a href="/quan-ly-san-pham.html">Quản lý<br>sản phẩm</a></div>
                      <div class="item swiper-slide" style="width: 162.857px;"><a href="/nen-tang-manh-me.html">Mạnh mẽ<br>&amp; Bảo mật</a></div>
                   </div>
                   <div class="swiper-pagination d-lg-none swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span></div>
                   <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                </div>
                <div class="swiper-button-prev d-lg-none swiper-button-disabled" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="true"><i class="fa fa-angle-left"></i></div>
                <div class="swiper-button-next d-lg-none swiper-button-disabled" tabindex="0" role="button" aria-label="Next slide" aria-disabled="true"><i class="fa fa-angle-right"></i></div>
             </div>
          </div>
       </div>
       <script type="text/javascript">
          addLoadEvent(function () {
              var blockSwipermain = new Swiper('.function-menu .swiper-container', {
                  slidesPerView: 7,
                  loop: false,
                  preventClicks: false,
                  preventClicksPropagation: false,
                  simulateTouch: false,
                  pagination: {
                      el: '.function-menu .swiper-pagination',
                      clickable: true,
                  },
                  navigation: {
                      nextEl: '.function-menu .swiper-button-next',
                      prevEl: '.function-menu .swiper-button-prev',
                  },
                  breakpoints: {
                      400: {
                          slidesPerView: 2,
                          simulateTouch: true,
                          slidesPerGroup: 2
                      },
                      767: {
                          slidesPerView: 3,
                          simulateTouch: true,
                          slidesPerGroup: 3
                      },
                      991: {
                          slidesPerView: 5,
                          simulateTouch: true,
                          slidesPerGroup: 5
                      }
                  }
              });
              blockSwipermain.slideTo(1, 300, false);
          });
       </script>
       <div class="function-content">
          <div class="container">
             <div class="row item align-items-center">
                <div class="col-lg-6 order-lg-2 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tinh-nang-noi-bat-sapo.jpg" alt="tính năng nổi bật Sapo Web" class="fade show">
                </div>
                <div class="col-lg-6 order-lg-1 block-content">
                   <h2>Bán hàng ngay chỉ sau 30s</h2>
                   <p>Thời gian và tiền là hai yếu tố mà ai cũng muốn đầu tư một cách tối ưu. Sapo Web giúp bạn thực hiện cả 2 việc này: <a href="/thiet-ke-website-ban-hang.html#công_nghệ_tính_năng">Thiết kế web bán hàng</a> trong 30 giây, cập nhật các thông tin  về sản phẩm, giới thiệu, liên hệ và bắt đầu bán hàng. Sử dụng miễn phí toàn bộ tính năng trong 15 ngày. Khi quyết định sử dụng, chắc chắn đầu tư của bạn đã hợp lý</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tinh-nang-Sapo-noi-bat.jpg" alt="Tính năng Sapo Web nổi bật">
                </div>
                <div class="col-lg-6 block-content">
                   <h2>Kho giao diện đa dạng, tùy chỉnh dễ dàng</h2>
                   <p>Kho giao diện của Sapo Web không chỉ đa dạng lĩnh vực mà còn phong phú về số lượng. Tất cả các giao diện đều được thiết kế bởi các chuyên gia sáng tạo hàng đầu. Giúp tăng tỉ lên chuyển đổi từ traffic thành đơn hàng, thúc đẩy tăng doanh số nhanh chóng</p>
                   <a href="//themes.sapo.vn">Xem thêm kho giao diện  <i class="fa fa-angle-double-right"></i></a>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 order-lg-2 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/sapo-tinh-nang-noi-bat.jpg" alt="Sapo Web tính năng nổi bật">
                </div>
                <div class="col-lg-6 order-lg-1 block-content">
                   <h2>Kho ứng dụng phong phú</h2>
                   <p>Sapo Web cung cấp kho ứng dụng có thể đáp ứng mọi yêu cầu của một website bán hàng chuyên nghiệp. Sapo Web sẽ đem đến cho website của bạn các ứng dụng hữu ích từ tích hợp vận chuyển, thanh toán online, sms marketing, email marketing đến tạo mẫu quảng cáo và hàng trăm tiện ích khác</p>
                   <a href="//apps.sapo.vn">Xem thêm kho ứng dụng <i class="fa fa-angle-double-right"></i></a>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tinh-nang-sapo-noi-bat1.jpg" alt="Tính năng Sapo Web nổi bật">
                </div>
                <div class="col-lg-6 block-content">
                   <h2>Tối ưu hóa bộ máy tìm kiếm - SEO</h2>
                   <p>Sapo Web được thiết kế tối ưu về kiến trúc và liên kết cho các máy tìm kiếm ngay từ khi phát triển. Các liên kết trên Sapo Web rất thân thiện với các bộ máy tìm kiếm (nhất là Google) Ngoài ra tính năng tối ưu nội dung của Sapo Web giúp bạn tối ưu dữ liệu một cách dễ dàng và chủ động</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 order-lg-2 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/tinh-nang-sapo.jpg" alt="Tính năng Sapo Web ">
                </div>
                <div class="col-lg-6 order-lg-1 block-content">
                   <h2>Tùy biến linh hoạt, chỉnh sửa dễ dàng</h2>
                   <p>Không cần là một người am hiểu về công nghệ thông tin bạn vẫn có thể bạn có thể tự mình up sản phẩm, tối ưu SEO và chỉnh mọi thứ trên website theo ý mình. Ngoài ra, Sapo Web còn có khả năng tùy biến linh hoạt để phù hợp với tất cả các ngành nghề. Bạn có thể tùy biến sản phẩm đa dạng với các công cụ được tích hợp trên hệ thống</p>
                </div>
             </div>
             <div class="row item align-items-center">
                <div class="col-lg-6 block-img">
                   <img src="https://www.sapo.vn/Themes/Portal/Default/Images/func/sapo-tinh-nang.jpg" alt="Sapo Web tính năng">
                </div>
                <div class="col-lg-6 block-content">
                   <h2>Đặt hàng thanh toán an toàn và nhanh chóng</h2>
                   <p>Với Sapo Web, khách hàng có thể thực hiện đặt hàng và thanh toán một cách đơn giản và an toàn. Hệ thống tích hợp các cổng thanh toán điện tử giúp việc giao dịch trở nên dễ dàng hơn bao giờ hết.</p>
                </div>
             </div>
          </div>
       </div>
    </div>
    @include('layouts.form_footer')
    <script>
    </script>
 </div>
@endsection