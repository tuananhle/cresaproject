@extends('layouts.master')
@php
   $images = json_decode($interface->images);
@endphp
@section('title') Template, Giao diện mẫu {{$interface->name}} @endsection
@section('description') {{$interface->description}} @endsection 
@section('url') {{url('chi-tiet/'.$interface->id)}} @endsection
@section('keyword') Giao diện website, giao dien , website, giao dien websie chuan seo, giao dien web dep @endsection
@section('image') {{url('/').$images[0]}} @endsection
@section('css')
    {{-- <link href="{{url('frontend/asset/css/index.min.css')}}" rel="preload" as="style" type="text/css" />
    <link href="{{url('frontend/asset/css/popup-register-default.min.css')}}" rel="stylesheet" /> --}}
@endsection
@section('js')
    <link href="{{url('frontend/asset/css/popup-register-default.min.css')}}" rel="stylesheet">
    <script src="{{url('frontend/asset/js/script.js')}}"></script>
    <script src="{{url('frontend/asset/js/popup-trial.min.js')}}"></script>
@endsection
@section('content')
<div id="wrapper" class="default-wrapper">
    <div class="detail-container">
       <div class="theme-intro responsive">
          <div class="container">
             <div class="row">
                <div class="col-lg-4">
                <h1>Template, Giao diện mẫu <span>{{$interface->name}}</span></h1>
                   <div class="desc">
                      {!!$interface->description!!}
                   </div>
                   <div class="other-info clearfix">
                      <span class="price float-left ">
                      <b>1,500,000 VNĐ</b>
                      </span>
                      <span class="rating float-right">
                      <img src="{{url('frontend/5-star.png')}}">
                      </span>
                   </div>
                   <div class="theme-action">
                      <a href="javascript:void(0)" data-toggle="modal" data-target="#ModalSetup" class="btn-registration install-theme">Chọn giao diện</a>
                   <a href="{{$interface->link_demo}}" class="view-demo action-preview-theme" target="_blank" data-url="{{$interface->link_demo}}">Xem trước giao diện</a>
                   </div>
                </div>
                <div class="col-lg-8 theme-image">
                   <div class="image-desktop d-none d-md-block">
                      <a href="{{$interface->link_demo}}" target="_blank" class="action-preview-theme" data-url="{{$interface->link_demo}}">
                      <img src="{{url('/').$images[0]}}" alt="{{$interface->name}}">
                      </a>
                   </div>
                   <div class="image-mobile">
                      
                   </div>
                </div>
             </div>
          </div>
       </div>
       <div class="theme-detail">
          <div class="container">
             <div class="row">
                <div class="col-lg-8">
                   <div class="detail-feature">
                      {!!$interface->content!!}
                   </div>
                </div>
                <div class="col-lg-4">
                   <div class="developer-info">
                      <h4>Nhà phát triển giao diện <span>{{$interface->name}}</span></h4>
                      <hr>
                   </div>
                </div>
             </div>
          </div>
       </div>
       <div class="themes-related">
          <div class="container">
             <h3>Các giao diện mới nhất</h3>
             <div class="row">
                @foreach($newInterface as $item)
                <div class="col-lg-4 col-xl-3 col-md-6 col-sm-6 ">
                   <div class="theme-item responsive">
                     @php
                        $image = json_decode($item->images);
                     @endphp
                      <div class="theme-image">
                      <img src="{{$image[0]}}" alt="{{ $item->name}}">
                         <div class="theme-action">
                            <div class="button">
                               <a href="{{$item->link_demo}}" class="view-demo action-preview-theme" data-url="{{$item->link_demo}}" target="_blank">Xem thử</a>
                               <a href="{{url('chi-tiet/'.$item->id)}}" class="view-detail">Chi tiết</a>
                            </div>
                         </div>
                      </div>
                      <div class="theme-info">
                         <h3><a href="{{url('chi-tiet/'.$item->id)}}" class="title">{{ $item->name}}</a></h3>
                         <span class="price ">
                         <b>1,500,000 VNĐ</b>
                         </span>
                      </div>
                   </div>
                </div>
                @endforeach
             </div>
          </div>
       </div>
    </div>
    <div class="modal modal-setup fade" id="ModalSetup" tabindex="-1" role="dialog" aria-labelledby="ModalSetupLabel" aria-hidden="true">
       <div class="modal-dialog">
          <div class="modal-content modal-themes">
             <div class="modal-header" style="padding-right:0;">
                <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
             </div>
             <div class="modal-body text-center">
                <div class="form-horizontal">
                   <div class="form-group">
                      <h3>Bạn muốn sử dụng giao diện này?</h3>
                      <p style="font-size: 15px; color: #b332d3cf;">Điền đầy đủ thông tin để được chúng tôi hỗ trợ bạn</p>
                   </div>
                <form method="post" action="{{route('changeInterface')}}" id="formLoginRedirect" novalidate>
                      @csrf
                      <input name="product_id" type="hidden" value="{{$interface->id}}" class="input-site-name">
                      <div class="form-group row quick-registration">
                         <div class="col-md-12">
                            <input name="name" type="text" placeholder="Họ và tên" class="input-site-name">
                         </div>
                      </div>
                      <div class="form-group row quick-registration">
                        <div class="col-md-12">
                            <input name="phone" type="text" placeholder="Số điện thoại" required class="input-site-name">
                         </div>
                      </div>
                      <div class="form-group row quick-registration">
                        <div class="col-md-12">
                            <input name="email" type="text" placeholder="Email" required class="input-site-name">
                         </div>
                      </div>
                      <hr style="margin: 20px 0; border-bottom: 1px solid #e0e0e0;">
                   <div class="form-group quick-registration">
                      <button id="btnSearch" type="submit" class="btn-registration event-Sapo-Free-Trial-form-open">
                      Hoàn tất
                      </button>
                   </div>
                   </form>
                   <script>
                       $("#btnSearch").click(function(event) {

                        // Fetch form to apply custom Bootstrap validation
                        var form = $("#formLoginRedirect")

                        if (form[0].checkValidity() === false) {
                        event.preventDefault()
                        event.stopPropagation()
                        }

                        form.addClass('was-validated');
                        // Perform ajax submit here...

                        });
                   </script>
                </div>
             </div>
          </div>
       </div>
    </div>
    <div class="clear"></div>
<img class="scroll-top" src="{{url('frontend/images/totop.png')}}" style="display:none;cursor :pointer; position : fixed;bottom : 90px;right : 33px;z-index : 99999;">
    <script>
       $(document).ready(function () {
           $(window).scroll(function () {
               if ($(window).scrollTop() > 700) {
                   $('.scroll-top').show();
               }
               else {
                   $('.scroll-top').hide();
               }
           });
           $('.scroll-top').click(function () {
               $("html, body").animate({ scrollTop: 0 }, "slow");
           });
           if ($(window).width() < 768) {
               $('.scroll-top').css({ 'bottom': '90px', 'right': '8px' });
           }
       });
    </script>
    <div class="register-bottom clearfix">
       <div class="container">
          <h4>Bắt đầu dùng thử 15 ngày</h4>
          <p>Để trải nghiệm nền tảng quản lý và bán hàng đa kênh được sử dụng nhiều nhất Việt Nam</p>
          <div class="reg-form">
             <input id="site_name_bottom" class="input-site-name d-none d-md-inline-block" type="text" value="" placeholder="Nhập tên cửa hàng/doanh nghiệp của bạn">
             <a class="btn-registration banner-home-registration event-Sapo-Free-Trial-form-open"  href="javascript:;">Dùng thử miễn phí</a>
          </div>
       </div>
    </div>
    <div id="footer" class="footer">
       <div class="footer-menu">
          <div class="container">
             <div class="row">
                <div class="col-lg col-md-4 col-6">
                   <div class="title">Sapo.vn</div>
                   <ul>
                      <li>
                         <a href="//www.sapo.vn/ve-chung-toi.html?utm_campaign=cpn:ve_chung_toi-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Về chúng tôi</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/sapo-la-gi.html?utm_campaign=cpn:sapo_la_gi-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Sapo là gì ?</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/bang-gia.html?utm_campaign=cpn:bang_gia-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Bảng giá</a>
                      </li>
                      <li>
                         <a href="https://themes.sapo.vn/" target="_blank">Kho giao diện</a>
                      </li>
                      <li>
                         <a href="https://apps.sapo.vn/?utm_campaign=cpn:apps-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" target="_blank">Kho ứng dụng web</a>
                      </li>
                      <li>
                         <a href="//tuyendung.sapo.vn/?utm_campaign=cpn:tuyen_dung-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" target="_blank">Tuyển dụng</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/profile" target="_blank">Profile</a>
                      </li>
                   </ul>
                </div>
                <div class="col-lg col-md-4 col-6">
                   <div class="title">Kênh bán hàng</div>
                   <ul>
                      <li>
                         <a href="//www.sapo.vn/phan-mem-quan-ly-ban-hang.html?utm_campaign=cpn:kenh_ban_hang-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" rel="nofollow">Bán hàng tại cửa hàng</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/thiet-ke-website-ban-hang.html?utm_campaign=cpn:kenh_ban_hang-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" rel="nofollow">Bán hàng trên website</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/ban-hang-tren-facebook.html?utm_campaign=cpn:kenh_ban_hang-plm:footer&amp;utm_source=theme.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Bán hàng trên Facebook</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/ban-hang-tren-lazada.html?utm_campaign=cpn:kenh_ban_hang-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Bán hàng trên Lazada</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/omnichannel.html?utm_campaign=cpn:kenh_ban_hang-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Bán hàng đa kênh Omnichannel</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/ban-hang-tren-shopee.html?utm_campaign=cpn:kenh_ban_hang-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Bán hàng trên Shopee</a>
                      </li>
                   </ul>
                </div>
                <hr style="border-top-color: #2b2a32; width: 100%; margin: 25px 0 30px;" class="d-block d-md-none">
                <div class="col-lg col-md-4 col-6">
                   <div class="title"><a href="//sapo.vn/thiet-ke-website-ban-hang.html">Thiết kế website bán hàng</a></div>
                   <ul>
                      <li>
                         <a href="//www.sapo.vn/thiet-ke-web-thoi-trang.html?utm_campaign=cpn:thiet_ke_website_ban_hang-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Thiết kế web thời trang</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/thiet-ke-web-my-pham.html?utm_campaign=cpn:thiet_ke_website_ban_hang-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Thiết kế web mỹ phẩm</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/thiet-ke-web-bat-dong-san.html?utm_campaign=cpn:thiet_ke_website_ban_hang-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Thiết kế website bất động sản</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/thiet-ke-web-doanh-nghiep.html?utm_campaign=cpn:thiet_ke_website_ban_hang-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Thiết kế web doanh nghiệp</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/thiet-ke-web-nha-hang.html?utm_campaign=cpn:thiet_ke_website_ban_hang-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Thiết kế web nhà hàng</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/thiet-ke-web-khach-san.html?utm_campaign=cpn:thiet_ke_website_ban_hang-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Thiết kế website khách sạn</a>
                      </li>
                   </ul>
                </div>
                <div class="col-lg col-md-4 col-6">
                   <div class="title"><a href="//sapo.vn/phan-mem-quan-ly-ban-hang.html#ngành_hàng">Quản lý bán hàng</a></div>
                   <ul>
                      <li>
                         <a href="https://www.sapo.vn/quan-ly-cua-hang-thoi-trang.html?utm_campaign=cpn:cua_hang_thoi_trang-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Cửa hàng thời trang</a>
                      </li>
                      <li>
                         <a href="https://www.sapo.vn/quan-ly-cua-hang-tap-hoa.html?utm_campaign=cpn:cua_hang_tap_hoa%20-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Cửa hàng tạp hoá</a>
                      </li>
                      <li>
                         <a href="https://www.sapo.vn/quan-ly-sieu-thi-mini.html?utm_campaign=cpn:sieu_thi_mini-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Siêu thị mini</a>
                      </li>
                      <li>
                         <a href="https://www.sapo.vn/quan-ly-cua-hang-my-pham.html?utm_campaign=cpn:cua_hang_my_pham-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Cửa hàng mỹ phẩm</a>
                      </li>
                   </ul>
                </div>
                <hr style="border-top-color: #2b2a32; width: 100%; margin: 25px 0 30px;" class="d-block d-md-none">
                <div class="col-lg col-md-4 col-6">
                   <div class="title">Hợp tác</div>
                   <ul>
                      <li>
                         <a href="//www.sapo.vn/doi-tac.html?utm_campaign=cpn:hop_tac-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" rel="nofollow">Chương trình đối tác</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/nha-phat-trien-ung-dung.html?utm_campaign=cpn:hop_tac-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" rel="nofollow">Nhà phát triển ứng dụng</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/nha-thiet-ke-giao-dien.html?utm_campaign=cpn:hop_tac-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" rel="nofollow">Dành cho nhà thiết kế</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/nha-dau-tu.html?utm_campaign=cpn:hop_tac-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" rel="nofollow">Dành cho nhà đầu tư</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/thiet-ke-website-mien-phi.html?utm_campaign=cpn:hop_tac-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Thiết kế website miễn phí</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/ten-mien-mien-phi.html?utm_campaign=cpn:hop_tac-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" rel="nofollow">Tên miền miễn phí</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/dai-ly-ban-hang.html?utm_campaign=cpn:hop_tac-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" rel="nofollow">Chương trình đại lý</a>
                      </li>
                   </ul>
                </div>
                <div class="col-lg col-md-4 col-6 d-block d-lg-none">
                   <div class="title">Trợ giúp</div>
                   <ul>
                      <li>
                         <a target="_blank" href="https://support.sapo.vn/?utm_campaign=cpn:support-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" rel="nofollow">Trung tâm trợ giúp</a>
                      </li>
                      <li>
                         <a href="https://support.sapo.vn/tai-lieu-nha-phat-trien?utm_campaign=cpn:hop_tac-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" target="_blank" rel="nofollow">Tài liệu nhà phát triển</a>
                      </li>
                      <li>
                         <a href="https://support.sapo.vn/hinh-thuc-thanh-toan?utm_campaign=cpn:web_docs-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" rel="nofollow">Hình thức thanh toán</a>
                      </li>
                      <li>
                         <a href="https://support.sapo.vn/huong-dan-dang-nhap-he-thong-sapo?utm_campaign=cpn:web_docs-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" rel="nofollow">Hướng dẫn đăng nhập Sapo</a>
                      </li>
                      <li>
                         <a target="_blank" href="https://www.sapo.vn/blog/?utm_campaign=cpn:blog-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Blog Sapo</a>
                      </li>
                      <li>
                         <a href="https://www.sapo.vn/seo-sapo-web.html?utm_campaign=cpn:seo_sapoweb-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" rel="nofollow">SEO Sapo Web</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/thiet-ke-website-mien-phi.html?utm_campaign=cpn:hop_tac-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" rel="nofollow">Quy định sử dụng</a>
                      </li>
                      <li>
                         <a href="https://support.sapo.vn/chinh-sach-bao-mat?utm_campaign=cpn:web_docs-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" rel="nofollow">Chính sách bảo mật</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/lien-he.html?utm_campaign=cpn:lien_he-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Liên hệ</a>
                      </li>
                   </ul>
                </div>
             </div>
          </div>
       </div>
       <div class="footer-address">
          <div class="container">
             <div class="row">
                <div class="col-w-20 col-md-4 d-lg-block d-none">
                   <div class="title">Trợ giúp</div>
                   <ul>
                      <li>
                         <a target="_blank" href="https://support.sapo.vn/?utm_campaign=cpn:support-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Trung tâm trợ giúp</a>
                      </li>
                      <li>
                         <a href="https://support.sapo.vn/tai-lieu-nha-phat-trien?utm_campaign=cpn:hop_tac-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" target="_blank">Tài liệu nhà phát triển</a>
                      </li>
                      <li>
                         <a href="https://support.sapo.vn/hinh-thuc-thanh-toan?utm_campaign=cpn:web_docs-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Hình thức thanh toán</a>
                      </li>
                      <li>
                         <a href="https://support.sapo.vn/huong-dan-dang-nhap-he-thong-sapo?utm_campaign=cpn:web_docs-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Hướng dẫn đăng nhập Sapo</a>
                      </li>
                      <li>
                         <a target="_blank" href="https://www.sapo.vn/blog/?utm_campaign=cpn:blog-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Blog Sapo</a>
                      </li>
                      <li>
                         <a href="https://www.sapo.vn/seo-sapo-web.html?utm_campaign=cpn:seo_sapoweb-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">SEO Sapo Web</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/thiet-ke-website-mien-phi.html?utm_campaign=cpn:hop_tac-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Quy định sử dụng</a>
                      </li>
                      <li>
                         <a href="https://support.sapo.vn/chinh-sach-bao-mat?utm_campaign=cpn:web_docs-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Chính sách bảo mật</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/lien-he.html?utm_campaign=cpn:lien_he-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Liên hệ</a>
                      </li>
                   </ul>
                </div>
                <div class="col-w-20 col-md-4">
                   <div class="title">Dịch vụ</div>
                   <ul>
                      <li>
                         <a href="//www.sapo.vn/dang-ky-ten-mien.html?utm_campaign=cpn:dang_ky_ten_mien-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Đăng kí tên miền</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/dich-vu-email-doanh-nghiep.html?utm_campaign=cpn:email_doanh_nghiep-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Email doanh nghiệp</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/Themes/Portal/Default/Contents/bang-gia-dich-vu-support-don-le-sapo-x.pdf" target="_blank" rel="nofollow">Dịch vụ support</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/xay-dung-chien-luoc-noi-dung.html?utm_campaign=cpn:xay_dung_chien_luoc_noi_dung-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Dịch vụ chiến lược nội dung</a>
                      </li>
                      <li>
                         <a href="//www.sapo.vn/cham-soc-noi-dung.html?utm_campaign=cpn:cham_soc_noi_dung-plm:footer&amp;utm_source=theme.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo">Dịch vụ chăm sóc nội dung</a>
                      </li>
                   </ul>
                   <div class="title shared-experience"><a href="https://shop.sapo.vn/?utm_campaign=cpn:sapo_shop-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" target="_blank">Thiết bị bán hàng</a></div>
                   <ul>
                      <li>
                         <a href="https://shop.sapo.vn/may-in-hoa-don-may-in-bill?utm_campaign=cpn:sapo_shop-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" target="_blank">Máy in hóa đơn</a>
                      </li>
                      <li>
                         <a href="https://shop.sapo.vn/may-in-ma-vach?utm_campaign=cpn:sapo_shop-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" target="_blank">Máy in mã vạch</a>
                      </li>
                      <li>
                         <a href="https://shop.sapo.vn/may-quet-ma-vach?utm_campaign=cpn:sapo_shop-plm:footer&amp;utm_source=themes.sapo.vn&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=footer_theme_sapo" target="_blank">Máy quét mã vạch</a>
                      </li>
                   </ul>
                </div>
                <div class="col-w-60 col-md-8">
                   <div class="title">Liên hệ</div>
                   <ul class="social">
                      <li>
                         <a href="https://www.facebook.com/sapo.vn/" target="_blank" rel="nofollow"><i class="fa fa-facebook-square"></i></a>
                      </li>
                      <li>
                         <a href="https://www.youtube.com/channel/UCXOMQd_gKgELyY_fhmPjI4w" target="_blank" rel="nofollow"><i class="fa fa-youtube"></i></a>
                      </li>
                      <li>
                         <a href="https://www.sapo.vn/blog" target="_blank"><i class="fa fa-weixin"></i></a>
                      </li>
                   </ul>
                   <div class="contact-info">
                      <p class="text-uppercase">Công ty cổ phần công nghệ Sapo (Sapo Technology JSC)</p>
                      <p><span>Trụ sở <i class="fa fa-map-marker"></i> </span>Tầng 6 - Tòa nhà Ladeco - 266 Đội Cấn - Phường Liễu Giai - Quận Ba Đình - TP Hà Nội</p>
                      <p>
                         <span class="d-xl-inline d-block">Chi nhánh </span><i class="fa fa-map-marker"></i> Lầu 3 - Tòa nhà Lữ Gia - Số 70 Lữ Gia - Phường 15 - Quận 11 - TP Hồ Chí Minh
                      </p>
                      <p>
                         <span style="visibility: hidden" class="d-none d-xl-inline">Chi nhánh </span><i class="fa fa-map-marker"></i> Số 124 - Đường Lê Đình Lý - Phường Vĩnh Trung - Quận Thanh Khê - TP Đà Nẵng
                      </p>
                      <p>
                         <span style="visibility: hidden" class="d-none d-xl-inline">Chi nhánh </span><i class="fa fa-map-marker"></i> Số 127 - Đường Lý Thường Kiệt - Phường Lê Lợi - TP Vinh - Tỉnh Nghệ An
                      </p>
                      <p><span>Tổng đài tư vấn và hỗ trợ khách hàng: </span><b>1800 6750</b></p>
                      <p><span>Email: </span><b>support@sapo.vn</b></p>
                      <p>Từ 8h00 – 22h00 các ngày từ thứ 2 đến Chủ nhật</p>
                   </div>
                   <a href="http://online.gov.vn/Home/WebDetails/44190" target="_blank" rel="nofollow" class="footer-bct">
                   <img src="/Themes/Portal/Default/Images/bocongthuong.png" alt="Chứng nhận Bộ Công Thương">
                   </a>
                </div>
             </div>
          </div>
       </div>
       <div class="footer-copyright">
          <div class="container">
             <div class="row">
                <div class="col-lg-7 col-md-6 order-md-1 order-3">
                   <p class="copyright"><span class="d-none d-lg-inline-block">Copyright © 2019&nbsp;</span><a href="https://www.sapo.vn" target="_blank">Sapo.vn</a> - Nền tảng bán hàng đa kênh được sử dụng nhiều nhất Việt Nam</p>
                </div>
                <hr style="border-top-color: #2b2a32; width: 100%;margin:0;" class="d-block d-sm-none order-2">
                <div class="col-lg-5 col-md-6 order-md-2 order-1">
                   <p class="achievement">
                      <span>Sản phẩm đạt giải: <br class="d-block d-lg-none">Nhân tài Đất Việt 2013 &amp; Sao Khuê  2015</span>
                      <span class="icon-achievement"><img src="//www.sapo.vn/Themes/Portal/Default/Styles_New/images/icon-cup.png"></span>
                   </p>
                </div>
             </div>
          </div>
          <p class="copyright-mobile d-lg-none d-md-block d-none">© Copyright 2008 - 2019</p>
       </div>
    </div>
    <div id="free-trial" style="display: none;">
       <a class="close"><span class="ti-close"></span></a>
       <iframe id="trial-frame" src="" width="100%" height="100%" frameborder="0"></iframe>
    </div>
    <script>
       $(function () {
           $("#free-trial .close").click(function () {
               var modal = document.getElementById('free-trial');
               $('body').removeClass('open');
               modal.style.display = "none";
               $("#free-trial iframe").attr("src", "");
           });
           $('[data-toggle="tooltip"]').tooltip();
       });
    </script>
    <script type="text/javascript">
       var LAST_STORE_COOKIE_NAME = "last_store";
       
       $(function () {
           var lastStore = getCookie(LAST_STORE_COOKIE_NAME);
           if (lastStore !== null && lastStore !== "") {
               $(".subdomain").removeClass("hide");
               $("#login-form input[name=Subdomain]").val(lastStore);
               $("#login-form input[name=Subdomain]").attr("aria-required", "true");
               $("#login-form input[name=Subdomain]").attr("data-val-required", "Nhập vào đường dẫn website");
               $("#login-form").removeData("validator");
               $("#login-form").removeData("unobtrusiveValidation");
               $.validator.unobtrusive.parse($("#login-form"));
           }
       });
       
       $(document).ready(function () {
           //luu thong tin tracking vao cookie
       
           var aff_id_ck = getCookie("aff_id");
           var aff_id = getParameterByName("aff_id");
           var aff_tracking_id = getParameterByName("aff_tracking_id");
           if (aff_id_ck == null || aff_id_ck == "") {
               if (aff_id !== null && aff_id !== "") {
                   setCookie("aff_id", aff_id, 30);
               }
       
               if (aff_tracking_id !== null && aff_tracking_id !== "") {
                   setCookie("aff_tracking_id", aff_tracking_id, 30);
               }
           }
           else {
               if (aff_id == aff_id_ck) {
                   if (aff_tracking_id !== null && aff_tracking_id !== "") {
                       setCookie("aff_tracking_id", aff_tracking_id, 30);
                   }
               }
           }
       
           var kd = getParameterByName("kd");
           if (kd !== null && kd !== "")
               setCookie("kd", kd, 30);
       
           var ref = getParameterByName("ref");
           if (ref !== null && ref !== "")
               setCookie("ref", ref, 30);
       
           var campaign = getParameterByName("campaign");
           if (campaign !== null && campaign !== "")
               setCookie("campaign", campaign, 30);
       
           if (document.referrer && document.referrer != '') {
               if (document.referrer.indexOf("www.sapo.vn") == -1) {
                   setCookie("referral", document.referrer, 30);
               }
           }
       
           var partner = getParameterByName("aff_partner_id");
           if (partner !== null && partner !== "")
               setCookie("partner", partner, 30);
       
           var landingPage = getCookie("landing_page");
           if (landingPage == null || landingPage == "") {
               setCookie("landing_page", document.location.href, 0.0115);
           }
       
           var startTime = getCookie("start_time");
           if (startTime == null || startTime == "") {
               setCookie("start_time", "12/26/2019 10:48:43", 0.0115);
           }
       
           var pageview = getCookie("pageview");
           if (pageview == null || pageview == "") {
               setCookie("pageview", 1, 0.0115);
           }
           else {
               setCookie("pageview", parseInt(pageview) + 1, 0.0115);
           }
       
           var updateCookie = setInterval(renewFirstPageCookie, 15 * 60 * 1000);
       
           //check orient change
           window.addEventListener("orientationchange", function () {
           }, false)
       
       });
       
       function showTrialForm(e, type, chkOmni) {
           var url = "https://app.sapo.vn/services/signup";
           url = setParameter(url, "Type", type);
       
           if (chkOmni) {
               url = setParameter(url, "PreferredService", "OMNI");
           }
       
           var storeName = $(e).parent().parent().find(".input-site-name").val();
           if (storeName != null && storeName != "")
               url = setParameter(url, "StoreName", storeName);
       
           var kd = getParameterByName("kd");
           if (kd !== null && kd !== "")
               setCookie("kd", kd, 30);
       
           kd = getCookie("kd");
           if (kd !== null && kd !== "")
               url = setParameter(url, "SaleName", kd);
       
           var ref = getParameterByName("ref");
           if (ref !== null && ref !== "")
               setCookie("ref", ref, 30);
       
           ref = getCookie("ref");
           if (ref !== null && ref !== "")
               url = setParameter(url, "Reference", ref);
       
           if (window.location.href && window.location.href != '') {
               url = setParameter(url, "Source", encodeURIComponent(window.location.href));
           }
       
           var referral = getCookie("referral");
           if (referral !== null && referral !== "")
               url = setParameter(url, "Referral", encodeURIComponent(referral));
       
           var campaign = getParameterByName("campaign");
           if (campaign !== null && campaign !== "")
               setCookie("campaign", campaign, 30);
       
           campaign = getCookie("campaign");
           if (campaign !== null && campaign !== "")
               url = setParameter(url, "Campaign", campaign);
       
           var landingPage = getCookie("landing_page");
           if (landingPage !== null && landingPage !== "")
               url = setParameter(url, "LandingPage", encodeURIComponent(landingPage));
       
           var startTime = getCookie("start_time");
           if (startTime !== null && startTime !== "")
               url = setParameter(url, "StartTime", encodeURIComponent(startTime));
       
           var endTime = "12/26/2019 10:48:43";
           if (endTime !== null && endTime !== "")
               url = setParameter(url, "EndTime", encodeURIComponent(endTime));
       
           var pageview = getCookie("pageview");
           if (pageview !== null && pageview !== "")
               url = setParameter(url, "Pageview", pageview);
       
           var aff_id = getCookie("aff_id");
           if (aff_id !== null && aff_id !== "") {
               url = setParameter(url, "AffId", aff_id);
           }
       
           var aff_tracking_id = getCookie("aff_tracking_id");
           if (aff_tracking_id !== null && aff_tracking_id !== "") {
               url = setParameter(url, "AffTrackingId", aff_tracking_id);
           }
       
           var partner = getCookie("partner");
           if (partner !== null && partner !== "") {
               url = setParameter(url, "partner", partner);
           }
       
           $('#free-trial iframe').attr("src", url);
           $('body').addClass('open');
           $('#free-trial').fadeIn('fast');
       }
       
       function renewFirstPageCookie() {
           var landingPage = getCookie("landing_page");
           if (landingPage !== null && landingPage !== "") {
               setCookie("landing_page", landingPage, 0.0115);
           }
       
           var startTime = getCookie("start_time");
           if (startTime !== null && startTime !== "") {
               setCookie("start_time", startTime, 0.0115);
           }
       
           var pageview = getCookie("pageview");
           if (pageview !== null || pageview !== "") {
               setCookie("pageview", pageview, 0.0115);
           }
       }
       
       function bodauTiengViet(str) {
           str = str.toLowerCase();
           str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
           str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
           str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
           str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
           str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
           str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
           str = str.replace(/đ/g, "d");
           return str;
       }
       
       var mobile = false;
       $(window).resize(function () {
           var ww = $(window).width();
           if (ww < 768) {
               if (!$('#login-div br').length > 0)
                   $('#login-div .popup-login-text').before('<br/>');
           }
           else {
               $('#login-div br').remove();
           }
       })
       $(window).trigger('resize');
       
       function getParameterByName(name) {
           name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
           var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
               results = regex.exec(location.search);
           return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
       }
       
       function onInputStoreName(e, element) {
           if (e.keyCode == 13) {
       
               return false;
           }
       }
       
       function generateAlias(text) {
           text = text.toLowerCase();
           text = text.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
           text = text.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
           text = text.replace(/ì|í|ị|ỉ|ĩ/g, "i");
           text = text.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
           text = text.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
           text = text.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
           text = text.replace(/đ/g, "d");
           text = text.replace(/'|\"|\(|\)|\[|\]/g, "");
           text = text.replace(/\W+/g, "-");
           if (text.slice(-1) === "-")
               text = text.replace(/-+$/, "");
       
           if (text.slice(0, 1) === "-")
               text = text.replace(/^-+/, "");
       
           return text;
       }
       
       function setCookie(cname, cvalue, exdays) {
           if (!exdays)
               exdays = 30;
       
           var d = new Date();
           d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
           var expires = "expires=" + d.toGMTString();
           document.cookie = cname + "=" + cvalue + "; " + expires + ";domain=.sapo.vn;path=/";
       }
       
       function newSetCookie(cname, cvalue) {
           document.cookie = cname + "=" + cvalue;
       }
       
       function getUrlWithoutDomain(url) {
           return url.replace(/^.*\/\/[^\/]+/, '');
       }
       
       function getCookie(cname) {
           var name = cname + "=";
           var ca = document.cookie.split(';');
           for (var i = 0; i < ca.length; i++) {
               var c = ca[i];
               while (c.charAt(0) == ' ') c = c.substring(1);
               if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
           }
           return null;
       }
       
       function getSessionStorage(sname) {
           return window.sessionStorage.getItem(sname);
       }
       
       function setSessionStorage(sname, svalue) {
           window.sessionStorage.setItem(sname, svalue);
       }
       
       function guid() {
           return 'xxxxxxxx-xxxx-4xxx-yxxx'.replace(/[xy]/g, function (c) {
               var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
               return v.toString(16);
           }).toUpperCase();
       }
       
       function setParameter(url, paramName, paramValue) {
           if (url.indexOf(paramName + "=") >= 0) {
               var prefix = url.substring(0, url.indexOf(paramName));
               var suffix = url.substring(url.indexOf(paramName));
               suffix = suffix.substring(suffix.indexOf("=") + 1);
               suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
               url = prefix + paramName + "=" + paramValue + suffix;
           }
           else {
               if (url.indexOf("?") < 0)
                   url += "?" + paramName + "=" + paramValue;
               else
                   url += "&" + paramName + "=" + paramValue;
           }
       
           return url;
       }
    </script>
 </div>
@endsection