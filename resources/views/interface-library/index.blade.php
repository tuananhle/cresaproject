@extends('layouts.master')
@section('title') Template, Giao diện mẫu web chuẩn seo @endsection
@section('description') Cresa sở hữu kho giao diện đa dạng, chuẩn seo @endsection 
@section('url') {{url('kho-giao-dien/'.'all'.'html')}} @endsection 
@section('keyword') Giao diện website, giao dien , website, giao dien websie chuan seo, giao dien web dep @endsection
@section('image') {{ url('/').$setting->logo }} @endsection
@section('css')
    {{-- <link href="{{url('frontend/asset/css/index.min.css')}}" rel="preload" as="style" type="text/css" />
    <link href="{{url('frontend/asset/css/popup-register-default.min.css')}}" rel="stylesheet" /> --}}
@endsection
@section('js')
    <link href="{{url('frontend/asset/css/search.min.css')}}" rel="stylesheet" />
    <link href="{{url('frontend/asset/css/popup-register-default.min.css')}}" rel="stylesheet">
    <script src="{{url('frontend/asset/js/script.js')}}"></script>
    <script src="{{url('frontend/asset/js/popup-trial.min.js')}}"></script>
@endsection
@section('content')
<div id="wrapper" class="default-wrapper">
    <script type="text/javascript">
       function openMenu() {
           $('body').css('overflow', 'hidden');
           $('.overlay-menu').fadeIn(300);
           $('.menu-mobile').addClass('show');
       }
       function closeMenu() {
           $('body').css('overflow', '');
           $('.overlay-menu').fadeOut(300);
           $('.menu-mobile').removeClass('show');
       }
       $(window).on('load', function () {
           var itemTop = $('.header .main-header').offset().top + $('.header .main-header').height() + $('.header .extra-header').height();
           if ($(window).scrollTop() > itemTop) {
               $('.header .extra-header').addClass('sticky');
           }
           $(window).on('scroll', function () {
               if ($(this).scrollTop() > itemTop) {
                   $('.header .extra-header').addClass('sticky');
               } else {
                   $('.header .extra-header').removeClass('sticky');
               }
           });
       });
    </script>
    <div class="search-container">
       <div class="search-banner">
          <div class="container">
             <h3>Tất cả các giao diện mẫu <span>đa dạng, chuyên nghiệp</span> <br class="hidden-xs">dành cho website của bạn</h3>
          </div>
       </div>
       <div class="menu-toolbar">
          <div class="container">
             @include('interface-library.navbar')
          </div>
          <script type="text/javascript">
             $(function () {
                 $(".show-search-form").click(function () {
                     $(".search-form-mobile").toggle();
                     $(".show-search-form .fa-search").toggle();
                     $(".show-search-form .fa-times").toggle();
                 });
             
                 $(".show-search-tablet").click(function () {
                     $(this).parent().find(".filter-form").toggle();
                     $(".show-search-tablet .fa-search").toggle();
                     $(".show-search-tablet .fa-times").toggle();
                 });
             
                 $(".title-filter-mobile").click(function () {
                     $(".filter-mobile .filter-by").toggle();
                 });
             
                 $(".filter-mobile .filter-by .has-child").click(function () {
                     $(this).find(".sub-menu").toggle();
                 });
             
                 var price = getParameterByName("price");
                 var discount = getParameterByName("discount");
                 if (price == "all") {
                     $("#all-theme").addClass("active");
                 }
                 else if (price == "free") {
                     $("#free-theme").addClass("active");
                 }
                 else if (discount == "show") {
                     $("#promotion-theme").addClass("active");
                 }
             
                 var collection = getParameterByName("collection");
                 if (collection == 40) {
                     $("#mobile-theme").addClass("active");
                 }
             
                 var sort = getParameterByName("sort");
                 $(".sort-by li").removeClass("active");
                 $(".sort-by li[data-sort='" + sort + "']").addClass("active");
             
                 $(".sort-by li").click(function () {
                     $(".sort-by li").removeClass("active");
                     $(this).addClass("active");
                     var sort = $(this).attr("data-sort");
                     var queryString = window.location.search;
                     var url = "/search" + queryString;
                     url = setParameter(url, "sort", sort);
                     window.location.href = url;
                 });
             
                 var key = getParameterByName("key");
                 if (key != "") {
                     $(".filter-form input[name='key']").val(key);
                 }
             });
          </script>
       </div>
       <div class="themes-list">
          <div class="container">
            @if(count($theme) > 0 )
             <h1 class="title">
                Danh sách giao diện 
             </h1>
             @endif
             <div class="row list-items">
                @if(count($theme) < 1 )
                <h5 style="text-align: center; margin-left: 32%; font-style: italic; color: red;">
                    Thông cảm! Chúng tôi vẫn đang cập nhật giao diện cho mục này
                 </h5><br>
                 <div>
                    <img src="{{url('frontend/waiting.png')}}" width="50%" style="margin-left: 33%;" alt="Evo Tour">
                 </div>
                @else 
                    @foreach($theme as $item)
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                    <div class="theme-item responsive">
                        <div class="theme-image">
                            @php
                                $images = json_decode($item->images);
                            @endphp
                            <img src="{{url('/').$images[0]}}" alt="{{$item->name}}">
                            <div class="theme-action">
                                <div class="button">
                                <a href="{{$item->link_demo}}" class="view-demo action-preview-theme" data-url="{{$item->link_demo}}" target="_blank">Xem thử</a>
                                <a href="{{url('chi-tiet/'.$item->id)}}" class="view-detail">Chi tiết</a>
                                </div>
                            </div>
                        </div>
                        <div class="theme-info">
                        <h3><a href="/evo-tour" class="title">{{$item->name}}</a></h3>
                            <span class="price ">
                            <b>1,500,000 VNĐ</b>
                            </span>
                        </div>
                    </div>
                    </div>
                    @endforeach
                @endif
                
             </div>
             @if(count($theme) > 0 )
             <div class="text-center">
                {{$theme->links()}}
             </div>
             @endif
          </div>
       </div>
    </div>
    <div class="clear"></div>
    <img class="scroll-top" src="{{url('frontend/images/totop.png')}}" style="cursor: pointer; position: fixed; bottom: 90px; right: 33px; z-index: 99999; display: none;">
    <script>
       $(document).ready(function () {
           $(window).scroll(function () {
               if ($(window).scrollTop() > 700) {
                   $('.scroll-top').show();
               }
               else {
                   $('.scroll-top').hide();
               }
           });
           $('.scroll-top').click(function () {
               $("html, body").animate({ scrollTop: 0 }, "slow");
           });
           if ($(window).width() < 768) {
               $('.scroll-top').css({ 'bottom': '90px', 'right': '8px' });
           }
       });
    </script>
    <div id="free-trial" style="display: none;">
       <a class="close"><span class="ti-close"></span></a>
       <iframe id="trial-frame" src="" width="100%" height="100%" frameborder="0"></iframe>
    </div>
    <script>
       $(function () {
           $("#free-trial .close").click(function () {
               var modal = document.getElementById('free-trial');
               $('body').removeClass('open');
               modal.style.display = "none";
               $("#free-trial iframe").attr("src", "");
           });
           $('[data-toggle="tooltip"]').tooltip();
       });
    </script>
 </div>
@endsection