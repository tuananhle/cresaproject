<div class="row filter-desktop d-none d-md-flex">
    <div class="col-lg-8 col-md-11 menu">
       <ul>
         <li id="all-theme" class=" @if(isset(Route::current()->parameters['type']) && Route::current()->parameters['type'] == 'all') active @endif"><a href="{{url('kho-giao-dien/'.'all'.'.html')}}">Tất cả</a></li>
          <li id="promotion-theme" class="@if(isset(Route::current()->parameters['type']) && Route::current()->parameters['type'] == 'discount') active @endif" ><a href="{{url('kho-giao-dien/'.'discount')}}">Khuyến mãi</a></li>
          <li class="has-child">
             <a href="javascript:;">Bán hàng</a>
             <i class="fas fa-angle-down"></i>
             <ul class="sub-menu clearfix">
                @foreach($cate_ban_hang as $item)
                <li>
                   <a href="{{url('kho-giao-dien/'.$item->quiz_id.'.html')}}">
                   <span>{{$item->name}}</span>
                   </a>
                </li>
                @endforeach
             </ul>
          </li>
          <li class="has-child">
             <a href="javascript:;">Doanh nghiệp</a>
             <i class="fas fa-angle-down"></i>
             <ul class="sub-menu">
                @foreach($doanh_nghiep as $item)
                <li>
                   <a href="{{url('kho-giao-dien/'.$item->quiz_id.'.html')}}">
                   <span>{{$item->name}}</span>
                   </a>
                </li>
                @endforeach
             </ul>
          </li>
       </ul>
    </div>
    <div class="col-lg-4 col-md-1 d-none d-md-block">
       <a class="show-search-tablet d-none d-md-block d-lg-none visible-sm" href="javascript:;">
       <i class="fa fa-search" aria-hidden="true"></i>
       <i class="fa fa-times" style="display: none;" aria-hidden="true"></i>
       </a>
       <div class="filter-form">
          <form action="{{route('searchinterface')}}" method="POST" novalidate="novalidate">
            @csrf
             <input type="text" placeholder="Bạn tìm giao diện gì?" name="keyword" class="search-key">
             <button type="submit"><i class="fas fa-search"></i></button>
          </form>
       </div>
    </div>
 </div>
 <div class="row filter-mobile d-flex d-md-none">
    <div class="col-12">
       <div class="title-filter-mobile">Lọc theo...</div>
       <a class="show-search-form" href="javascript:;">
       <i class="fas fa-search" aria-hidden="true"></i>
       <i class="fas fa-times" style="display: none;" aria-hidden="true"></i>
       </a>
       <div class="search-form-mobile">
          <form action="{{route('searchinterface')}}" method="POST" novalidate="novalidate">
            @csrf
             <input type="text" placeholder="Tìm kiếm giao diện ..." name="keyword" class="search-key">
             <button><i class="fas fa-search" aria-hidden="true"></i></button>
          </form>
       </div>
       <div class="filter-by">
          <ul>
             <li id="all-theme"><a href="{{url('kho-giao-dien/'.'all'.'.html')}}">Tất cả</a></li>
             <li id="promotion-theme"><a href="{{url('kho-giao-dien/'.'discount')}}">Khuyến mãi</a></li>
             <li class="has-child">
                <a>Bán hàng</a>
                <i class="fa fa-angle-down" aria-hidden="true"></i>
                <ul class="sub-menu clearfix">
                  @foreach($cate_ban_hang as $item)
                   <li>
                      <a href="{{url('kho-giao-dien/'.$item->quiz_id.'.html')}}">
                      <span>{{$item->name}}</span>
                      </a>
                   </li>
                   @endforeach
                </ul>
             </li>
             <li class="has-child">
               <a>Doanh nghiệp</a>
               <i class="fa fa-angle-down" aria-hidden="true"></i>
               <ul class="sub-menu clearfix">
                 @foreach($doanh_nghiep as $item)
                  <li>
                     <a href="{{url('kho-giao-dien/'.$item->quiz_id.'.html')}}">
                     <span>{{$item->name}}</span>
                     </a>
                  </li>
                  @endforeach
               </ul>
            </li>
          </ul>
       </div>
    </div>
 </div>