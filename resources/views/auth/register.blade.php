@extends('layouts.master')
@section('css')
<link href="{{asset('frontend/asset/css/evo-accounts.scss.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<section class="bread-crumb margin-bottom-10">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
                        <li class="home"><a itemprop="url" href="/" title="Trang chủ"><span itemprop="title">Trang chủ</span></a><span><i class="fa fa-angle-right"></i></span></li>
                        
                        <li><strong itemprop="title">Đăng ký tài khoản</strong></li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </section>
<div class="container margin-bottom-20 margin-top-30">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="page-login account-box-shadow">
                <div id="login">
                <h1 class="title-head text-center">Đăng ký tài khoản</h1>
                <div class="text-center fix-sblock">Đăng nhập để mua hàng và sử dụng những tiện ích mới nhất từ <strong>Ribeto</strong></div>
                {{-- <div class="social-login text-center margin-bottom-10">
                    <script>function loginFacebook(){var a={client_id:"947410958642584",redirect_uri:"https://store.bizwebvietnam.net/account/facebook_account_callback",state:JSON.stringify({redirect_url:window.location.href}),scope:"email",response_type:"code"},b="https://www.facebook.com/v3.2/dialog/oauth"+encodeURIParams(a,!0);window.location.href=b}function loginGoogle(){var a={client_id:"885968593373-197u9i4pte44vmvcc0j50pvhlfvl27ds.apps.googleusercontent.com",redirect_uri:"https://store.bizwebvietnam.net/account/google_account_callback",scope:"email profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile",access_type:"online",state:JSON.stringify({redirect_url:window.location.href}),response_type:"code"},b="https://accounts.google.com/o/oauth2/v2/auth"+encodeURIParams(a,!0);window.location.href=b}function encodeURIParams(a,b){var c=[];for(var d in a)if(a.hasOwnProperty(d)){var e=a[d];null!=e&&c.push(encodeURIComponent(d)+"="+encodeURIComponent(e))}return 0==c.length?"":(b?"?":"")+c.join("&")}</script> 
                    <a href="javascript:void(0)" class="social-login--facebook" onclick="loginFacebook()"><img width="129px" height="37px" alt="facebook-login-button" src="//bizweb.dktcdn.net/assets/admin/images/login/fb-btn.svg"></a> 
                    <a href="javascript:void(0)" class="social-login--google" onclick="loginGoogle()"><img width="129px" height="37px" alt="google-login-button" src="//bizweb.dktcdn.net/assets/admin/images/login/gp-btn.svg"></a>
                </div> --}}
            <form accept-charset="UTF-8" action="{{route('register.post')}}" id="customer_register" method="post" class="has-validation-callback">
                    @csrf
                    @if ($errors->has('email')) 
                        <div class="form-signup" style="color:red">Email đã tồn tại. <br>
                        </div>
                    @endif
                    <div class="form-signup clearfix">
                        <div class="row">
                            <div class="col-md-6">
                            <fieldset class="form-group">
                                <label>Họ<span class="required">*</span></label>
                                <input placeholder="Nhập Họ" type="text" class="form-control form-control-lg" value="" name="lastName" id="lastName" required="" >
                               
                            </fieldset>
                            </div>
                            <div class="col-md-6">
                            <fieldset class="form-group">
                                <label>Tên<span class="required">*</span></label>
                                <input placeholder="Nhập Tên" type="text" class="form-control form-control-lg" value="" name="firstName" id="firstName" required="">
                            </fieldset>
                            </div>
                            <div class="col-md-12">
                            <fieldset class="form-group">
                                <label>Số điện thoại<span class="required">*</span></label>
                                <input placeholder="Nhập Số điện thoại" type="tel" class="number-sidebar form-control form-control-lg" value="" name="phone" id="phone" required="">
                            </fieldset>
                            </div>
                            <div class="col-md-12">
                            <fieldset class="form-group">
                                <label>Email<span class="required">*</span></label>
                                <input placeholder="Nhập Địa chỉ Email" type="email" class="form-control form-control-lg" value="" name="email" id="email" required="">
                            </fieldset>
                            </div>
                            <div class="col-md-12">
                            <fieldset class="form-group">
                                <label>Mật khẩu<span class="required">*</span></label>
                                <input placeholder="Nhập Mật khẩu" type="password" class="form-control form-control-lg" value="" name="password" id="password" required="">
                            </fieldset>
                            </div>
                            <div class="col-md-12 text-center" style="margin-top:15px;">
                            <button type="submit" value="Đăng ký" class="btn btn-style btn-blues">Tạo tài khoản</button>
                            <a href="/account/login" title="Đăng nhập" class="btn btn-register btn-register-login">Đăng nhập</a>
                            </div>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection