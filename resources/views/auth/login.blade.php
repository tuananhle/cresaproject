@extends('layouts.master')
@section('css')
<link href="{{asset('frontend/asset/css/evo-accounts.scss.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<section class="bread-crumb margin-bottom-10">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
                        <li class="home"><a itemprop="url" href="/" title="Trang chủ"><span itemprop="title">Trang chủ</span></a><span><i class="fa fa-angle-right"></i></span></li>
                        
                        <li><strong itemprop="title">Đăng nhập tài khoản</strong></li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <div class="container margin-bottom-20 margin-top-30">
            <div class="row">
               <div class="col-md-8 col-md-offset-2">
                  <div class="page-login account-box-shadow">
                     <div id="login">
                        <div class="text-center margin-bottom-30">
                           <h1 class="title-head">Đăng nhập tài khoản</h1>
                           <p>
                              Đăng nhập để mua hàng và sử dụng những tiện ích mới nhất từ <strong>ribeto</strong>
                           </p>
                        </div>
                    <form accept-charset="UTF-8" action="{{route('login.index')}}" id="customer_login" method="post" class="has-validation-callback">
                            @csrf
                            {{-- @if(Session::has('error'))
                                <div class="form-signup" style="color:red">
                                    Thông tin đăng nhập không chính xác. <br>
                                </div>
                            @endif
                            @if(Session::has('message'))
                                <div class="form-signup" style="color:green">
                                    {{ Session::get('success') }} <br>
                                </div>
                             @elseif(Session::has('errors'))
                                <div class="form-signup" style="color:red">
                                    Email không tồn tại <br>
                                </div>
                            @endif --}}
                           <div class="form-signup clearfix">
                              <fieldset class="form-group margin-bottom-20">
                                 <label>Email<span class="required">*</span></label>
                                 <input autocomplete="off" placeholder="Nhập Địa chỉ Email" type="email" class="form-control form-control-lg" value="" name="email" id="customer_email" required="">
                              </fieldset>
                              <fieldset class="form-group">
                                 <label>Mật khẩu<span class="required">*</span></label>
                                 <input autocomplete="off" placeholder="Nhập Mật khẩu" type="password" class="form-control form-control-lg" value="" name="password" id="customer_password" >
                              </fieldset>
                              <div class="pull-xs-left text-center" style="margin-top: 15px;">
                                 <button class="btn btn-style btn-blues" type="submit" value="Đăng nhập">Đăng nhập</button>
                              </div>
                              <div class="clearfix"></div>
                              <p class="text-center">
                                 <a href="#recover" class="btn-link-style" onclick="showRecoverPasswordForm();" title="Quên mật khẩu?">Quên mật khẩu?</a>
                              </p>
                              <div class="text-login text-center">
                                 <p>
                                    Bạn chưa có tài khoản. Đăng ký <a href="{{route('register.index')}}" title="Đăng ký">tại đây.</a>
                                 </p>
                              </div>
                           </div>
                        </form>
                     </div>
                     <div id="recover-password" class="form-signup" style="display:none;">
                        <div class="text-center">
                           <h2 class="title-head"><span>Đặt lại mật khẩu</span></h2>
                        </div>
                        <div class="fix-sblock text-center">
                           Bạn quên mật khẩu? Nhập địa chỉ email để lấy lại mật khẩu qua email.
                        </div>
                        <form accept-charset="UTF-8" action="{{route('resetPassword.index')}}" id="recover_customer_password" method="post" class="has-validation-callback">
                          @csrf
                           @if(Session::has('message'))
                                <div class="form-signup" style="color:red">
                                    Chúng tôi sẽ gửi thông báo về email của bạn <br>
                                </div>
                            @elseif(Session::has('errors'))
                                <div class="form-signup" style="color:red">
                                    Email không tồn tại <br>
                                </div>
                            @endif
                           <div class="form-signup clearfix">
                              <fieldset class="form-group">
                                 <label>Email<span class="required">*</span></label>
                                 <input type="email" class="form-control form-control-lg" value="" name="email" id="recover-email" placeholder="Nhập địa chỉ Email" required="">
                              </fieldset>
                           </div>
                           <div class="action_bottom text-center">
                              <button class="btn btn-style btn-blues" style="margin-top: 15px;" type="submit">Lấy lại mật khẩu</button>
                           </div>
                           <div class="text-login text-center">
                              <p>Quay lại <a href="javascript:;" class="btn-link-style btn-register" onclick="hideRecoverPasswordForm();" title="Quay lại">tại đây.</a></p>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
@endsection
<script type="text/javascript">
	function showRecoverPasswordForm() {
		document.getElementById('recover-password').style.display = 'block';
		document.getElementById('login').style.display='none';
	}

	function hideRecoverPasswordForm() {
		document.getElementById('recover-password').style.display = 'none';
		document.getElementById('login').style.display = 'block';
	}
</script>