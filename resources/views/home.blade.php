@extends('layouts.master')
@section('title') Cresa | Thiết kế website chuyên nghiệp @endsection
@section('description') {{$setting->company}} @endsection 
@section('url') {{ $setting->webname }} @endsection
@section('keyword') Thiết kế website, website, dịch vụ website, web chuẩn seo, seo website @endsection
@section('image') {{ $setting->logo }} @endsection
@section('css')
    <link href="{{url('frontend/asset/css/ld-sellatwebsite.min.css')}}" rel="preload" as="style" type="text/css" />
    <link href="{{url('frontend/asset/css/ld-sellatwebsite.min.css')}}" rel="stylesheet" />
@endsection
@section('content')
<div id="wrapper" class="clearfix">
    <div class="ld-sellatwebsite">
       <div class="banner">
          <div class="container">
             <div class="row">
                <div class="col-lg-6 col-12 block-content">
                   <h1>Thiết kế website chuyên nghiệp<br class="d-none d-md-block" /></h1>
                   <p>Hơn <b>1000</b> chủ shop, doanh nghiệp tin dùng</p>
                <a class="btn-registration"  href="{{url('kho-giao-dien/'.'all'.'.html')}}">Kho giao diện</a>
                </div>
                <div class="col-lg-6 d-none d-lg-block block-img">
                <img src="{{url('frontend/images/thiet-ke-website-chuyen-nghiep.png')}}" alt="Thiết kế website giá rẻ, chuẩn seo" />
                </div>
             </div>
          </div>
          <a href="javascript:;" data-href=".benefit" class="scroll-down">
          <i class="fas fa-angle-down"></i>
          </a>
       </div>
       <div class="benefit text-center">
          <div class="container">
             <h2><span>Thiết kế website bán hàng với Cresa và</span>4 lợi ích không thể phủ nhận đối với các <br class="d-none d-md-block" />cửa hàng và doanh nghiệp</h2>
             <div class="row">
                <div class="swiper-container">
                   <div class="swiper-wrapper">
                      <div class="swiper-slide">
                         <div class="item">
                            <span class="number one"></span>
                            <h3>Tiếp cận 61% người mua hàng trên website</h3>
                            <p>
                               Mặc dù xu hướng mua hàng trên sàn TMĐT, mạng xã hội <br class="d-none d-xl-block" />
                               ngày càng cao, nhất là Facebook, Shopee và Lazada, <br class="d-none d-xl-block" />
                               thế nhưng theo nghiên cứu của Moore, khách hàng vẫn <br class="d-none d-xl-block" />
                               tin tưởng lựa chọn website là nơi mua sắm uy tín.
                            </p>
                         </div>
                      </div>
                      <div class="swiper-slide">
                         <div class="item">
                            <span class="number two"></span>
                            <h3>Tăng hiệu quả hoạt động quảng cáo</h3>
                            <p>
                               Nhờ tính ổn định và bảo mật, mọi hoạt động quảng cáo <br class="d-none d-xl-block" />
                               trực tuyến cần lấy website làm trung tâm, gốc rễ. <br class="d-none d-xl-block" />
                               Việc thiết kế website bán hàng chuyên nghiệp sẽ làm <br class="d-none d-xl-block" />
                               tăng hiệu quả SEO và quảng cáo, tăng tỉ lệ chuyển đổi
                            </p>
                         </div>
                      </div>
                      <div class="swiper-slide">
                         <div class="item">
                            <span class="number three"></span>
                            <h3>Xây dựng uy tín thương hiệu</h3>
                            <p>
                               Khi thiết kế web bán hàng với giao diện chuyên nghiệp, <br class="d-none d-xl-block" />
                               hỗ trợ mua sắm online tiện lợi sẽ khiến cho khách hàng <br class="d-none d-xl-block" />
                               yêu quý, tin tưởng hơn vào các sản phẩm và thương hiệu của bạn.
                            </p>
                         </div>
                      </div>
                      <div class="swiper-slide">
                         <div class="item">
                            <span class="number four"></span>
                            <h3>Chăm sóc khách hàng 24/7</h3>
                            <p>
                               Khi bán hàng trên website, khách hàng sẽ xem và đặt hàng bất <br class="d-none d-xl-block" />
                               cứ lúc nào trong ngày. Với một website bạn có thể tư vấn cho <br class="d-none d-xl-block" />
                               khách hàng 24/7. Từ đó, doanh thu của bạn sẽ tăng lên nhanh chóng.
                            </p>
                         </div>
                      </div>
                   </div>
                </div>
                <div class="swiper-pagination"></div>
                <div class="swiper-button-prev d-none d-md-block"><i class="fa fa-arrow-left" aria-hidden="true"></i></div>
                <div class="swiper-button-next d-none d-md-block"><i class="fa fa-arrow-right" aria-hidden="true"></i></div>
             </div>
          </div>
       </div>
       <div class="function text-center">
          <div class="container">
             <h2 id="sapo_web">Cresa Web giúp bạn thiết kế website bán hàng chuyên nghiệp</h2>
             <p>Không cần biết thiết kế hay lập trình bạn vẫn có thể sở hữu một website với nhiều tiện ích hiện đại</p>
             <div class="swiper-container">
                <div class="swiper-wrapper">
                   <div class="item first swiper-slide">
                      <div class="row">
                         <div class="col-xl-6 block-content">
                            <i class="icon theme"></i>
                            <h3>100+ giao diện Responsive đẹp mắt<i class="fa fa-minus d-block d-xl-none"></i></h3>
                            <p style="display: block;">
                            Giao diện <a href="{{url('kho-giao-dien/all')}}" target="_blank">website bán hàng</a> của Cresa được đầu tư thiết kế <br />tỉ mỉ và đa dạng,
                               sẵn sàng đáp ứng nhu cầu thiết kế website <br class="d-none d-xl-block" />
                               bán hàng cho hơn 30 ngành nghề khác nhau. Đặc biệt, tất cả <br class="d-none d-xl-block" />
                               giao diện này đều được thiết kế với công nghệ Responsive nên <br class="d-none d-xl-block" />
                               có thể hiển thị tối ưu trên mọi thiết bị. Dù cho khách hàng của bạn <br class="d-none d-xl-block" />
                               online bằng thiết bị gì, máy tính, máy tính bảng hay điện thoại <br class="d-none d-xl-block" />
                               di động thì website đều hiển thị đẹp mắt.
                            </p>
                            <a href="{{url('kho-giao-dien/'.'all'.'.html')}}" class="btn-registration d-none d-xl-inline-block">Kho giao diện</a>
                         </div>
                         <div class="col-xl-6 block-img d-none d-xl-block">
                         <img class="block-image-2" width='100%' src="{{url('frontend/images/et-web-design-free-responsive-joomla-template.png')}}" alt="Thiết kế website giá rẻ, chuẩn seo" />
                         </div>
                      </div>
                   </div>
                   <div class="item second swiper-slide">
                      <div class="row">
                         <div class="col-xl-6 block-content">
                            <i class="icon mobile"></i>
                            <h3>Bảo mật tuyệt đối website với chứng chỉ SSL<i class="fa fa-plus d-block d-xl-none"></i></h3>
                            <p>
                               Là công ty thiết kế website chuyên nghiệp, Cresa sẽ giúp bạn <br class="d-none d-xl-block" />
                               cài đặt HTTPS và SSL cho website trong vòng 50s và hoàn toàn miễn phí. <br class="d-none d-xl-block" />
                               Nhờ vậy, website của bạn sẽ được bảo vệ bởi hàng rào bảo mật <br class="d-none d-xl-block" />
                               tuyệt đối HTTPS và SSL tiêu chuẩn quốc tế. Mọi thông tin khách hàng <br class="d-none d-xl-block" />
                               của bạn sẽ tránh được nguy cơ tấn công của virus, hacker.
                            </p>
                         </div>
                         <div class="col-xl-6 block-img d-none d-xl-block">
                         <img src="{{url('frontend/images/dich-vu-cai-ssl.png')}}" alt="Thiết kế website giá rẻ, chuẩn seo" />
                         </div>
                      </div>
                   </div>
                   <div class="item three swiper-slide">
                      <div class="row">
                         <div class="col-xl-6 block-content">
                            <i class="icon connect"></i>
                            <h3>Kết nối tự động với các đơn vị giao hàng, thanh toán trực tuyến<i class="fa fa-plus d-block d-xl-none"></i></h3>
                            <p class="desc">
                               Cresa Web hỗ trợ tối đa cho các website thương mại điện tử thông qua <br class="d-none d-xl-block" />
                               việc kết nối với các đơn vị thanh toán trực tuyến hàng đầu như <br class="d-none d-xl-block" />
                               Paypal, Napas, OnePay... giúp cho người mua hàng có thể thanh <br class="d-none d-xl-block" />
                               toán ngay trên website 24/7.
                            </p>
                         </div>
                         <div class="col-xl-6 block-img d-none d-xl-block">
                         <img src="{{url('frontend/images/94a73a95-89a6-41cb-850d-34e3adcef907.png')}}" alt="Thiết kế website giá rẻ, chuẩn seo" />
                         </div>
                      </div>
                   </div>
                   <div class="item four swiper-slide">
                      <div class="row">
                         <div class="col-xl-6 block-content">
                            <i class="icon time"></i>
                            <h3>Tích hợp báo cáo Google Analytics ngay trên quản trị website<i class="fa fa-plus d-block d-xl-none"></i></h3>
                            <p>
                               Nhờ việc tích hợp báo cáo Google Analytics vào website Cresa, bạn có thể dễ theo dõi các thông tin như có bao nhiêu người truy cập vào website, họ truy cập vào từ máy tính hay mobile? Họ bao nhiêu tuổi ? Là nam hay nữ ... ngay trên trang quản trị Cresa. Kiểm soát tốt các chỉ số trên, bạn sẽ dễ dàng đánh giá hiệu quả website đem đến cho việc kinh doanh của mình.
                            </p>
                         </div>
                         <div class="col-xl-6 block-img d-none d-xl-block">
                         <img src="{{url('frontend/images/ANALYTICS-2-800x598.png')}}" alt="Thiết kế website giá rẻ, chuẩn seo" />
                         </div>
                      </div>
                   </div>
                </div>
             </div>
             <div class="swiper-pagination d-xl-block d-none"></div>
             <div class="swiper-button-prev d-xl-block d-none">
                <i class="fa fa-arrow-left" aria-hidden="true"></i>
             </div>
             <div class="swiper-button-next d-xl-block d-none">
                <i class="fa fa-arrow-right" aria-hidden="true"></i>
             </div>
             <div class="d-block d-xl-none text-center">
                <a class="btn-registration" href="javascript:;" data-toggle="modal" data-target="#ModalSetupTuvan">Bắt đầu kết nối</a>
             </div>
             <div class="d-none d-xl-block">
                <a href="javascript:;" data-href=".seo-ads" class="scroll-down">
                <i class="fas fa-angle-down"></i>
                </a>
             </div>
          </div>
       </div>
       <div class="seo-ads text-center">
          <div class="container">
             <p>Làm sao để có đến hàng trăm đơn hàng mỗi ngày từ website?</p>
             <h2 id="thiet_ke_web_ban_hang">Website bán hàng Cresa Web giúp <br class="d-none d-md-block d-xl-none" />tối ưu hiệu quả SEO và quảng cáo</h2>
             <div class="row">
                <div class="col-xl-5 col-lg-6 block-img d-none d-lg-block">
                <img src="{{url('frontend/images/toi-uu-seo.png')}}" alt="Thiết kế website giá rẻ, chuẩn seo" />
                </div>
                <div class="col-xl-7 col-lg-6 col-12 block-content">
                   <ul>
                      <li>
                         <i class="icon-arrow"></i>
                         <h3>Hỗ trợ tối ưu cho SEO</h3>
                         <p>
                            Với dịch vụ thiết kế website chuyên nghiệp, không cần phải là một chuyên gia SEO bạn vẫn có thể đưa website của mình lên top 1 Google. Cresa Web giúp bạn <a href="{{url('kho-giao-dien/'.'all'.'.html')}}" rel="nofollow">thiết kế website chuẩn SEO</a> với cấu trúc website đã được tối ưu thân thiện với công cụ tìm kiếm. Ngoài ra, bạn có thể tùy chỉnh thẻ tiêu đề (Title), mô tả (Meta Description) và URL trong website, thêm thẻ alt của hình ảnh.
                         </p>
                      </li>
                      <li>
                         <i class="icon-arrow"></i>
                         <h3>Tăng hiệu quả quảng cáo</h3>
                         <p>
                            Bên cạnh việc hỗ trợ quảng bá web tự nhiên thông qua SEO, thiết kế website bán hàng với Cresa còn giúp bạn tạo ra một trang đích tuyệt vời nhằm phát huy tốt nhất hiệu quả quảng cáo trả phí như Google Adwords hay Facebook Ads. Phương pháp này giúp website ngay lập tức tiếp cận đến khách hàng tiềm năng, đồng thời hỗ trợ để các chiến dịch quảng bá tự nhiên hiệu quả hơn.
                         </p>
                      </li>
                      <li>
                         <i class="icon-arrow"></i>
                         <h3>Mẹo nhỏ khi SEO</h3>
                         <p>
                            Với nhiều năm kinh nghiệm trong lĩnh vực SEO, Cresa đã tích lũy được rất nhiều thủ thuật, mẹo nhỏ khi SEO. Và chúng tôi sắn sàng mang nó ra và chia sẻ lại với khách hàng, từ đó giúp khách hàng sử dụng website một cách hiệu quả và tiết kiêm nhất trong quá trình kinh doanh online.
                         </p>
                      </li>
                   </ul>
                </div>
             </div>
             <a class="btn-registration"  href="javascript:;" data-toggle="modal" data-target="#ModalSetupTuvan">Bắt đầu kết nối</a>
          </div>
       </div>
       <div class="technology text-center">
          <div class="container">
             <h2 id="công_nghệ_tính_năng">
                <span>Với công nghệ vượt trội và tính năng khác biệt,</span> Thiết kế website bán hàng Cresa giúp bạn bán hàng 24/7
             </h2>
             <div class="row hidden-xs">
                <div class="col-xl-6 col-12 block-content">
                   <div class="swiper-container">
                      <div class="swiper-wrapper">
                         <div class="item swiper-slide">
                            <h3>Tích hợp phần mềm chatlive</h3>
                            <p>
                               Mọi thắc mắc của khách hàng sẽ được trả lời nhanh chóng, không những giúp bạn ghi điểm trong mắt khách hàng mà còn tăng khả năng chốt đơn hàng. Thiết kế website bán hàng Cresa đã được tích hợp sẵn sàng với các phần mềm live chat hàng đầu như Zopim, Subiz ..., giúp chủ cửa hàng chat với khách hàng ngay trên website bất cứ khi nào.
                            </p>
                         </div>
                         <div class="item swiper-slide">
                            <h3>Quy trình đặt hàng tự động</h3>
                            <p>
                               Khách hàng của bạn có thể tiến hành đặt hàng trên web bất cứ khi nào. Bởi vậy, thiết kế website bán hàng với quy trình đặt hàng hoàn toàn tự động sẽ giúp khách hàng sở hữu món đồ ưa thích chỉ sau một vài thao tác đơn giản mà không cần phải tốn thời gian gọi điện hay nhắn tin cho chủ shop.
                            </p>
                         </div>
                         <div class="item swiper-slide">
                            <h3>Tăng tỉ lệ hoàn thành đơn hàng với tính năng Abandoned Checkout</h3>
                            <p>
                               Với tính năng Abandoned Checkout, Cresa sẽ giúp bạn thu thập thông tin khách hàng ngay cả khi họ chưa hoàn tất đơn hàng. Từ những thông tin này bạn có thể gửi email marketing, coupons khuyến mại ...để "thúc giục" khách hàng hoàn tất đơn đặt hàng.
                            </p>
                         </div>
                      </div>
                      <div class="swiper-pagination d-block d-lg-none"></div>
                   </div>
                </div>
                <div class="col-xl-6 block-img d-none d-xl-block">
                   <div class="image-phone">
                      <div class="bg-phone"></div>
                   <img src="{{url('frontend/images/thietkewebsitebanhang-block-5-icon-phone.png')}}" alt="Thiết kế website giá rẻ, chuẩn seo" />
                   </div>
                   <div class="image-message">
                      <div class="bg-message"></div>
                   <img src="{{url('frontend/images/thietkewebsitebanhang-block-5-icon-message.png')}}" alt="Thiết kế website giá rẻ, chuẩn seo" />
                   </div>
                   <div class="image-email">
                      <div class="bg-email"></div>
                   <img src="{{url('frontend/images/thietkewebsitebanhang-block-5-icon-email.png')}}" alt="Thiết kế website giá rẻ, chuẩn seo" />
                   </div>
                </div>
             </div>
             <div class="text-center">
                <p>
                   <a class="btn-registration" href="javascript:;" data-toggle="modal" data-target="#ModalSetupTuvan">Bắt đầu kết nối</a>
                </p>
                <p style="margin-bottom:0;">
                   <a class="morelink" href="/bang-gia" title="Tham khảo thêm bảng giá thiết kế website của Cresa">Tham khảo thêm bảng giá thiết kế website của Cresa</a>
                </p>
             </div>
          </div>
       </div>
       <div class="why-choose">
          <div class="container">
             <h2>Tại sao bạn nên chọn thiết kế website bán hàng với Cresa <span>mà không phải đơn vị nào khác?</span></h2>
             <div class="row">
                <div class="col-xl-6 item">
                   <i class="icon why-choose-1"></i>
                   <div>
                      <h3>Vì Cresa là nền tảng thiết kế website sử dụng những công nghệ mới.</h3>
                      <p>Cresa đã có hơn 1,000 khách hàng tin dùng và là nền tảng thiết kế website sử dụng những công nghệ mới. Vì vậy, bạn có thể hoàn toàn yên tâm khi lựa chọn chúng tôi</p>
                   </div>
                </div>
                <div class="col-xl-6 item">
                   <i class="icon why-choose-2"></i>
                   <div>
                      <h3>Vì website bán hàng Cresa dễ dàng sử dụng và quản trị</h3>
                      <p>Với hệ thống quản trị thân thiện, bạn hoàn toàn có thể thêm bớt, chỉnh sửa: sản phẩm, gian hàng, nội dung của website một cách linh hoạt và dễ dàng mà không cần hiểu về code hay lập trình.</p>
                   </div>
                </div>
                <div class="col-xl-6 item">
                   <i class="icon why-choose-3"></i>
                   <div>
                      <h3>Vì Cresa được ứng dụng những công nghệ hiện đại nhất</h3>
                      <p>Tại Cresa, chúng tôi hiểu rằng công nghệ không ngừng thay đổi, vì vậy các website được thiết kế bởi Cresa luôn được cập nhật và ứng dụng những công nghệ hiện tại nhất để mang đến sự hài lòng cho khách hàng.</p>
                   </div>
                </div>
                <div class="col-xl-6 item">
                   <i class="icon why-choose-4"></i>
                   <div>
                      <h3>Vì Cresa sở hữu đội ngũ nhân viên giàu kinh nghiệp</h3>
                      <p>Với phương trâm khách hàng trước tiên, bạn sẽ luôn được đảm bảo hỗ trợ nhanh chóng bởi đội ngũ nhân viên nhiệt tình, giàu kinh nghiệm bất cứ khi nào gặp khó khăn trong quá trình vận hành website bán hàng của mình.</p>
                   </div>
                </div>
             </div>
          </div>
          <div class="d-none d-xl-block">
             <a href="javascript:;" data-href=".experience" class="scroll-down">
             <i class="fas fa-angle-down"></i>
             </a>
          </div>
       </div>
       <div class="experience text-center">
          <div class="container">
             <h2>
                Chia sẻ kinh nghiệm bán hàng online <br class="d-none d-md-block" />
                và thiết kế website bán hàng dành riêng cho bạn
             </h2>
             <div class="row">
             </div>
          </div>
       </div>
       <div class="top-refer-website text-center">
          <div class="container">
             <h2>Các website đẹp của khách hàng chúng tôi</h2>
             <div class="swiper-container">
                <div class="swiper-wrapper">
                   <div class="swiper-slide">
                      <div class="website-item">
                         <div class="item-infomation">
                            <div class="item-thumbnail">
                               <a href="https://bicicosmetics.vn/" target="_blank" rel="nofollow" title="Sao Th&#225;i Dương">
                               <img src="{{url('frontend/images/bici.png')}}" alt="Sao Th&#225;i Dương" class="item-img" />
                               </a>
                            </div>
                            <div class="hidden-info">
                               <div class="description">
                                 Bicicosmetics là doanh nghiệp bán hàng và phân phối mỹ phẩm lớn ở Việt Nam
                               </div>
                               <div class="view-zoom">
                                  <a href="https://bicicosmetics.vn/" class="zoom" target="_blank" rel="nofollow">
                                  <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="https://bicicosmetics.vn/">
                                  </a>
                               </div>
                               <div class="view-detail">
                                  <a href="https://bicicosmetics.vn/" target="_blank" rel="nofollow" class="btn-registration">
                                  Xem chi tiết
                                  </a>
                               </div>
                            </div>
                         </div>
                         <div class="website-name">
                            <a href="https://bicicosmetics.vn/" target="_blank" rel="nofollow" title="Mỹ phẩm Bicicosmetics">
                            Mỹ phẩm Bicicosmetics
                            </a>
                         </div>
                      </div>
                   </div>
                   <div class="swiper-slide">
                      <div class="website-item">
                         <div class="item-infomation">
                            <div class="item-thumbnail">
                               <a href="https://www.shopnhatchaly.com/" target="_blank" rel="nofollow" title="Kangaroo H&#224; Nội">
                               <img src="{{url('frontend/images/duocpham.png')}}" alt="Dược phẩm Chaly" class="item-img" />
                               </a>
                            </div>
                            <div class="hidden-info">
                               <div class="description">
                                 Shop Nhật Chaly tự tin mang tới cho khách hàng những sản phẩm Nhật nội địa chuẩn về nguồn gốc xuất xứ, đảm bảo về chất lượng và cung cách phục vụ tận tình, tâm huyết nhất
                               </div>
                               <div class="view-zoom">
                                  <a href="https://www.shopnhatchaly.com/" class="zoom" target="_blank" rel="nofollow">
                                  <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="https://www.shopnhatchaly.com/">
                                  </a>
                               </div>
                               <div class="view-detail">
                                  <a href="https://www.shopnhatchaly.com/" target="_blank" rel="nofollow" class="btn-registration">
                                  Xem chi tiết
                                  </a>
                               </div>
                            </div>
                         </div>
                         <div class="website-name">
                            <a href="https://www.shopnhatchaly.com/" target="_blank" rel="nofollow" title="Dược phẩm Chaly">
                            Dược phẩm Chaly
                            </a>
                         </div>
                      </div>
                   </div>
                   <div class="swiper-slide">
                      <div class="website-item">
                         <div class="item-infomation">
                            <div class="item-thumbnail">
                               <a href="https://ivymoda.com/" target="_blank" rel="nofollow" title="Viglacera">
                               <img src="{{url('frontend/images/ivymoda.png')}}" alt="Shop thời trang Ivymoda" class="item-img" />
                               </a>
                            </div>
                            <div class="hidden-info">
                               <div class="description">
                                  Shop thời trang Ivymoda
                               </div>
                               <div class="view-zoom">
                                  <a href="https://ivymoda.com/" class="zoom" target="_blank" rel="nofollow">
                                  <img src="{{url('frontend/images/icon-view-zoom.png')}}" alt="https://ivymoda.com/">
                                  </a>
                               </div>
                               <div class="view-detail">
                                  <a href="https://ivymoda.com/" target="_blank" rel="nofollow" class="btn-registration">
                                  Xem chi tiết
                                  </a>
                               </div>
                            </div>
                         </div>
                         <div class="website-name">
                            <a href="https://ivymoda.com/" target="_blank" rel="nofollow" title="Viglacera">
                            Viglacera
                            </a>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
             <a class="btn-registration" href="{{route('khachHang')}}" target="_blank">Xem thêm website khác</a><br><br><br>
          </div>
       </div>
       <script type="text/javascript">
          addLoadEvent(function () {
              var referSwiper = new Swiper('.top-refer-website .swiper-container', {
                  slidesPerView: 3,
                  pagination: {
                      clickable: true,
                  },
                  spaceBetween: 30,
                  autoplay: '5000',
                  loop: true,
                  breakpoints: {
                      767: {
                          slidesPerView: 1
                      },
                      991: {
                          slidesPerView: 2
                      }
                  }
              });
          });
       </script>
    </div>
    <script type="text/javascript" src="{{asset('frontend/asset/js/onloadEvent.js')}}"></script>
 </div>
@endsection