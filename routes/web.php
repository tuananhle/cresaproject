<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});
Route::get('/admin', function () {
    return view('app');
});
Route::get('/','HomeController@home')->name('home');
Route::group(['namespace'=>'Client','middleware' => ['checkLanguage']], function(){
    Route::group(['prefix'=>'san-pham'], function(){
        Route::get('/','ProductController@list')->name('product.list');
        Route::get('filter-product','ProductController@filter_product')->name('product.filter-product');
        Route::get('/{slug}','ProductController@detail_product')->name('product.detail');
    });
    Route::group(['prefix'=>'tin-tuc'], function(){
        Route::get('/','BlogController@list')->name('blog.list');
        Route::get('/{slug}','BlogController@detail')->name('blog.detail');
    });
    Route::get('tinh-nang','PageController@feature')->name('tinhnang');
    Route::get('web-ban-hang-uu-viet.html','PageController@banHangUuViet')->name('banHangUuViet');
    Route::get('quan-ly-cua-hang-online.html','PageController@quanLyCuaHang')->name('quanLyCuaHang');
    Route::get('tuy-chinh-giao-dien.html','PageController@tuyChinhGiaoDien')->name('tuyChinhGiaoDien');
    Route::get('marketing-seo.html','PageController@marketingSeo')->name('marketingSeo');
    Route::get('quan-ly-san-pham.html','PageController@quanLySanPham')->name('quanLySanPham');
    Route::get('manh-me-bao-mat.html','PageController@manhMeBaoMat')->name('manhMeBaoMat');
    Route::get('kho-giao-dien/{type}.html','PageController@khoGiaoDien')->name('khogiaodien');
    Route::post('search-interface','PageController@searchInterface')->name('searchinterface');
    Route::get('chi-tiet/{id}','PageController@detailInterface')->name('detaiinterface');
    Route::post('chon-giao-dien','PageController@changeInterface')->name('changeInterface');
    Route::post('user-request','PageController@userRequests')->name('userRequesta');
    Route::get('ve-chung-toi.html','PageController@aboutUs')->name('aboutUs');
    Route::get('bang-gia-theo-giao-dien-mau.html','PageController@priceTable')->name('priceTable');
    Route::get('bang-gia-theo-yeu-cau.html','PageController@priceRequest')->name('priceRequest');
    Route::get('khach-hang.html','PageController@khachHang')->name('khachHang');
    Route::get('lien-he.html','PageController@contact')->name('lienHe');
    Route::get('chi-tiet-goi-start-up.html','PageController@packageStartup')->name('packageStartup');
    Route::get('chi-tiet-goi-combo-pro.html','PageController@packageCombopro')->name('packageCombopro');
    Route::get('chi-tiet-goi-combo-ecommerce.html','PageController@packageEcommerce')->name('packageEcommerce');
    Route::get('chi-tiet-goi-combo-business.html','PageController@packageBusiness')->name('packageBusiness');


    Route::get('/dang-nhap','AuthController@login')->name('login.index');
    Route::post('/dang-nhap','AuthController@postLogin')->name('login.post');
    Route::get('/dang-ky','AuthController@register')->name('register.index');
    Route::post('/dang-ky','AuthController@postRegister')->name('register.post');
    Route::get('/dang-xuat','AuthController@logout')->name('logout.index');
    Route::post('reset-password', 'ResetPasswordController@sendMail')->name('resetPassword.index');
    Route::post('get-reset-password/{token}', 'ResetPasswordController@reset')->name('resetPassword.reset');
    Route::get('get-reset-password/{token}', 'ResetPasswordController@getReset')->name('resetPassword.getReset');
});


Route::post('/languages', 'LanguageController@index')->name('languages');
Route::get('/pusher', function() {
    event(new App\Events\HelloPusherEvent('Hi there Pusher!'));
    return "Event has been sent!";
});