<?php

namespace App\models\blog;

use Illuminate\Database\Eloquent\Model;
use App\models\language\Language;

class BlogCategory extends Model
{
    protected $table = "blog_category";
    public function saveCate($request)
    {
        $quiz_id = $request->quiz_id;
        $language = $request->language;
        $nextId = BlogCategory::max('id') + 1;
        if($quiz_id != "" && $language != ""){
            $query = BlogCategory::where([
                'quiz_id' => $quiz_id,
                'language'=> $language
             ])->first();
            if ($query) {
                $file= str_replace('http://localhost:8080','',$query->avatar);
                $filename = public_path().$file;
                if(file_exists( public_path().$file ) ){
                    \File::delete($filename);
                }
                $query->name = $request->name;
                $query->path = $request->path;
                $query->status = $request->status;
                $query->avatar = $request->avatar;
                $query->save();
            }else{
                $query = new BlogCategory();
                $query->quiz_id = $quiz_id;
                $query->language = $language;
                $query->name = $request->name;
                $query->path = $request->path;
                $query->status = $request->status;
                $query->avatar = $request->avatar;
                $query->save();
            }
            
        }else{
            $listLanguage = Language::get()->toArray();
            foreach ($listLanguage as $item) {
                $query = new BlogCategory();
                $query->quiz_id = $nextId;
                $query->language = $item['code'];
                $query->name = $request->name;
                $query->path = $request->path;
                $query->status = $request->status;
                $query->avatar = $request->avatar;
                $query->save();
            }
            
        }
        return $query;
    }
}
