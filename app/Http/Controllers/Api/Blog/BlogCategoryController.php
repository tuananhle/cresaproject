<?php

namespace App\Http\Controllers\Api\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\blog\BlogCategory;

class BlogCategoryController extends Controller
{
    public function add(Request $request, BlogCategory $category)
    {
        $data = $category->saveCate($request);
        return response()->json([
    		'message' => 'Save Success',
    		'data'=> $data
    	],200);
    }
    public function list(Request $request)
    {
        $keyword = $request->keyword;
        if($keyword == ""){
            $data = BlogCategory::orderBy('id','DESC')->where('language','vi')->get();
        }else{
            $data = BlogCategory::where('name', 'LIKE', '%'.$keyword.'%')->where('language','vi')->orderBy('id','DESC')->get()->toArray();
        }
        return response()->json([
            'data' => $data,
            'message' => 'success'
        ]);
    }
    public function edit($quiz_id, $language)
    {
        $data = BlogCategory::where(['quiz_id'=>$quiz_id,'language'=>$language])->first();
        return response()->json([
            'message' => 'success',
            'data' => $data
        ], 200);
    }
    public function delete( $id)
    {
        $query = BlogCategory::find($id);
        $file= str_replace('http://localhost:8080','',$query->avatar);
        $filename = public_path().$file;
        if(file_exists( public_path().$file ) ){
            \File::delete($filename);
        }
        $query->delete();
        return response()->json(['message'=>'Delete Success']);
    }
}
