<?php

namespace App\Http\Controllers\Api\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\blog\Blog;

class BlogController extends Controller
{
    public function create(Request $request, Blog $blog)
    {
    	$data = $blog->saveBlog($request);
        return response()->json([
    		'message' => 'Save Success',
    		'data'=> $data
    	],200);
    }
    public function list(Request $request)
    {
    	$keyword = $request->keyword;
        if($keyword == ""){
            $data = Blog::orderBy('id','DESC')->where('language','vi')->get();
        }else{
            $data = Blog::where('title', 'LIKE', '%'.$keyword.'%')->where('language','vi')->orderBy('id','DESC')->get()->toArray();
        }
        return response()->json([
            'data' => $data,
            'message' => 'success'
        ]);
    }
    public function delete($quiz_id, Blog $blog)
    {
        $data = $blog->deleteBlog($quiz_id);
        return response()->json([
    		'message' => 'Delete Success',
    		'data'=> $data
    	],200);
    }
    public function edit($quiz_id,$language)
    {
        $data = Blog::where([
            'quiz_id'=> $quiz_id,
            'language' => $language
        ])
        ->first();
        return response()->json([
            'data' => $data,
            'message' => 'success'
        ]);
    }

}
