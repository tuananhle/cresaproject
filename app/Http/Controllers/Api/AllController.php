<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AllController extends Controller
{
    public function uploadImage(Request $request)
    {
        if($imgAvatar = $request->file('img')){
            $nameAvatar = rand().$imgAvatar->getClientOriginalName();
            $imgAvatar->move('uploads/images/', $nameAvatar);
            return response()->json([
                'messenge' => 'success',
                'path' => url('/').'/uploads/images/'.$nameAvatar
            ],200);
        }else{
            return response()->json([
                'data' => 'fail'
            ],500);
        }
        
    }
    public function uploadImageMulti(Request $request)
    {
        $uploadId = [];
        if($files = $request->file('file')){
            foreach($request->file('file') as $key => $file){
                $name = rand().$file->getClientOriginalName();
                $fielname = $file->move('uploads/imagesMuli/', $name);
                $uploadId[] = url('/').'/uploads/images/'.$name;
            }
        }
        return response()->json([
            'messenge' => 'success',
            'path' => $uploadId
        ],200);
    }
}
