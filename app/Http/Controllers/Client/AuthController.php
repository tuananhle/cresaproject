<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use App\User;
use Auth,Validator;
use App\Notifications\CustomerRigisterNotify;
use App\Notifications\testNoti;

class AuthController extends Controller
{
    public function login()
    {

        return view('auth.login');
    }
    public function postLogin(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $customer = Customer::where('email',$request->email)->first();
        $status = $customer ? $customer->status : 0;
        if($status == 1){
            if(Auth::guard('customer')->attempt($credentials)){
                return redirect('/')->with('success', 'Đăng nhập thành công');
            }else{
                return back()->with('error', 'Mật khẩu không chính xác');
            }
        }else{
            return back()->with('error', 'Tài khoản không tồn tại hoặc không có quyền đăng nhập');
        }
        

    }
    public function register()
    {
        return view('auth.register');
    }
    public function postRegister(Request $request)
    {
        
        // $user->notify(new testNoti('user'));
        $validator = Validator::make($request->all(), [
            'email' => 'unique:customer'
        ]);
        if ($validator->fails()) {
            return back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = new Customer();
        $data->name = $request->lastName.$request->firstName;
        $data->phone = $request->phone;
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->status = 1;
        $data->save();
        if($data){
            $user = User::get();
            \Notification::send($user, new testNoti($data));
            $data->notify(new CustomerRigisterNotify($data));
        }
        return view('auth.login');
    }
    public function logout()
    {
        Auth::guard('customer')->logout();
        return redirect('/');
    }
}
