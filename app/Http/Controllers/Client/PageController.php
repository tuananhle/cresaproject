<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\product\Product;
use Session;
use App\models\CustomerEmail;
use Mail;
use App\Mail\CustomerInterface;
use App\Mail\UserRequest;
class PageController extends Controller
{
    public function feature()
    {
        return view('feature.feature');
    }
    public function banHangUuViet()
    {
        return view('feature.web-uu-viet');
    }
    public function quanLyCuaHang()
    {
        return view('feature.quan-ly-cua-hang');
    }
    public function tuyChinhGiaoDien()
    {
        return view('feature.tuy-chinh-giao-dien');
    }
    public function marketingSeo()
    {
        return view('feature.marketing-seo');
    }
    public function quanLySanPham()
    {
        return view('feature.quan-ly-san-pham');
    }
    public function manhMeBaoMat()
    {
        return view('feature.manh-me-bao-mat');
    }
    public function khoGiaoDien($type)
    {
        $language_current = Session::get('locale');
        if($type == 'all'){
            $data['theme'] = Product::where(['language'=>$language_current, 'status' => 1])->paginate(12);
        }elseif($type == 'discount'){
            $data['theme'] = Product::where(['language'=>$language_current, 'status' => 2])->paginate(12);
        }else{
            $data['theme'] = Product::where(['language'=>$language_current, 'status' => 1, 'category' => $type])->paginate(12);
        }
        return view('interface-library.index',$data);
    }
    public function searchInterface(Request $request)
    {
        $language_current = Session::get('locale');
        if($request->keyword){
            $data['theme'] = Product::where([
                ['language', '=', $language_current],
                ['status', '=', 1],
                ['name', 'like', '%' . $request->keyword . '%'],
            ])->paginate(12);
        }
        return view('interface-library.index',$data);
    }
    public function detailInterface($id)
    {
        $language_current = Session::get('locale');
        $data['interface'] = Product::where('id',$id)->first();
        $data['newInterface'] = Product::where(['language'=>$language_current, 'status' => 1])->orderBy('id','DESC')->paginate(4);
        return view('interface-library.detail',$data);
    }
    public function changeInterface(Request $request)
    {
        $input = $request->all();
        $data = new CustomerEmail();
        $data->name = $request->name;
        $data->phone = $request->phone;
        $data->email = $request->email;
        $data->product_id = $request->product_id;
        $data->save();
        if($data){
            Mail::to('tuananhdinamo1122@gmail.com')->send(new CustomerInterface($input));
        }
    	return back();
    }
    public function userRequests(Request $request)
    {
        $input = $request->all();
        $data = new CustomerEmail();
        $data->name = $request->name;
        $data->phone = $request->phone;
        $data->email = $request->email;
        $data->product_id = "";
        $data->note = $request->note;
        $data->save();
        if($data){
            Mail::to('tuananhdinamo1122@gmail.com')->send(new UserRequest($input));
        }
    	return back();
    }
    public function aboutUs()
    {
        return view('aboutUs');
    }
    public function priceTable()
    {
        return view('price.price');
    }
    public function khachHang()
    {
        return view('khachhang.index');
    }
    public function contact()
    {
        return view('contact.index');
    }
    public function priceRequest()
    {
        return view('price.price-request');
    }
    public function packageStartup()
    {
        $language_current = Session::get('locale');
        $data['newInterface'] = Product::where(['language'=>$language_current, 'status' => 1])->orderBy('id','DESC')->paginate(4);
        return view('packageService.start_up',$data);
    }
    public function packageCombopro()
    {
        $language_current = Session::get('locale');
        $data['newInterface'] = Product::where(['language'=>$language_current, 'status' => 1])->orderBy('id','DESC')->paginate(4);
        return view('packageService.combo_pro',$data);
    }
    public function packageEcommerce()
    {
        $language_current = Session::get('locale');
        $data['newInterface'] = Product::where(['language'=>$language_current, 'status' => 1])->orderBy('id','DESC')->paginate(4);
        return view('packageService.combo_ecommerce',$data);
    }
    public function packageBusiness()
    {
        $language_current = Session::get('locale');
        $data['newInterface'] = Product::where(['language'=>$language_current, 'status' => 1])->orderBy('id','DESC')->paginate(4);
        return view('packageService.combo_business',$data);
    }
}
