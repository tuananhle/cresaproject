<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\product\Product;
use App\models\blog\Blog;
use Session;

class ProductController extends Controller
{
    public function list()
    {
        $language_current = Session::get('locale');
        $data['data'] = Product::where(['language'=>$language_current, 'status' => 1])->paginate(9);
        return view('product.list',$data);
    }
    public function filter_product(Request $request)
    {
        $language_current = Session::get('locale');
        $type = $request->type;
        if($type == "alpha-asc"){
            $data = Product::where(['language'=>$language_current, 'status' => 1])->orderBy('name', 'ASC')->paginate(9);
        }elseif($type == "alpha-desc"){
            $data = Product::where(['language'=>$language_current, 'status' => 1])->orderBy('name', 'DESC')->paginate(9);
        }elseif($type == "created-desc"){
            $data = Product::where(['language'=>$language_current, 'status' => 1])->orderBy('id', 'DESC')->paginate(9);
        }elseif($type == "price-asc"){
            $data = Product::where(['language'=>$language_current, 'status' => 1])->orderBy('price', 'ASC')->paginate(9);
        }
        else{
            $data = Product::where(['language'=>$language_current, 'status' => 1])->orderBy('price', 'DESC')->paginate(9);
        }
        
        return view('layouts.product.ajax_list_product',[
            'data' => $data
          ]);
    }
    public function detail_product($slug)
    {
        $language_current = Session::get('locale');
        $data['blog'] = Blog::where(['language'=>$language_current, 'status' => 1])->orderBy('id','DESC')->paginate(5);
        $data['product'] = Product::where(['language'=>$language_current, 'slug' => $slug])->first();
        $data['product_relate'] = Product::where(['language'=>$language_current, 'category' => $data['product']->category])->get();
        return view('product.detail',$data);
    }
}
